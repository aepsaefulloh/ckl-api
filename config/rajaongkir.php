<?php
return [	
	'api_key'			=> env('RAJAONGKIR_API_KEY',''),
	'origin'			=>'',
	'type'				=> 'starter',
	'available_couriers'=>[
		'jne',
		'pos',
		'tiki'
	]
];
<?php

## default Route
$router->get('/', function() {
    return response(['status' => 401, 'message' => 'Not access!'], 401);
});

## Super Admin Route Backend Api
$router->group(['prefix' => 'api-admin'], function () use ($router) {
    $router->post('/login', 'Auth\AdminAuthController@login');
    $router->get('/find-admin/{id}', 'Auth\AdminAuthController@find_data');
    $router->post('/admin-update/token', 'Auth\AdminAuthController@update_token');


    ## Module Category
    $router->get('/category', 'CMS\Category\CategoryController@get_all_data'); 
    $router->get('/category-datatables', 'CMS\Category\CategoryController@datatables');
    $router->post('/category-store', 'CMS\Category\CategoryController@store');
    $router->get('/category/{id}', 'CMS\Category\CategoryController@find_data');
    $router->post('/category-publish', 'CMS\Category\CategoryController@publish');
    $router->post('/category/delete', 'CMS\Category\CategoryController@delete');
            
    ## Module Brand
    $router->get('/brand', 'CMS\Brand\BrandController@all_data');
    $router->get('/brand-datatables', 'CMS\Brand\BrandController@datatables');
    $router->get('/brand/{id}', 'CMS\Brand\BrandController@find_data');
    $router->post('/brand-store', 'CMS\Brand\BrandController@store');
    $router->post('/brand-publish', 'CMS\Brand\BrandController@publish');
    $router->post('/brand-delete', 'CMS\Brand\BrandController@delete');

    ## Module Product
    $router->get('/product-datatables', 'CMS\Product\ProductController@datatables');
    $router->get('/product/{id}', 'CMS\Product\ProductController@find');
    $router->post('/product/store', 'CMS\Product\ProductController@store');
    $router->post('/product/publish', 'CMS\Product\ProductController@publish');
    $router->post('/product/delete', 'CMS\Product\ProductController@delete');
    $router->post('/product/add-image/delete', 'CMS\Product\ProductController@delete_additional_image');
    $router->get('/product/remove_color/{id}', 'CMS\Product\Product_ontroller@remove_color');

    $router->post('/product/videos_store', 'CMS\Product\ProductController@videos_store');
    $router->get('/product/videos_datatables/{id}', 'CMS\Product\ProductController@videos_datatables');
    $router->post('/product/videos-delete', 'CMS\Product\ProductController@delete_videos');
    $router->put('/product/videos-publish/{id}', 'CMS\Product\ProductController@publish_videos');

    ## Module Toko
    $router->get('/toko/datatables', 'CMS\Toko\TokoController@datatables'); 
    $router->get('/toko/cari', 'CMS\Toko\TokoController@search_toko'); 
    $router->post('/toko/store', 'CMS\Toko\TokoController@store');
    $router->post('/toko/publish', 'CMS\Toko\TokoController@publish');
    $router->get('/toko/find-data/{id}', 'CMS\Toko\TokoController@find_data'); 
    $router->post('/toko/delete', 'CMS\Toko\TokoController@delete');

    ## Module Banner
    $router->get('/banner-datatables', 'CMS\Banner\BannerController@datatables');
    $router->get('/banner/{id}', 'CMS\Banner\BannerController@find');
    $router->post('/banner-store', 'CMS\Banner\BannerController@store');
    $router->post('/banner-publish', 'CMS\Banner\BannerController@publish');
    $router->post('/banner-image-delete', 'CMS\Banner\BannerController@delete_image');
    $router->post('/banner-delete', 'CMS\Banner\BannerController@delete');

    ## Module Admin Bank
    $router->get('/admin-bank/datatables', 'CMS\Bank\BankController@datatables');
    $router->post('/admin-bank/store', 'CMS\Bank\BankController@store');
    $router->post('/admin-bank/data', 'CMS\Bank\BankController@find_data');
    $router->post('/admin-bank/delete', 'CMS\Bank\BankController@delete');

    ## Module voucher 
    ## Module News and Tips
    $router->get('/news-tips/datatables', 'CMS\NewsTips\NewsTipsController@datatables');
    $router->get('/news-tips/{id}', 'CMS\NewsTips\NewsTipsController@find_data');
    $router->post('/news_tips/store', 'CMS\NewsTips\NewsTipsController@store');
    $router->post('/news_tips/delete', 'CMS\NewsTips\NewsTipsController@delete');
    $router->post('/news_tips/publish', 'CMS\NewsTips\NewsTipsController@publish');
    $router->post('/news_tips/delete_image', 'CMS\NewsTips\NewsTipsController@delete_image');

    ## Module Account 
    ## Admin
    $router->get('/account/admin-datatables', 'CMS\Account\AdminController@datatables');
    $router->get('/account/admin/{id}', 'CMS\Account\AdminController@find_by_id');
    $router->post('/account/admin-store', 'CMS\Account\AdminController@store');
    
    ## Customer
    $router->get('/account/customer-datatables', 'CMS\Account\CustomerController@datatables');
    $router->get('/account/customer/{id}', 'CMS\Account\CustomerController@find_data');

    ## Module Setting Apps
    $router->get('setting/{id}', 'CMS\Setting\SettingController@find_data');
    $router->post('setting/store', 'CMS\Setting\SettingController@store');

    ## Module Dashboard Admin
    $router->get('/admin/activity/{id}', 'Auth\AdminAuthController@activity');

    ## Module Payament
    ## Transaksi
    $router->get('/transaction/datatables', 'CMS\Payment\PaymentController@transaction_datatables');
    $router->get('/transaction/{id}', 'CMS\Payment\PaymentController@transaction_detail');
    $router->post('/transaction/payment/{type}', 'CMS\Payment\PaymentController@transaction_payment_update');
    $router->post('/transaction-status', 'CMS\Payment\PaymentController@update_status');
});

## Customer Route
$router->group(['prefix' => 'api'], function () use ($router) {
    ## setting apps data
    $router->get('/setting-apps/{id}', 'API\Setting\SettingController@find_data');

    ## Module Authentikasi
    $router->post('/register', 'Auth\CustomerAuthController@register');
    $router->post('/login', 'Auth\CustomerAuthController@login');
    $router->post('/logout', 'Auth\CustomerAuthController@logout');
    $router->get('/user', 'Auth\CustomerAuthController@profile');

    ## Wilyah Api
    $router->get('/provinsi', 'API\Wilayah\WilayahController@get_prov');
    $router->get('/kabupaten/{id}', 'API\Wilayah\WilayahController@get_city');

    ## Module Category
    ##public 
    $router->get('/category_parent/{type}', 'API\Category\CategoryController@all_parent');
    $router->get('/category/{type}', 'API\Category\CategoryController@all_sub');

    ## Module Brand
    ## Public
    $router->get('/brand', 'API\Brand\BrandController@all_data');

    ## Module Product
    ## public
    $router->get('/product', 'API\Product\ProductController@all_product');
    $router->get('/product/{id}', 'API\Product\ProductController@find_product');
    $router->get('/product_new', 'API\Product\ProductController@news_product');
    $router->get('/product_sale', 'API\Product\ProductController@sale_product');
    $router->get('/product_popular', 'API\Product\ProductController@popular_product');
    $router->get('/product_hit_top', 'API\Product\ProductController@hit_top_product');
    $router->get('/product-related/{category_id}/{product_id}', 'API\Product\ProductController@related_product');
    ## private
    $router->post('/product/comments', 'API\Product\ProductCommentsController@store');
    $router->get('/product/comments/{id}', 'API\Product\ProductCommentsController@result_by_product');

    $router->get('/product/add-cart/total', ['middleware' => 'auth', 'uses' => 'API\Product\ProductController@add_cart_product_count']);
    $router->get('/product/add-cart/all', ['middleware' => 'auth', 'uses' => 'API\Product\ProductController@get_add_cart_product_customer_data']);
    $router->post('/product/add-cart', ['middleware' => 'auth', 'uses' => 'API\Product\ProductController@add_cart_product_customer']);
    $router->post('/product/remove-cart', ['middleware' => 'auth', 'uses' => 'API\Product\ProductController@remove_cart_product_customer']);
    $router->get('/product/stock-cart/customer', 'API\Product\ProductController@stock_cart_product_customer');
    
    ## Module Payment
    ## Private
    // $router->get('payment/cart/{customer_id}', 'API\Payment\PaymentController@cart_by_customer');
    // $router->post('payment/cart-store', 'API\Payment\PaymentController@cart_store');
    $router->post('payment/order', 'API\Payment\PaymentController@insert_transaksi');
    $router->post('transaction/{tr_number}', 'API\Payment\PaymentController@transaction_by_number');

    ## Module News & Tips
    ## public
    $router->get('news-tips', 'API\NewsTips\NewsTipsController@get_all_data');
    $router->get('news-tips/{title}', 'API\NewsTips\NewsTipsController@get_find_data');
    $router->get('product-review', 'API\NewsTips\NewsTipsController@review_product');
    $router->get('artikel-product', 'API\NewsTips\NewsTipsController@artikel_product');
    $router->post('static-page', 'API\NewsTips\NewsTipsController@search_static_page');

    ##BANK
    $router->get('/bank', 'API\Bank\BankController@get_all');

    ## Module Banner
    ## Public
    $router->get('/content/banner', 'API\Banner\BannerController@get_by_category');

    ## Module Subscribe Form
    $router->post('/subscribe-store', 'API\Subscribe\SubscribeController@store');

    ## Module About Form
    $router->post('/about-form', 'API\About\AboutFormController@store');
});



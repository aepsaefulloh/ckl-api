<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductColor extends Model
{
    protected $table = 'product_color';
    protected $primaryKey = 'ID';
    public $timestamps = false;


    public function find_data($id) {
        $data = ProductColor::where('ID', $id)->first();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brand';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    // CMS SUPERADMIN API 
    public function all_data() {
        $data = Brand::where('STATUS', 1)->get();

        return $data;
    }

    public function datatables($type, $dir_origin, $dir_thumb) {
        $status = [
            " ",
            "checked='checked'"
        ];

        if ($type == "delete") {
            $get = Brand::where('STATUS', 99);
        } elseif ($type == 'all') {
            $get = Brand::where('STATUS', '<>', 99);
        }

        $result = $get->select(
            'ID',
            \DB::raw("CONCAT('".url($dir_origin)."', '/', IMAGE) AS IMAGE_ORIGIN"),
            \DB::raw("CONCAT('".url($dir_thumb)."', '/', IMAGE) AS IMAGE_THUMB"),
            'NAME_BRAND AS NAMA',
            'SEO',
            \DB::raw('(CASE WHEN STATUS = 1 THEN "' . $status[1] . '" ELSE "'. $status[0] .'" END) AS CHECKED') 
        )->get();
        return $result;
    }

    public function post($mode, $data, $id=null) {
        $post = \DB::table('brand');
        if ($mode == 'update') {
            $post->where('ID', $id)->update($data);
        } else {
            $post->insert($data);
        }

        return $post;
        
    }

    public function find_data($id, $dir_origin, $dir_thumb) {
        $get = Brand::where('ID', $id)
        ->select(
            'ID',
            'IMAGE',
            \DB::raw("CONCAT('". url($dir_origin) ."', '/', IMAGE) AS IMAGE_ORIGIN"),
            \DB::raw("CONCAT('". url($dir_thumb) ."', '/', IMAGE) AS IMAGE_THUMB"),
            'NAME_BRAND',
            'SEO',
            'META_DESC',
            'STATUS'
        )->first();

        return $get;
    }

    // CUSTOMER API
    public function get_all($dir_origin, $dir_thumb) {
        $data = Brand::where('STATUS', 1)
        ->select(
            'ID',
            \DB::raw("CONCAT('". url($dir_origin) ."', '/', IMAGE) AS IMAGE_ORIGIN"),
            \DB::raw("CONCAT('". url($dir_thumb) ."', '/', IMAGE) AS IMAGE_THUMB"),
            'NAME_BRAND',
            "SEO",
            "META_DESC"
        )
        ->orderBy('NAME_BRAND', 'asc')
        ->get();

        return $data;
    }
}

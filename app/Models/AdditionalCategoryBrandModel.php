<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AdditionalCategoryBrandModel extends Model
{
    protected $table = 'additional_category_brand';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function all_data_by_id($id) {
        $data = AdditionalCategoryBrandModel::where('CATEGORY', $id)->get();

        return $data;
    }

    public function store($data) {
        $post = DB::table('additional_category_brand')->insert($data);

        return $post;
    }

    public function remove($id) {
        $delete = DB::table('additional_category_brand')->where('CATEGORY', $id)->delete();

        return $delete;
    }
}

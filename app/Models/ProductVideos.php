<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductVideos extends Model
{
    protected $table = 'product_videos';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function datatables($id) {
        $data = ProductVideos::where('UNIQ_CODE', $id)->get();

        return $data;
    }

    public function remove($id) {
        $data = \DB::table('product_videos')->where('ID', $id)->delete();

        return $data;
    }

    public function publish($id) {
        // Get Value Status 
        $update = \DB::table('product_videos')->where('ID', $id)->first();

        if ($update && $update->STATUS == 1) {
            \DB::table('product_videos')->where('ID', $id)->update([
                "STATUS"    => 0
            ]);

            return [
                "status"  => true,
                "message" => "Unpublish"
            ];
        } elseif ($update && $update->STATUS == 0) {
            \DB::table('product_videos')->where('ID', $id)->update([
                "STATUS"    => 1
            ]);

            return [
                "status"  => true,
                "message" => "Publish"
            ];
        } else {
            return [
                "status"  => false,
                "message" => "Failed!"
            ];
        }
    }
}

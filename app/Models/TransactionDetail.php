<?php

namespace App\Models;
use DB;


use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    protected $table = 'transaction_detail';

    public function insert($data=array()){
        $result = DB::table('transaction_detail')->insert($data);

        if($result){
            $result = [];
            $result['success'] = 201;//created
            $result['message'] = "Data has been saved!";
        }else{
            $result = [];
            $result['success'] = 500;//internal server error
            $result['message'] = "data failed to save!";
        }            
        return $result;
    }

    public function updates($key=[],$data=[]){
        $result = DB::table('product')->where($key)->update($data);
 
        if($result){
            $result = [];
            $result['success'] = 202;//accepted
            $result['message'] = "Data has been Updated!";
            $result = response()->json($result,$result['success']);
        }else{
            $result = [];
            $result['success'] = 304;//not modified
            $result['message'] = "data failed to Update!";
            $result = response()->json($result,$result['success']);
        }            
        return $result;
    }

    public function delete($key=[],$data=[]){
        $result = DB::table('product')->where($key)->update($data);
 
        if($result){
            $result = [];
            $result['success'] = 202;//accepted
            $result['message'] = "Data has been deleted!";
            $result = response()->json($result,$result['success']);
        }else{
            $result = [];
            $result['success'] = 304;//not modified
            $result['message'] = "data failed to delete!";
            $result = response()->json($result,$result['success']);
        }            
        return $result;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;
use City;

class Transaction extends Model
{
    protected $table = 'transaction';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    // Admin CMS
    public function datatables($type) {
        $transaksi = DB::table('transaction')->orderBy('TRDATE', 'desc')->get();
        
        return $transaksi;
    }

    public function result_by_id($id) {
        // Get Transaksi By Id
        $transaksi = DB::table('transaction')->where('ID', $id)->first();

        // Get Bank Admin Data By ID
        $bank = DB::table('bank')->where('ID', $transaksi->BANK_ID)->first();

        // Get Transaksi Produk By ID
        $transaksi_product = DB::table('transaction_product')->where('TRANSACTION_ID', $transaksi->ID)->get();

        // Result
        $result['ID']               = $transaksi->ID;
        $result['TRDATE']           = $transaksi->TRDATE;
        $result['TRNUMBER']         = $transaksi->TRNUMBER;
        $result['FULLNAME']         = $transaksi->FULLNAME;
        $result['EMAIL']            = $transaksi->EMAIL;
        $result['PHONE']            = $transaksi->PHONE;
        $result['PAYMENT']          = $transaksi->PAYMENT;
        $result['BANK']             = [
            "NAME"  => $bank->BANK_NAME,
            "NO_REK"=> $bank->NO_REK 
        ];
        $result['LOCATION']         = [
            "PROV"      => $transaksi->PROV_ID,
            "KAB"       => City::find($transaksi->KAB_ID),
            "ADDRESS"   => $transaksi->ADDRESS,
            "KODEPOS"   => $transaksi->KODEPOS
        ];
        $result['COURIER']          = [
            "CODE"      => $transaksi->COURIER,
            "SERVICE"   => $transaksi->SERVICE_COURIER,
            "PRICE"     => $transaksi->PRICE_ONGKIR,
            "DAY"       => $transaksi->COURIER_DAY
        ];
        
        foreach ($transaksi_product as $key => $y) {
            // Search Product BY UNIQCODE
            $product = DB::table('product')->where('UNIQ_CODE', $y->UNIQ_CODE)->first();

            $result['PRODUCT'][$key]['UNIQ_CODE']   = $y->UNIQ_CODE;
            $result['PRODUCT'][$key]['NAME']        = $product->NAME_PRODUCT;
            $result['PRODUCT'][$key]['IMAGE']       = ($product->IMAGE) ? url().'/storage/images/product/thumb/'.$product->CATEGORY.'/'.$product->IMAGE : null;
            $result['PRODUCT'][$key]['QUANTITY']    = $y->QUANTITY;
            $result['PRODUCT'][$key]['PRICE']       = $y->PRICE;
            $result['PRODUCT'][$key]['TOTAL']       = $y->TOTAL;
        }
        $result['PRICE_PRODUCT']    = (count($transaksi_product) > 0) ? collect($transaksi_product)->SUM('TOTAL') : 0;
        $result['TOTAL_ALL_PRICE']  = (count($transaksi_product) > 0) ? collect($transaksi_product)->SUM('TOTAL') + $transaksi->PRICE_ONGKIR : 0;
        $result['KET']              = $transaksi->KET;
        $result['CUSTOMER_ID']      = $transaksi->CUSTOMER_ID;
        $result['STATUS']           = $transaksi->STATUS;
        $result['NO_RESI']          = $transaksi->NO_RESI;
        $result['UPDATE_DATE']      = $transaksi->UPDATE_DATE;

        return $result;
    }

    public function update_payment_status($type, $id, $stok=null) {
        if ($type == 'bayar') {
            $status = 1;
        } elseif ($type == 'failed') {
            $status = 99;
        } else { 
            $status = 0;
        }

        // Update Data Transaction
        $update = DB::table('transaction')->where('ID', $id)->update([
            "STATUS"        => $status,
            "UPDATE_DATE"   => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
        ]);

        // Update Status Transaction Product
        DB::table('transaction_product')->where('TRANSACTION_ID', $id)->update(['status' => $status]);

        // Update Stock Product Jika Product Behasil Dibeli
        if ($type == 'bayar') {
            $search_product = DB::table('transaction_product')->where('TRANSACTION_ID', $id)->get();

            foreach ($search_product as $key => $value) {
                // Update Stok Product
                DB::table('stok')->where('UNIQ_CODE', $value->UNIQ_CODE)->update([
                    "STOCK_IN"      => DB::table('stok')->where('UNIQ_CODE', $value->UNIQ_CODE)->SUM('STOCK_IN') - (int) $value->QUANTITY,
                    "STOCK_UPDATE"  => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);

                // Insert Activity Stok Product
                DB::table('activity_stok')->insert([
                    "UNIQ_CODE" => $value->UNIQ_CODE,
                    "STOCK"     => ($value->QUANTITY == 0) ? $value->QUANTITY : '-'.$value->QUANTITY,
                    "STOCK_DATE"=> \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);
            }
        } 
        
        return $update;
    }

    public function payment_status($id, $data) {
        // Update Data Transaction
        if ($data['status'] == 2) {
            $req = [
                "STATUS"        => $data['status'],
                "NO_RESI"       => $data['no_resi'],
                "UPDATE_DATE"   => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
            ];
        } else {
            $req = [
                "STATUS"        => $data['status'],
                "UPDATE_DATE"   => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
            ];
        }
        
        $update = DB::table('transaction')->where('ID', $id)->update($req);

        // Update Status Transaction Product
        DB::table('transaction_product')->where('TRANSACTION_ID', $id)->update(['status' => $data['status']]);

        // Update Stock Product Jika Product Behasil Dibeli
        if ($data['status'] == 1) {
            $search_product = DB::table('transaction_product')->where('TRANSACTION_ID', $id)->get();

            foreach ($search_product as $key => $value) {
                // Update Stok Product
                DB::table('stok')->where('UNIQ_CODE', $value->UNIQ_CODE)->update([
                    "STOCK_IN"      => DB::table('stok')->where('UNIQ_CODE', $value->UNIQ_CODE)->SUM('STOCK_IN') - (int) $value->QUANTITY,
                    "STOCK_UPDATE"  => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);

                // Insert Activity Stok Product
                DB::table('activity_stok')->insert([
                    "UNIQ_CODE" => $value->UNIQ_CODE,
                    "STOCK"     => ($value->QUANTITY == 0) ? $value->QUANTITY : '-'.$value->QUANTITY,
                    "STOCK_DATE"=> \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);
            }
        } 
        
        return $update;
    }


    // Public Front End
    public function insert($transaksi, $product) {
        // Generate Number Transaksi
        $latest_id = DB::table('transaction')->select('TRNUMBER')->latest('ID')->first();
        
        if ($latest_id) {
            $tr_number = preg_replace('/\TM/', '', $latest_id->TRNUMBER);
            $removed1char = substr($tr_number, 1);
            $generate = 'TM'.str_pad($removed1char + 1, 17, "0", STR_PAD_LEFT);
        } else {
            $generate = 'TM'.str_pad(1, 17, "0", STR_PAD_LEFT);
        }

        // Insert Transaksi Data
        $store_transaksi                    = New Transaction;
        $store_transaksi->TRNUMBER          = $generate;
        $store_transaksi->TRDATE            = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
        $store_transaksi->FULLNAME          = $transaksi['FULLNAME'];
        $store_transaksi->PHONE             = $transaksi['PHONE'];
        $store_transaksi->EMAIL             = $transaksi['EMAIL'];
        $store_transaksi->PROV_ID           = $transaksi['PROV_ID'];
        $store_transaksi->KAB_ID            = $transaksi['KAB_ID'];
        $store_transaksi->ADDRESS           = $transaksi['ADDRESS'];
        $store_transaksi->KODEPOS           = $transaksi['KODEPOS'];
        $store_transaksi->PAYMENT           = $transaksi['PAYMENT'];
        $store_transaksi->BANK_ID           = $transaksi['BANK_ID'];
        $store_transaksi->COURIER           = $transaksi['COURIER'];
        $store_transaksi->SERVICE_COURIER   = $transaksi['SERVICE_COURIER'];
        $store_transaksi->PRICE_ONGKIR      = $transaksi['PRICE_ONGKIR'];
        $store_transaksi->COURIER_DAY       = $transaksi['COURIER_DAY'];
        $store_transaksi->KET               = $transaksi['KET'];
        $store_transaksi->CUSTOMER_ID       = $transaksi['CUSTOMER_ID'];
        $store_transaksi->STATUS            = 0;
        $store_transaksi->save();

        // Insert Transaksi Product Data
        foreach ($product as $products) {
            // Search Product By Uniq Code
            $result_product = DB::table('product')->where('UNIQ_CODE', $products['UNIQ_CODE'])->first();

            DB::table('transaction_product')->insert([
                "TRANSACTION_ID"        => $store_transaksi->ID,
                "UNIQ_CODE"             => $products['UNIQ_CODE'],
                "QUANTITY"              => $products['TOTAL'],
                "PRICE"                 => $products['PRICE'],
                "TOTAL"                 => $products['PRICE_TOTAL'],
                "SALE"                  => (isset($result_product) && $result_product && $result_product->SALE != '') ? $result_product->SALE : null,
                "DISCOUNT"              => (isset($result_product) && $result_product && $result_product->SALE != '' && $result_product->DISCOUNT != '') ? $result_product->DISCOUNT : null,
                "STATUS"                => 0
            ]);
        }

        return [
            "status"            => $store_transaksi->save(),
            "transaction_id"    => $store_transaksi->ID,
            "transaction_number"=> $store_transaksi->TRNUMBER,
            "transaction_date"  => $store_transaksi->TRDATE
        ];
    } 

    public function result_by_number($tr_number) {
        // Get Transaction Data By TRNUMBER
        $transaksi = DB::table('transaction')->where('TRNUMBER', $tr_number)->first();

        if ($transaksi) {
            // Search Bank 
            $bank = DB::table('bank')->where('ID', $transaksi->BANK_ID)->first();

            // Result Data
            $result['TRNUMBER']     = $transaksi->TRNUMBER;
            $result['TRDATE']       = $transaksi->TRDATE;
            $result['FULLNAME']     = $transaksi->FULLNAME;
            $result['PHONE']        = $transaksi->PHONE;
            $result['EMAIL']        = $transaksi->EMAIL;
            $result['LOCATION']     = City::find($transaksi->KAB_ID);
            $result['ADDRESS']      = $transaksi->ADDRESS;
            $result['KODEPOS']      = $transaksi->KODEPOS;
            $result['PAYMENT']      = $transaksi->PAYMENT;
            $result['BANK']         = [
                "NAME"      => ($bank) ? $bank->BANK_NAME : null,
                "NO_REK"    => ($bank) ? $bank->NO_REK : null
            ];
            $result['COURIER']      = [
                "CODE"      => $transaksi->COURIER,
                "SERVICE"   => $transaksi->SERVICE_COURIER,
                "PRICE"     => $transaksi->PRICE_ONGKIR,
                "DAY"       => $transaksi->COURIER_DAY
            ];
            $result['CUSTOMER']     = $transaksi->CUSTOMER_ID;
            $result['KET']          = $transaksi->KET;

            // Get Product Transaction
            $transaksi_product = DB::table('transaction_product')->where('TRANSACTION_ID', $transaksi->ID)
            ->select(
                'ID',
                'UNIQ_CODE',
                'QUANTITY',
                'PRICE',
                'TOTAL'
            )->get();

            foreach ($transaksi_product as $key => $tp) {
                // Get Product By Transaksi Product
                $product = DB::table('product')->where('UNIQ_CODE', $tp->UNIQ_CODE)->first();

                $result['PRODUCT'][$key]['ID']          = $tp->ID;
                $result['PRODUCT'][$key]['UNIQ_CODE']   = $tp->UNIQ_CODE;
                $result['PRODUCT'][$key]['NAME']        = ($product) ? $product->NAME_PRODUCT : null;
                $result['PRODUCT'][$key]['IMAGE']       = ($product) ? url().'/storage/images/product/thumb/'.$product->CATEGORY.'/'.$product->IMAGE : null;
                $result['PRODUCT'][$key]['QUANTITY']    = $tp->QUANTITY;
                $result['PRODUCT'][$key]['PRICE']       = $tp->PRICE;
                $result['PRODUCT'][$key]['TOTAL']       = $tp->TOTAL;
            }

            $result['PRICE_PRODUCT_TOTAL']  = collect($transaksi_product)->SUM('PRICE');
            $result['PRICE_TOTAL']  = collect($transaksi_product)->SUM('PRICE') + $transaksi->PRICE_ONGKIR;
            
            if ($transaksi->STATUS == 0) {
                $result['STATUS']   = "pending|hasn't transferred the payment yet";
            } elseif ($transaksi->STATUS == 1) {
                $result['STATUS']   = "success|Already Transfer Payment";
            } elseif ($transaksi->STATUS == 99) {
                $result['STATUS']   = "Failed|time limit";
            }
            
        } else {
            $result = [];
        }
        
        return $result;
    }
}

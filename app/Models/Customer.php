<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Customer extends Model 
{
    protected $table = 'customer';
    protected $primaryKey = 'ID';
    protected $fillable = [
        'EMAIL',
        'USER',
        'PASSWORD',
        'REG_DATE',
        'FULLNAME',
        'ADDRESS',
        'TELEPHONE',
        'TOKEN_KEY',
        'STATUS'
    ];
    protected $hidden = [
        'PASSWORD',
        'KEY_TOKEN'
    ];

    public $timestamps = false;

   public function login($email, $password, $ip, $user_agent) {
        // Find Akun Customer
        $akun = \DB::table('customer')->where('EMAIL', $email)->where('STATUS', 1)->first();

        if ($akun) {
            $verify_pass = Hash::check($password, $akun->PASSWORD);
            if ($verify_pass == 1) {
                ## Generate Key Token
                $token = openssl_random_pseudo_bytes(16);
                $token = bin2hex($token);

                ## Update Token User
                \DB::table('customer')->where('ID', $akun->ID)->update([
                    "TOKEN" => $token
                ]);

                ## Insert Data Activity User
                \DB::table('activity_customer')->insert([
                    "IP_ADDRESS"    => $ip,
                    "CUSTOMER_ID"   => $akun->ID,
                    "USER_AGENT"    => $user_agent,
                    "LOGIN_DATE"    => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);

                return [
                    "CODE"          => "valid",
                    "CUSTOMER_ID"   => $akun->ID,
                    "EMAIL"         => $akun->EMAIL,
                    "USER"          => $akun->USER,
                    "FULLNAME"      => $akun->FULLNAME,
                    "REGISTER_DATE" => $akun->REG_DATE,
                    "ADDRESS"       => $akun->ADDRESS,
                    "PHONE"         => $akun->TELEPHONE,
                    "TOKEN"         => $token
                ];
            } else {
                return [
                    "CODE"  => "not_valid"
                ]; 
            }
        } else {
            return [
                "CODE"  => "not_found"
            ];
        }
        
   }

   public function register($data, $email) {
       $find_email = \DB::table('customer')->where('EMAIL', $email)->count();

       if ($find_email > 0) {
           // 99 tipe untuk email yang sudah ada
           return 99;
       } else {
            $insert_akun = \DB::table('customer')->insert($data);

            return $insert_akun;
       }
   }

   public function logout($id) {
       ## FInd Akun & Update Token is Null
       $akun = \DB::table('customer')->where('ID', $id)->update([
           "TOKEN"  => ''
       ]);

       return $akun;
   }

   public function datatables() {
       $customer = \DB::table('customer')->select('ID', 'EMAIL', 'USER', 'REG_DATE', 'FULLNAME', 'TELEPHONE', 'ADDRESS', 'STATUS')->get();

       return $customer;
   }
}

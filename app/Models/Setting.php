<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $table = 'setting';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function find_data($id, $dir_origin, $dir_thumb) {
        $data = Setting::where('ID', $id)
        ->select(
            '*',
            \DB::raw("CONCAT('". url($dir_origin) ."', '/', LOGO) AS LOGO_ORIGIN"),
            \DB::raw("CONCAT('". url($dir_thumb) ."', '/', LOGO) AS LOGO_THUMB"),
            \DB::raw("CONCAT('". url($dir_origin) ."', '/', LOGO_FOOTER) AS LOGO_FOOTER_ORIGIN"),
            \DB::raw("CONCAT('". url($dir_thumb) ."', '/', LOGO_FOOTER) AS LOGO_FOOTER_THUMB")
        )->first();

        return $data;
    }

}

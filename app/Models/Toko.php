<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Province;
use City;

class Toko extends Model
{
    protected $table = 'toko';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function datatables($dir_origin, $dir_thumb, $type) {
        $status = [
            " ",
            "checked='checked'"
        ];

        if ($type == "delete") {
            $get = Toko::where('STATUS', 99);
        } else {
            $get = Toko::where('STATUS', '<>', 99);
        }

        $get->select(
            'ID as id',
            'NAME_TOKO as name_toko',
            \DB::raw('CONCAT("'.url().'/'.$dir_origin.'", IMAGE) AS image_origin'),
            \DB::raw('CONCAT("'.url().'/'.$dir_thumb.'", IMAGE) AS image_thumb'),
            'BANK as bank',
            'NO_REK as no_rek',
            'TELEPHONE as telephone',
            'ADDRESS as address',
            'PROV_ID as prov',
            'KOTA_ID as city',
            'STATUS as status',
            \DB::raw('(CASE WHEN STATUS = 1 THEN "' . $status[1] . '" ELSE "'. $status[0] .'" END) AS checked') 
        );
        $results = $get->get();

        if (count($results) > 0) {
            foreach ($get->get() as $key => $val) {
                $findProv = Province::find($val->prov);
                $findCity = City::find($val->city);

                $result[$key] = $val;
                $result[$key]['prov'] = $findProv['province'];
                $result[$key]['city'] = $findCity['city_name'];
            }
        } else {
            $result = [];
        }
        
        return $result;
    }

    public function searchToko($keyword) {
        $toko = Toko::where('NAME_TOKO', 'LIKE', '%'.$keyword.'%')->where('STATUS', 1)->get();

        return $toko;
    }

    public function get_by_id($id, $dir_origin, $dir_thumb) {
            $get = Toko::where('ID', $id)->select(
            'ID as id',
            'NAME_TOKO as name_toko',
            'IMAGE as image',
            \DB::raw('CONCAT("'.url().'/'.$dir_origin.'", IMAGE) AS image_origin'),
            \DB::raw('CONCAT("'.url().'/'.$dir_thumb.'", IMAGE) AS image_thumb'),
            'BANK as bank',
            'NO_REK as no_rek',
            'TELEPHONE as telephone',
            'ADDRESS as address',
            'PROV_ID as prov',
            'KOTA_ID as city',
            'STATUS as status'
        )->first();

        // if ($get) {
        //     $findProv = Province::find($get->prov);
        //     $findCity = City::find($get->city);

        //     $result = $get;
        //     $result['prov'] = $findProv['province'];
        //     $result['city'] = $findCity['city_name'];
        // }
        
        return $get;
    }

    public function find($id) {
        $get = Toko::where('ID', $id)->first();

        return $get;
    }
}

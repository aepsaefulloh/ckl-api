<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use App\Models\AdditionalCategoryBrandModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Category extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'category';
    protected $primaryKey = 'ID';
    protected $fillable = [
        'PARENT_ID',
        'TITLE',
        'SEO',
        'TIPE',
        'STATUS'
    ];

    public $timestamps = false;

    // CMS API
    public function all_data($type) {
        $data = Category::where('TIPE', $type)
        ->where('STATUS', 1)
        ->select(
            '*',
            \DB::raw("CONCAT('".url('storage/images/category/origin')."', '/', 'IMAGE') AS IMAGE_ORIGIN"),
            \DB::raw("CONCAT('".url('storage/images/category/thumb')."', '/', 'IMAGE') AS IMAGE_THUMB")
        )->get();

        return $data;
    }

    public function datatables($type) {
        $status = [
            " ",
            "checked='checked'"
        ];

        if ($type == "delete") {
            $get = Category::where('STATUS', 99);
        } else {
            $get = Category::where('STATUS', '<>', 99);
        }

        $get->select(
            'ID as id',
            'TITLE as title',
            'SEO as seo',
            'STATUS as status',
            'TIPE as tipe',
            \DB::raw('CONCAT("'.url().'/storage/images/category/origin/'.'", IMAGE) AS image_origin'),
            \DB::raw('CONCAT("'.url().'/storage/images/category/thumb/'.'", IMAGE) AS image_thumb'),
            \DB::raw('(CASE WHEN STATUS = 1 THEN "' . $status[1] . '" ELSE "'. $status[0] .'" END) AS checked') 
        );

        return $get->get();
    }

    public function get_find($id) {
        // Find Data Category
        $category = Category::where('ID', $id)
        ->select(
            '*',
            \DB::raw('CONCAT("'.url().'/storage/images/category/origin/'.'", IMAGE) AS IMAGE_ORIGIN'),
            \DB::raw('CONCAT("'.url().'/storage/images/category/thumb/'.'", IMAGE) AS IMAGE_THUMB'),
        )->first();
        
        // Find Data Additional Category Brand
        $add_category_brand = AdditionalCategoryBrandModel::where('CATEGORY', $category->ID)->pluck('BRAND');
        
        $result = $category;
        $result['BRAND'] = $add_category_brand;
        
        return $result;
    }

    // Customer API
    public function all_parent($type) {
        $data = Category::where('TIPE', $type)
        ->where('STATUS', 1)
        ->where('PARENT_ID', 0)
        ->select(
            '*',
            \DB::raw("CONCAT('".url('storage/images/category/origin')."', '/', IMAGE) AS IMAGE_ORIGIN"),
            \DB::raw("CONCAT('".url('storage/images/category/thumb')."', '/', IMAGE) AS IMAGE_THUMB")
        )->get();

        return $data;
    }

    public function all_sub($type) {
        $data = Category::where('TIPE', $type)
        ->where('STATUS', 1)
        ->where('PARENT_ID', '<>', 0)
        ->select(
            '*',
            \DB::raw("CONCAT('".url('storage/images/category/origin')."', '/', IMAGE) AS IMAGE_ORIGIN"),
            \DB::raw("CONCAT('".url('storage/images/category/thumb')."', '/', IMAGE) AS IMAGE_THUMB")
        )->get();

        return $data;
    }

    

    

    
    public function sort_data() {
        $sort = [];
        // Get Category Parent
        $category_parent = Category::where('STATUS', 1)
        ->where('TIPE', 'product')
        ->where('PARENT_ID', '=', \DB::raw('ID'))
        ->select(
            '*',
            \DB::raw("CONCAT('PARENT') AS CATEGORY")
        )->get();

        // Get Category Childs
        $category_childs = Category::where('STATUS', 1)
        ->where('TIPE', 'product')
        ->where('PARENT_ID', '<>', \DB::raw('ID'))
        ->get();

        // // Get Child
        // foreach ($req_loop as $key => $rl) {
        //     $get_child[$key] = Category::where('PARENT_ID', $rl->ID)
        //     ->where('ID', '<>', $rl->ID)
        //     ->where('STATUS', 1)
        //     ->select('*', \DB::raw('CONCAT("CHILD") AS CATEGORY'))
        //     ->get();

        //     // Get Childs
        //     foreach ($get_child[$key] as $keys => $ch) {
        //         $get_childs[$ch->ID] = Category::where('PARENT_ID', $ch->ID)
        //         ->where('ID', '<>', $ch->ID)
        //         ->where('STATUS', 1)
        //         ->select('*', \DB::raw('CONCAT("CHILDS") AS CATEGORY'))
        //         ->get();
        //     }
        // }

        return [
            "PARENT"    => $category_parent,
            "CHILDS"    => $category_childs
        ];
    }

    public function sort_parent_child($id) {
        // Get BreadCrumb Category Product
        $cat = [];
        // Get Parent
        $req = Category::where('ID', $id)->first();
        // Get Child (1)
        $getParent = Category::where('ID', $req->PARENT_ID)->first();
        // Get Childs (2)
        $getParents = Category::where('ID', $getParent->PARENT_ID)->first();


        $cat = [$req, $getParent, $getParents];
        if ($req->ID == $getParent->ID && $getParent->ID == $getParents->ID) {
            $cat[0]['POSITION'] = 1;
            $cat[1]['POSITION'] = 'DUPLICAT';
            $cat[2]['POSITION'] = 'DUPLICAT';
        } elseif ($getParent->ID == $getParents->ID) {
            $cat[0]['POSITION'] = 2;
            $cat[1]['POSITION'] = 1;
            $cat[2]['POSITION'] = 'DUPLICAT';
        } else {
            $cat[0]['POSITION'] = 3;
            $cat[1]['POSITION'] = 2;
            $cat[2]['POSITION'] = 1;
        }

        return $cat;
    }
}

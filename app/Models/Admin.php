<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Admin extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'admin';
    protected $primaryKey = 'ID';
    protected $fillable = [
        'EMAIL',
        'USER',
        'ROLE',
        'FULLNAME',
        'LAST_LOGIN',
        'PASSWORD',
        'TOKEN_KEY',
        'STATUS'
    ];
    protected $hidden = [
        'PASSWORD',
        'KEY_TOKEN'
    ];
    public $timestamps = false;
    
    public function datatables() {
        $admin = \DB::table('admin')->select('ID', 'EMAIL', 'FULLNAME', 'USER', 'ROLE', 'STATUS')->get();

        return $admin;
    }

    public function find_by_id($id) {
        $admin = \DB::table('admin')->where('ID', $id)->select('ID', 'EMAIL', 'FULLNAME', 'USER', 'ROLE', 'STATUS')->first();

        return $admin;
    }
}

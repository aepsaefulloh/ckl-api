<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class ProductComments extends Model
{
    protected $table = 'product_coments';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function result_by_product($id, $limit) {
        $comment = DB::table('product_coments')->where('UNIQ_CODE', $id)
        ->orderBy('COMMENTS_DATE', 'desc')
        ->take($limit)
        ->get();
        
        if (count($comment) > 0) {
            foreach ($comment as $key => $value) {
                $search_customer = DB::table('customer')->where('ID', $value->CUSTOMER_ID)->first();
    
                // Result Final
                $result[$key]['ID']             = $value->ID;
                $result[$key]['CUSTOMER']       = $value->CUSTOMER_ID;
                $result[$key]['CUSTOMER_NAME']  = ($search_customer) ? $search_customer->FULLNAME : 'Default';
                $result[$key]['COMMENTS']       = $value->COMMENTS;
                $result[$key]['COMMENTS_DATE']  = $value->COMMENTS_DATE;
            }
        } else {
            $result = [];
        }

        return $result;
    }

    public function insert($data) {
        $insert = DB::table('product_coments')->insert($data);

        if ($insert == 1) {
            return 1;
        } else {
            return 0;
        }
    }
}

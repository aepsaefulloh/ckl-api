<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use App\Models\AdditionalImageModel;
use App\Models\Category;
use App\Models\ProductColor;
use App\Models\ProductVideos;
use App\Models\Brand;
use App\Models\Stok;
use DB;

class ProductModel extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'product';
    protected $primaryKey = 'UNIQ_CODE';
    public $timestamps = false;


    // Super Admin CMS
    public function datatables($dir_origin_image, $dir_thumb_image, $type, $id_toko=null) {
        $status = ["","checked=`checked`"];
        
        $getProduct = DB::table('product as product')
        ->select(
            '*',
            DB::raw('
                (CASE 
                    WHEN STATUS = "1" THEN "'.$status[1].'"  
                    ELSE "'.$status[0].'" 
                END) AS CHECKED',
            ),
            DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
            '),
            DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", product.IMAGE) AS IMAGE_THUMB
            ')
        );

        if ($type == 'all') {
            $getProduct->where('STATUS', '<>', 99);
        } elseif ($type == 'delete') {
            $getProduct->where('STATUS', 99);
        }
        $final = $getProduct->orderBy('CREATED_AT', 'desc')->get();
        $result = [];
        foreach ($final as $key => $value) {
            $category = Category::where('ID', $value->CATEGORY)->where('STATUS', 1)->first();
            $brand = Brand::where('ID', $value->BRAND)->where('STATUS', 1)->first();
            $toko = Toko::where('ID', $value->ID_TOKO)->where('STATUS', 1)->first();
            $stok = Stok::where('UNIQ_CODE', $value->UNIQ_CODE)->sum('STOCK_IN');
            // $color = ProductColor::where('UNIQ_CODE', $value->UNIQ_CODE)
            // ->select(
            //     \DB::raw("CONCAT('<span class=".'"'."badge mr-1".'"'." style=".'"'."background-color:', COLOR, '".';"'." title=".'"'."', TITLE, '"  .'"'.">&nbsp;</span>') AS COLOR")
            // )->pluck('COLOR');

            $result[$key]['UNIQ_CODE']              = $value->UNIQ_CODE;
            $result[$key]['NAME_PRODUCT']           = $value->NAME_PRODUCT;
            $result[$key]['DESCRIPTION_PRODUCT']    = $value->DESCRIPTION_PRODUCT;
            $result[$key]['CATEGORY']               = ($category) ? $category->TITLE : '';
            $result[$key]['BRAND']                  = ($brand) ? $brand->NAME_BRAND : '';
            $result[$key]['IMAGE']['ORIGIN']        = $value->IMAGE_ORIGIN;
            $result[$key]['IMAGE']['THUMB']         = $value->IMAGE_THUMB;
            $result[$key]['PRICE']                  = $value->PRICE;
            $result[$key]['WEIGHT']                 = $value->WEIGHT;
            $result[$key]['HIT']                    = $value->HIT;
            $result[$key]['TOKO']                   = ($toko) ? $toko->NAME_TOKO : '';
            $result[$key]['PCS']                    = $value->PCS;
            $result[$key]['STATUS_BARANG']          = $value->STATUS_BARANG;
            $result[$key]['STOK']                   = $stok;
            $result[$key]['SALE']                   = $value->SALE;
            $result[$key]['TAG']                    = $value->TAG;
            $result[$key]['CHECKED']                = $value->CHECKED;
        }

        return $result;
    }

    public function find_data($dir_origin_image, $dir_thumb_image, $id) {
        $this->productVideos = New ProductVideos;
        
        $result = [];
        $product = DB::table('product')->where('UNIQ_CODE', $id)->select(
            '*',
            DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
            '),
            DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", product.IMAGE) AS IMAGE_THUMB
            ')
        )->first();

        if (isset($product)) {
            $find_category = Category::where('ID', $product->CATEGORY)->where('STATUS', '<>', 99)->first();
            $find_brand = Brand::where('ID', $product->BRAND)->where('STATUS', '<>', 99)->first();
            $find_toko = Toko::where('ID', $product->ID_TOKO)->where('STATUS', '<>', 99)->first();
            $find_stok = Stok::where('UNIQ_CODE', $product->UNIQ_CODE)->pluck('STOCK_IN')->first();
            $find_additional_image = DB::table('additional_image')
            ->where('UNIQ_CODE', $product->UNIQ_CODE)
            ->where('STATUS', 1)
            ->select(
                'ID',
                DB::raw("CONCAT('". url($dir_origin_image) ."' '/', '".$product->CATEGORY."', '/', IMAGE) AS IMAGE_ORIGIN"),
                DB::raw("CONCAT('". url($dir_thumb_image) ."' '/', '".$product->CATEGORY."', '/', IMAGE) AS IMAGE_THUMB")
            )->get();
            // $find_color = ProductColor::where('UNIQ_CODE', $product->UNIQ_CODE)->get();
            ## Get Videos Product
            $videos = $this->productVideos->datatables($product->UNIQ_CODE);

            $result['UNIQ_CODE']              = $product->UNIQ_CODE;
            $result['NAME_PRODUCT']           = $product->NAME_PRODUCT;
            $result['DESCRIPTION_PRODUCT']    = $product->DESCRIPTION_PRODUCT;
            $result['ID_CATEGORY']            = $product->CATEGORY;
            $result['CATEGORY']               = ($find_category) ? $find_category->TITLE : '';
            $result['ID_BRAND']               = $product->BRAND;
            $result['BRAND']                  = ($find_brand) ? $find_brand->NAME_BRAND : '';
            $result['IMAGE']['ORIGIN']        = $product->IMAGE_ORIGIN;
            $result['IMAGE']['THUMB']         = $product->IMAGE_THUMB;
            $result['ADDITIONAL_IMAGE']       = $find_additional_image;
            $result['PRICE']                  = $product->PRICE;
            $result['WEIGHT']                 = $product->WEIGHT;
            $result['SALE_PERSEN']            = ($product->DISCOUNT != '') ? $product->DISCOUNT*100 : null;
            $result['SALE_PRICE']             = $product->DISCOUNT*$product->PRICE;
            $result['HIT']                    = $product->HIT;

            // Get Looping Color
            // if (count($find_color) > 0) {
            //     foreach ($find_color as $key => $value) {
            //         $result['COLOR'][$key]['ID_COLOR'] = $value->ID;
            //         $result['COLOR'][$key]['TITLE'] = $value->TITLE;
            //         $result['COLOR'][$key]['COLOR'] = $value->COLOR;
    
            //         // Find Data Stok
            //         $find_stok = Stok::where('PRODUCT_COLOR', $value->ID)->first();
            //         $result['COLOR'][$key]['ID_STOK'] = $find_stok->ID;
            //         $result['COLOR'][$key]['STOK'] = $find_stok->STOCK_IN;
            //     }
            // } else {
            //     $result['COLOR'] = [];
            // }

            $result['ID_TOKO']                = $product->ID_TOKO;
            $result['TOKO']                   = ($find_toko) ? $find_toko->NAME_TOKO : '';
            $result['PROV']                   = ($find_toko) ? $find_toko->PROV_ID : '';
            $result['KAB']                    = ($find_toko) ? $find_toko->KOTA_ID : '';
            $result['ADDRESS']                = ($find_toko) ? $find_toko->ADDRESS : '';
            $result['PCS']                    = $product->PCS;
            $result['STATUS_BARANG']          = $product->STATUS_BARANG;
            $result['SALE']                   = $product->SALE;
            $result['STOK']                   = ($find_stok) ? $find_stok : 0;
            $result['TAG']                    = $product->TAG;
            $result['META_DESC']              = $product->META_DESC;
            $result['VIDEOS']                 = (isset($videos) && count($videos) > 0) ? $videos : [];
            $result['STATUS']                 = $product->STATUS;

            return $result;
        } else {
            return [];
        }
    }

    // Api Public & Private
    public function sale($dir_origin_image, $dir_thumb_image, $sale, $limit) {
        $data = DB::table('product')->where('STATUS', 1)
        ->where('SALE', $sale)
        ->orderBy('CREATED_AT', 'desc')
        ->select(
            '*',
            DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
            '),
            DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", product.IMAGE) AS IMAGE_THUMB
            ')
        )->take($limit)->get();

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                // Get Category Data
                $category = Category::where('ID', $value->CATEGORY)->first();
                
                // Get Brand Data
                $brand = Brand::where('ID', $value->BRAND)->first();

                // Get Color Product
                // $color = ProductColor::where('UNIQ_CODE', $value->UNIQ_CODE)->select('TITLE', 'COLOR')->get();

                // Get Stok
                $stok = Stok::where('UNIQ_CODE', $value->UNIQ_CODE)->pluck('STOCK_IN')->first();

                // Get Toko
                $toko = Toko::where('ID', $value->ID_TOKO)->first();

                // Get Product Terjual
                $product_transaction = DB::table('transaction_product')->where('UNIQ_CODE', $value->UNIQ_CODE)
                ->where('SALE', $sale)
                ->where('STATUS', 1)
                ->count();

                // Result Data Product
                $result[$key]['UNIQ_CODE']              = $value->UNIQ_CODE;
                $result[$key]['NAME_PRODUCT']           = $value->NAME_PRODUCT;
                $result[$key]['DESCRIPTION_PRODUCT']    = $value->DESCRIPTION_PRODUCT;
                $result[$key]['CATEGORY_ID']            = $value->CATEGORY;
                $result[$key]['CATEGORY_NAME']          = $category->TITLE;
                $result[$key]['STATUS_CATEGORY']        = $category->STATUS;
                $result[$key]['BRAND_ID']               = $value->BRAND;
                $result[$key]['BRAND_NAME']             = $brand->NAME_BRAND;
                $result[$key]['STATUS_BRAND']           = $brand->STATUS;
                $result[$key]['IMAGE']['ORIGIN']        = $value->IMAGE_ORIGIN;
                $result[$key]['IMAGE']['THUMB']         = $value->IMAGE_THUMB;
                $result[$key]['SALE']                   = $value->SALE;
                $result[$key]['SALE_PERSEN']            = ($value->DISCOUNT != '') ? $value->DISCOUNT*100 : null;
                $result[$key]['SALE_PRICE']             = $value->DISCOUNT*$value->PRICE;
                $result[$key]['PRICE']                  = $value->PRICE;
                $result[$key]['WEIGHT']                 = $value->WEIGHT;
                $result[$key]['PRICE_DISCOUNT']         = $value->PRICE - ($value->DISCOUNT*$value->PRICE);
                $result[$key]['HIT']                    = $value->HIT;
                $result[$key]['STOK']                   = ($stok) ? $stok : 0;
                $result[$key]['TAG']                    = $value->TAG;
                $result[$key]['ID_TOKO']                = $value->ID_TOKO;
                $result[$key]['TOKO_NAME']              = $toko->NAME_TOKO;
                $result[$key]['PROV']                   = $toko->PROV_ID;
                $result[$key]['KAB']                    = $toko->KOTA_ID;
                $result[$key]['ADDRESS']                = $toko->ADDRESS;
                $result[$key]['STATUS_TOKO']            = $toko->STATUS;
                $result[$key]['PCS']                    = $value->PCS;
                $result[$key]['STATUS_BARANG']          = $value->STATUS_BARANG;
                $result[$key]['CLAIMED_PRODUCT']        = ($product_transaction && $product_transaction > 0) ? $product_transaction : 0;
                $result[$key]['STATUS']                 = $value->STATUS;
                $result[$key]['CREATED_AT']             = $value->CREATED_AT;
                $result[$key]['UPDATED_AT']             = $value->UPDATED_AT;
            }
        } else {
            $result = [];
        }
        // Filtering Data Product
        $final = collect($result)->where('STATUS_BRAND', 1)
        ->where('STATUS_CATEGORY', 1)
        // ->where('STOK', '>', 0)
        ->where('STATUS_TOKO', 1);

        return $final;
    }

    public function popular($dir_origin_image, $dir_thumb_image, $limit) {
        $data = DB::table('product')->where('STATUS', 1)
        ->orderBy('HIT', 'desc')
        ->select(
            '*',
            DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
            '),
            DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", product.IMAGE) AS IMAGE_THUMB
            ')
        )->take($limit)->get();

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                // Get Category Data
                $category = Category::where('ID', $value->CATEGORY)->first();
                
                // Get Brand Data
                $brand = Brand::where('ID', $value->BRAND)->first();

                // Get Color Product
                // $color = ProductColor::where('UNIQ_CODE', $value->UNIQ_CODE)->select('TITLE', 'COLOR')->get();

                // Get Stok
                $stok = Stok::where('UNIQ_CODE', $value->UNIQ_CODE)->pluck('STOCK_IN')->first();

                // Get Toko
                $toko = Toko::where('ID', $value->ID_TOKO)->first();

                // Result Data Product
                $result[$key]['UNIQ_CODE']              = $value->UNIQ_CODE;
                $result[$key]['NAME_PRODUCT']           = $value->NAME_PRODUCT;
                $result[$key]['DESCRIPTION_PRODUCT']    = $value->DESCRIPTION_PRODUCT;
                $result[$key]['CATEGORY_ID']            = $value->CATEGORY;
                $result[$key]['CATEGORY_NAME']          = $category->TITLE;
                $result[$key]['STATUS_CATEGORY']        = $category->STATUS;
                $result[$key]['BRAND_ID']               = $value->BRAND;
                $result[$key]['BRAND_NAME']             = $brand->NAME_BRAND;
                $result[$key]['STATUS_BRAND']           = $brand->STATUS;
                $result[$key]['IMAGE']['ORIGIN']        = $value->IMAGE_ORIGIN;
                $result[$key]['IMAGE']['THUMB']         = $value->IMAGE_THUMB;
                $result[$key]['SALE']                   = $value->SALE;
                $result[$key]['SALE_PERSEN']            = ($value->DISCOUNT != '') ? $value->DISCOUNT*100 : null;
                $result[$key]['SALE_PRICE']             = $value->DISCOUNT*$value->PRICE;
                $result[$key]['PRICE']                  = $value->PRICE;
                $result[$key]['WEIGHT']                 = $value->WEIGHT;
                $result[$key]['PRICE_DISCOUNT']         = $value->PRICE - ($value->DISCOUNT*$value->PRICE);
                $result[$key]['HIT']                    = $value->HIT;
                $result[$key]['STOK']                   = ($stok) ? $stok : 0;
                $result[$key]['TAG']                    = $value->TAG;
                $result[$key]['ID_TOKO']                = $value->ID_TOKO;
                $result[$key]['TOKO_NAME']              = $toko->NAME_TOKO;
                $result[$key]['PROV']                   = $toko->PROV_ID;
                $result[$key]['KAB']                    = $toko->KOTA_ID;
                $result[$key]['ADDRESS']                = $toko->ADDRESS;
                $result[$key]['STATUS_TOKO']            = $toko->STATUS;
                $result[$key]['PCS']                    = $value->PCS;
                $result[$key]['STATUS_BARANG']          = $value->STATUS_BARANG;
                $result[$key]['STATUS']                 = $value->STATUS;
                $result[$key]['CREATED_AT']             = $value->CREATED_AT;
                $result[$key]['UPDATED_AT']             = $value->UPDATED_AT;
            }
        } else {
            $result = [];
        }
        
        // Filtering Data Product
        $final = collect($result)->where('STATUS_BRAND', 1)
        ->where('STATUS_CATEGORY', 1)
        // ->where('STOK', '>', 0)
        ->where('STATUS_TOKO', 1);

        return $final;
    }

    public function news_data($dir_origin_image, $dir_thumb_image, $limit) {
        $data = DB::table('product')->where('STATUS', 1)
        ->orderBy('CREATED_AT', 'desc')
        ->select(
            '*',
            DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
            '),
            DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", product.IMAGE) AS IMAGE_THUMB
            ')
        )->take($limit)->get();

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                // Get Category Data
                $category = Category::where('ID', $value->CATEGORY)->first();

                // Get Brand Data
                $brand = Brand::where('ID', $value->BRAND)->first();

                // Get Color Product
                // $color = ProductColor::where('UNIQ_CODE', $value->UNIQ_CODE)->select('TITLE', 'COLOR')->get();

                // Get Stok
                $stok = Stok::where('UNIQ_CODE', $value->UNIQ_CODE)->pluck('STOCK_IN')->first();

                // Get Toko
                $toko = Toko::where('ID', $value->ID_TOKO)->first();
    
                // Result Data Product
                $result[$key]['UNIQ_CODE']              = $value->UNIQ_CODE;
                $result[$key]['NAME_PRODUCT']           = $value->NAME_PRODUCT;
                $result[$key]['DESCRIPTION_PRODUCT']    = $value->DESCRIPTION_PRODUCT;
                $result[$key]['CATEGORY_ID']            = $value->CATEGORY;
                $result[$key]['CATEGORY_NAME']          = $category->TITLE;
                $result[$key]['STATUS_CATEGORY']        = $category->STATUS;
                $result[$key]['BRAND_ID']               = $value->BRAND;
                $result[$key]['BRAND_NAME']             = $brand->NAME_BRAND;
                $result[$key]['STATUS_BRAND']           = $brand->STATUS;
                $result[$key]['IMAGE']['ORIGIN']        = $value->IMAGE_ORIGIN;
                $result[$key]['IMAGE']['THUMB']         = $value->IMAGE_THUMB;
                $result[$key]['SALE']                   = $value->SALE;
                $result[$key]['SALE_PERSEN']            = ($value->DISCOUNT != '') ? $value->DISCOUNT*100 : null;
                $result[$key]['SALE_PRICE']             = $value->DISCOUNT*$value->PRICE;
                $result[$key]['PRICE']                  = $value->PRICE;
                $result[$key]['WEIGHT']                 = $value->WEIGHT;
                $result[$key]['PRICE_DISCOUNT']         = $value->PRICE - ($value->DISCOUNT*$value->PRICE);
                $result[$key]['HIT']                    = $value->HIT;
                $result[$key]['STOK']                   = ($stok) ? $stok : 0;
                $result[$key]['TAG']                    = $value->TAG;
                $result[$key]['ID_TOKO']                = $value->ID_TOKO;
                $result[$key]['TOKO_NAME']              = $toko->NAME_TOKO;
                $result[$key]['PROV']                   = $toko->PROV_ID;
                $result[$key]['KAB']                    = $toko->KOTA_ID;
                $result[$key]['ADDRESS']                = $toko->ADDRESS;
                $result[$key]['STATUS_TOKO']            = $toko->STATUS;
                $result[$key]['PCS']                    = $value->PCS;
                $result[$key]['STATUS_BARANG']          = $value->STATUS_BARANG;
                $result[$key]['STATUS']                 = $value->STATUS;
                $result[$key]['CREATED_AT']             = $value->CREATED_AT;
                $result[$key]['UPDATED_AT']             = $value->UPDATED_AT;   
            }
        } else {
            $result = [];
        }
        
        // Filtering Data Product
        $final = collect($result)->where('STATUS_BRAND', 1)
        ->where('STATUS_CATEGORY', 1)
        // ->where('STOK', '>', 0)
        ->where('STATUS_TOKO', 1);

        return $final;
    }

    public function all_data($type, $dir_origin, $dir_thumb, $keyword, $brand, $category, $page, $per_page, $price_awal, $price_akhir) {
        // Find Brand By Request Filter
        $search_brand = Brand::where('STATUS', 1)->where('SEO', $brand)->pluck('ID')->first();
        $id_brand = ($search_brand != '') ? $search_brand : null ;

        if ($type == 'array') {
            // Get Data Product
            $product = DB::table('product')
            ->where('STATUS', 1)
            ->where('NAME_PRODUCT', 'LIKE', "%".$keyword."%")
            ->where('BRAND', 'LIKE', $id_brand)
            ->where('CATEGORY', 'LIKE', $category)
            ->get();

            if (count($product) > 0) {
                foreach ($product as $key => $value) {
                    // Get Category
                    $category = Category::where('ID', $value->CATEGORY)
                    ->first();
                    
                    // Get Brand
                    $brand = Brand::where('ID', $value->BRAND)
                    ->first();

                    // Get Stok
                    $stok = Stok::where('UNIQ_CODE', $value->UNIQ_CODE)->pluck('STOCK_IN')->first();
                    
                    // Get Additional Image
                    $add_image = AdditionalImageModel::where('UNIQ_CODE', $value->UNIQ_CODE)
                    ->select(
                        'ID', 
                        DB::raw("CONCAT('". url($dir_origin . $value->CATEGORY) ."', '/', IMAGE) AS IMAGE_ORIGIN"),
                        DB::raw("CONCAT('". url($dir_thumb . $value->CATEGORY) ."', '/', IMAGE) AS IMAGE_THUMB"),
                    )
                    ->get();
        
                    // Get Toko
                    $toko = Toko::where('ID', $value->ID_TOKO)->first();
        
                    // Result
                    $result[$key]['UNIQ_CODE']      = $value->UNIQ_CODE;
                    $result[$key]['NAME_PRODUCT']   = $value->NAME_PRODUCT;
                    $result[$key]['DESCRIPTION']    = $value->DESCRIPTION_PRODUCT;
                    $result[$key]['CATEGORY_ID']    = $value->CATEGORY;
                    $result[$key]['CATEGORY_NAME']  = $category->TITLE;
                    $result[$key]['STATUS_CATEGORY']= $category->STATUS;
                    $result[$key]['BRAND_ID']       = $value->BRAND;
                    $result[$key]['BRAND_NAME']     = $brand->NAME_BRAND;
                    $result[$key]['STATUS_BRAND']   = $brand->STATUS;
                    $result[$key]['IMAGE']['ORIGIN']= url($dir_origin.$value->CATEGORY.'/'.$value->IMAGE);
                    $result[$key]['IMAGE']['THUMB'] = url($dir_thumb.$value->CATEGORY.'/'.$value->IMAGE);
                    $result[$key]['ADD_IMAGE']      = $add_image;
                    $result[$key]['SALE']           = $value->SALE;
                    $result[$key]['SALE_PERSEN']    = ($value->DISCOUNT != '') ? $value->DISCOUNT*100 : null;
                    $result[$key]['SALE_PRICE']     = $value->DISCOUNT*$value->PRICE;
                    $result[$key]['PRICE']          = $value->PRICE;
                    $result[$key]['WEIGHT']         = $value->WEIGHT;
                    $result[$key]['PRICE_DISCOUNT'] = $value->PRICE - ($value->DISCOUNT*$value->PRICE);
                    $result[$key]['HIT']            = $value->HIT;
                    $result[$key]['ID_TOKO']        = $value->ID_TOKO;
                    $result[$key]['STOK']           = ($stok) ? $stok : 0;
                    $result[$key]['TAG']            = $value->TAG;
                    $result[$key]['TOKO_NAME']      = $toko->NAME_TOKO;
                    $result[$key]['PROV']           = $toko->PROV_ID;
                    $result[$key]['KAB']            = $toko->KOTA_ID;
                    $result[$key]['ADDRESS']        = $toko->ADDRESS;
                    $result[$key]['STATUS_TOKO']    = $toko->STATUS;
                    $result[$key]['PCS']            = $value->PCS;
                    $result[$key]['STATUS_BARANG']  = $value->STATUS_BARANG;
                    $result[$key]['STATUS']         = $value->STATUS;
                    $result[$key]['CREATE_DATE']    = $value->CREATED_AT;
                }
            } else {
                $result = [];
            }
        } elseif ($type == 'count') {
            // Get Data Product
            $product = DB::table('product')
            ->where('STATUS', 1)
            ->where('NAME_PRODUCT', 'LIKE', "%".$keyword."%")
            ->where('BRAND', 'LIKE', $id_brand)
            ->where('CATEGORY', 'LIKE', $category)
            ->get();

            if (count($product) > 0) {
                foreach ($product as $key => $value) {
                    // Get Category
                    $category = Category::where('ID', $value->CATEGORY)
                    ->first();
                    
                    // Get Brand
                    $brand = Brand::where('ID', $value->BRAND)
                    ->first();
                    
                    // Get Toko
                    $toko = Toko::where('ID', $value->ID_TOKO)->first();
    
                     // Result
                     $result[$key]['UNIQ_CODE']      = $value->UNIQ_CODE;
                     $result[$key]['NAME_PRODUCT']   = $value->NAME_PRODUCT;
                     $result[$key]['DESCRIPTION']    = $value->DESCRIPTION_PRODUCT;
                     $result[$key]['CATEGORY_ID']    = $value->CATEGORY;
                     $result[$key]['CATEGORY_NAME']  = $category->TITLE;
                     $result[$key]['STATUS_CATEGORY']= $category->STATUS;
                     $result[$key]['BRAND_ID']       = $value->BRAND;
                     $result[$key]['BRAND_NAME']     = $brand->NAME_BRAND;
                     $result[$key]['STATUS_BRAND']   = $brand->STATUS;
                     $result[$key]['SALE']           = $value->SALE;
                     $result[$key]['SALE_PERSEN']    = ($value->DISCOUNT != '') ? $value->DISCOUNT*100 : null;
                     $result[$key]['SALE_PRICE']     = $value->DISCOUNT*$value->PRICE;
                     $result[$key]['PRICE']          = $value->PRICE;
                     $result[$key]['WEIGHT']         = $value->WEIGHT;
                     $result[$key]['PRICE_DISCOUNT'] = $value->PRICE - ($value->DISCOUNT*$value->PRICE);
                     $result[$key]['HIT']            = $value->HIT;
                     $result[$key]['ID_TOKO']        = $value->ID_TOKO;
                     $result[$key]['TOKO_NAME']      = $toko->NAME_TOKO;
                     $result[$key]['PROV']           = $toko->PROV_ID;
                     $result[$key]['KAB']            = $toko->KOTA_ID;
                     $result[$key]['ADDRESS']        = $toko->ADDRESS;
                     $result[$key]['STATUS_TOKO']    = $toko->STATUS;
                     $result[$key]['PCS']            = $value->PCS;
                     $result[$key]['STATUS_BARANG']  = $value->STATUS_BARANG;
                     $result[$key]['STATUS']         = $value->STATUS;
                     $result[$key]['CREATE_DATE']    = $value->CREATED_AT;
                }
            } else {
                $result = [];
            }
        }
        // Filtering Data Status
        if ($price_awal != '' && $price_akhir != '') {
            $final = collect($result)->where('STATUS_BRAND', 1)
            ->where('STATUS_CATEGORY', 1)
            ->where('STATUS_TOKO', 1)
            // ->where('STOK', '>', 0)
            ->whereBetween('PRICE', [$price_awal."%", $price_akhir."%"])
            ->sort()
            ->values()
            ->all();
        } else {
            $final = collect($result)->where('STATUS_BRAND', 1)
            ->where('STATUS_CATEGORY', 1)
            ->where('STATUS_TOKO', 1)
            // ->where('STOK', '>', 0)
            ->sort()
            ->values()
            ->all();
        }

        return ($type == 'array') ? $finals = collect($final)->forPage((int) $page, (int) $per_page)->all() : $finals = collect($final)->count();
    }

    public function hit_top($dir_origin_image, $dir_thumb_image, $limit) {
        $data = DB::table('product')->where('STATUS', 1)
        ->orderBy('HIT', 'desc')
        ->select(
            '*',
            DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
            '),
            DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", product.IMAGE) AS IMAGE_THUMB
            ')
        )->take($limit)->get();

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                // Get Category Data
                $category = Category::where('ID', $value->CATEGORY)->first();

                // Get Brand Data
                $brand = Brand::where('ID', $value->BRAND)->first();

                // Get Color Product
                // $color = ProductColor::where('UNIQ_CODE', $value->UNIQ_CODE)->select('TITLE', 'COLOR')->get();

                // Get Stok
                $stok = Stok::where('UNIQ_CODE', $value->UNIQ_CODE)->pluck('STOCK_IN')->first();

                // Get Toko
                $toko = Toko::where('ID', $value->ID_TOKO)->first();
    
                // Result Data Product
                $result[$key]['UNIQ_CODE']              = $value->UNIQ_CODE;
                $result[$key]['NAME_PRODUCT']           = $value->NAME_PRODUCT;
                $result[$key]['DESCRIPTION_PRODUCT']    = $value->DESCRIPTION_PRODUCT;
                $result[$key]['CATEGORY_ID']            = $value->CATEGORY;
                $result[$key]['CATEGORY_NAME']          = $category->TITLE;
                $result[$key]['STATUS_CATEGORY']        = $category->STATUS;
                $result[$key]['BRAND_ID']               = $value->BRAND;
                $result[$key]['BRAND_NAME']             = $brand->NAME_BRAND;
                $result[$key]['STATUS_BRAND']           = $brand->STATUS;
                $result[$key]['IMAGE']['ORIGIN']        = $value->IMAGE_ORIGIN;
                $result[$key]['IMAGE']['THUMB']         = $value->IMAGE_THUMB;
                $result[$key]['SALE']                   = $value->SALE;
                $result[$key]['SALE_PERSEN']            = ($value->DISCOUNT != '') ? $value->DISCOUNT*100 : null;
                $result[$key]['SALE_PRICE']             = $value->DISCOUNT*$value->PRICE;
                $result[$key]['PRICE']                  = $value->PRICE;
                $result[$key]['WEIGHT']                 = $value->WEIGHT;
                $result[$key]['PRICE_DISCOUNT']         = $value->PRICE - ($value->DISCOUNT*$value->PRICE);
                $result[$key]['HIT']                    = $value->HIT;
                $result[$key]['STOK']                   = ($stok) ? $stok : 0;
                $result[$key]['TAG']                    = $value->TAG;
                $result[$key]['ID_TOKO']                = $value->ID_TOKO;
                $result[$key]['TOKO_NAME']              = $toko->NAME_TOKO;
                $result[$key]['PROV']                   = $toko->PROV_ID;
                $result[$key]['KAB']                    = $toko->KOTA_ID;
                $result[$key]['ADDRESS']                = $toko->ADDRESS;
                $result[$key]['STATUS_TOKO']            = $toko->STATUS;
                $result[$key]['PCS']                    = $value->PCS;
                $result[$key]['STATUS_BARANG']          = $value->STATUS_BARANG;
                $result[$key]['STATUS']                 = $value->STATUS;
                $result[$key]['CREATED_AT']             = $value->CREATED_AT;
                $result[$key]['UPDATED_AT']             = $value->UPDATED_AT;   
            }
        } else {
            $result = [];
        }
        
        // Filtering Data Product
        $final = collect($result)->where('STATUS_BRAND', 1)
        ->where('STATUS_CATEGORY', 1)
        // ->where('STOK', '>', 0)
        ->where('STATUS_TOKO', 1);

        return $final;
    }

    public function related_category_product($dir_origin_image, $dir_thumb_image, $category_id, $product_id, $limit) {
        $data = DB::table('product')->where('STATUS', 1)
        ->where('CATEGORY', $category_id)
        ->where('UNIQ_CODE', '<>', $product_id)
        ->orderBy('CREATED_AT', 'desc')
        ->select(
            '*',
            DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
            '),
            DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", product.IMAGE) AS IMAGE_THUMB
            ')
        )->take($limit)->get();

        if (count($data) > 0) {
            foreach ($data as $key => $value) {
                // Get Category Data
                $category = Category::where('ID', $value->CATEGORY)->first();
                
                // Get Brand Data
                $brand = Brand::where('ID', $value->BRAND)->first();

                // Get Color Product
                // $color = ProductColor::where('UNIQ_CODE', $value->UNIQ_CODE)->select('TITLE', 'COLOR')->get();

                // Get Stok
                $stok = Stok::where('UNIQ_CODE', $value->UNIQ_CODE)->pluck('STOCK_IN')->first();

                // Get Toko
                $toko = Toko::where('ID', $value->ID_TOKO)->first();

                // Result Data Product
                $result[$key]['UNIQ_CODE']              = $value->UNIQ_CODE;
                $result[$key]['NAME_PRODUCT']           = $value->NAME_PRODUCT;
                $result[$key]['DESCRIPTION_PRODUCT']    = $value->DESCRIPTION_PRODUCT;
                $result[$key]['CATEGORY_ID']            = $value->CATEGORY;
                $result[$key]['CATEGORY_NAME']          = $category->TITLE;
                $result[$key]['STATUS_CATEGORY']        = $category->STATUS;
                $result[$key]['BRAND_ID']               = $value->BRAND;
                $result[$key]['BRAND_NAME']             = $brand->NAME_BRAND;
                $result[$key]['STATUS_BRAND']           = $brand->STATUS;
                $result[$key]['IMAGE']['ORIGIN']        = $value->IMAGE_ORIGIN;
                $result[$key]['IMAGE']['THUMB']         = $value->IMAGE_THUMB;
                $result[$key]['SALE']                   = $value->SALE;
                $result[$key]['SALE_PERSEN']            = ($value->DISCOUNT != '') ? $value->DISCOUNT*100 : null;
                $result[$key]['SALE_PRICE']             = $value->DISCOUNT*$value->PRICE;
                $result[$key]['PRICE']                  = $value->PRICE;
                $result[$key]['WEIGHT']                 = $value->WEIGHT;
                $result[$key]['PRICE_DISCOUNT']         = $value->PRICE - ($value->DISCOUNT*$value->PRICE);
                $result[$key]['HIT']                    = $value->HIT;
                $result[$key]['STOK']                   = ($stok) ? $stok : 0;
                $result[$key]['TAG']                    = $value->TAG;
                $result[$key]['ID_TOKO']                = $value->ID_TOKO;
                $result[$key]['TOKO_NAME']              = $toko->NAME_TOKO;
                $result[$key]['PROV']                   = $toko->PROV_ID;
                $result[$key]['KAB']                    = $toko->KOTA_ID;
                $result[$key]['ADDRESS']                = $toko->ADDRESS;
                $result[$key]['PROV']                   = $toko->PROV_ID;
                $result[$key]['KAB']                    = $toko->KOTA_ID;
                $result[$key]['ADDRESS']                = $toko->ADDRESS;
                $result[$key]['STATUS_TOKO']            = $toko->STATUS;
                $result[$key]['PCS']                    = $value->PCS;
                $result[$key]['STATUS_BARANG']          = $value->STATUS_BARANG;
                $result[$key]['STATUS']                 = $value->STATUS;
                $result[$key]['CREATED_AT']             = $value->CREATED_AT;
                $result[$key]['UPDATED_AT']             = $value->UPDATED_AT;
            }
        } else {
            $result = [];
        }
        
        // Filtering Data Product
        $final = collect($result)->where('STATUS_BRAND', 1)
        ->where('STATUS_CATEGORY', 1)
        // ->where('STOK', '>', 0)
        ->where('STATUS_TOKO', 1);

        return $final;
    }
}
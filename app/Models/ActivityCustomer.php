<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityCustomer extends Model
{
    protected $table = 'activity_customer';
    public $timestamps = false;
}

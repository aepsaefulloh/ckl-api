<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subscribe extends Model
{
    protected $table = 'subscribe_form';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function insert($data) {
        $insert = \DB::table('subscribe_form')->insert($data);

        if ($insert == 1) {
            return 1;
        } else {
            return 0;
        }
    }
}

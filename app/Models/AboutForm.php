<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class AboutForm extends Model
{
    protected $table = 'about_form';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function insert($data) {
        $insert = DB::table('about_form')->insert($data);

        if ($insert == 1) {
            return 1;
        } else {
            return 0;
        }
    }
}

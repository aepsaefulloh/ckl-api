<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Stok extends Model
{
    protected $table = 'stok';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function find_data($id) {
        $data = Stok::where('UNIQ_CODE', $id)->first();

        return $data;
    }

    public function store($tipe, $data, $id=null) {
        $post = \DB::table('stok');

        if ($tipe == "update") {
            $post->where('UNIQ_CODE', $id)
            ->update($data);
        } elseif ($tipe == 'add') {
            $post->insert($data);
        }

        return $post;
        
    }
}

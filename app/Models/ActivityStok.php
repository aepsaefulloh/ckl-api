<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ActivityStok extends Model
{
    protected $table = 'activity_stok';
    protected $primaryKey = 'ID';
    public $timestamps = false;
}

<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use DB;

class AdditionalImageModel extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = 'additional_image';
    protected $primaryKey = 'ID';
    public $timestamps = false;
    
    public function store($uniq_code, $image, $status) {
        $post = DB::table('additional_image')->insert([
            "UNIQ_CODE" => $uniq_code,
            "IMAGE"     => $image,
            "STATUS"    => $status
        ]);

        return $post;
    }

    public function get_data($id) {
        $get = DB::table('additional_image')->where('UNIQ_CODE', $id)->where('STATUS', 1)->get();

        return $get;
    }

    
    public function updates($id, $data){
        $post = DB::table('additional_image')->where($id)->update($data);
 
        return $post;
    }
}

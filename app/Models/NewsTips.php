<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;
use App\Models\Category;

class NewsTips extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'news_and_tips';
    protected $primaryKey = 'ID';
    protected $fillable = [
        'TITLE',
        'SUMMARY',
        'CATEGORY',
        'CONTENT',
        'IMAGE',
        'CAPTION',
        'HIT',
        'PUBLISH_TIMESTAMP',
        'STATUS'
    ];

    public $timestamps = false;

    public function datatables($dir_origin_image, $dir_thumb_image, $type) {
        $status = [
            " ",
            "checked='checked'"
        ];

        // Find Category
        if ($type == "delete") {
            $get = NewsTips::where('STATUS', 99);
        } elseif ($type == 'all') {
            $get = NewsTips::where('STATUS', '<>', 99);
        }

        $result = $get->select(
            'ID as id',
            'TITLE AS title',
            'SUMMARY as summary',
            'CATEGORY as category',
            'CONTENT as content',
            \DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS image_origin
            '),
            \DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", IMAGE) AS image_thumb
            '),
            'CAPTION as caption',
            'HIT as hit',
            'PUBLISH_TIMESTAMP as publish_time',
            'STATUS as status',
            \DB::raw('(CASE WHEN STATUS = 1 THEN "' . $status[1] . '" ELSE "'. $status[0] .'" END) AS checked') 
        )->get();

        if (count($result) > 0) {
            foreach ($result as $key => $val) {
                $findCategory = Category::where('ID', $val->category)->first();
                $return[$key] = $val;
                $return[$key]['key_category'] = $findCategory->ID;
                $return[$key]['category'] = $findCategory->TITLE;
            }
        } else {
            $return = [];
        }
        
        return $return;
    }

    public function find_Data($dir_origin_image, $dir_thumb_image, $id) {
        $result = NewsTips::where('ID', $id)->select(
            'ID as id',
            'TITLE AS title',
            'SUMMARY as summary',
            'CATEGORY as category',
            'CONTENT as content',
            'IMAGE as image',
            \DB::raw('
                CONCAT("'.url().'/'.$dir_origin_image.'", CATEGORY, "/", IMAGE) AS image_origin
            '),
            \DB::raw('
                CONCAT("'.url().'/'.$dir_thumb_image.'", CATEGORY, "/", IMAGE) AS image_thumb
            '),
            'EMBLED as embled_yt',
            'CAPTION as caption',
            'TAG as tag',
            'META_DESC',
            'HIT as hit',
            'PUBLISH_TIMESTAMP as publish_time',
            'STATUS as status'
        )->first();

        return $result;
    }

    public function delete_data($id) {
        $post = NewsTips::where('ID', $id)->first();
        $post->STATUS = 99;
        $post->save();

        return $post;
    }

    // API Public (Customer)
    public function find_category_review_product($val) {
        $data = Category::where('SEO', $val)
        ->where('TIPE', 'news_tips')
        ->where('STATUS', 1)
        ->select('ID', 'TITLE')
        ->first();

        return $data;
    }

    public function review_data($dir_origin_image, $dir_thumb_image, $limit) {
        // Find Category Review Product
        $find_category = $this->find_category_review_product('review-product');
        $id_category = ($find_category) ? $find_category->ID : 0 ;
        
        if ($find_category) {
            // Find Product By Category Review Product
            $data =  NewsTips::where('CATEGORY', $id_category)
            ->where('STATUS', 1)
            ->orderBy('PUBLISH_TIMESTAMP', 'desc')
            ->select(
                '*',
                \DB::raw('CONCAT("'.$find_category->TITLE.'") AS CATEGORY'),
                \DB::raw('
                    CONCAT("'.url().'/'.$dir_origin_image.'", "/", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN
                '),
                \DB::raw('
                    CONCAT("'.url().'/'.$dir_thumb_image.'", "/", CATEGORY, "/", IMAGE) AS IMAGE_THUMB
                '),
                \DB::raw('CONCAT("active") AS STATUS')
            )->take($limit)->get();
        } else {
            $data = [];
        }
        return $data;
    }

    public function news_and_popular($type, $dir_origin, $dir_thumb, $limit) {
        // Find Category Review Product
        $find_review_category = $this->find_category_review_product('review-product');
        $id_category = ($find_review_category) ? $find_review_category->ID : 0 ;

        // Data News Tips
        $data = NewsTips::where('STATUS', 1)
        ->where('CATEGORY', '<>', $id_category)
        ->select(
            '*',
            \DB::raw('CONCAT("'. url($dir_origin) .'", "/", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN'),    
            \DB::raw('CONCAT("'. url($dir_thumb) .'", "/", CATEGORY, "/", IMAGE) AS IMAGE_THUMB'),    
        )
        ->orderBy($type, 'desc')
        ->limit($limit)
        ->get();

        if (count($data) > 0) {
            foreach ($data as $key => $val) {
                // Get Category By News Tips Data
                $category = Category::where('ID', $val['CATEGORY'])->first();

                $result[$key]['ID']                 = $val['ID'];
                $result[$key]['TITLE']              = $val['TITLE'];
                $result[$key]['SUMMARY']            = $val['SUMMARY'];
                $result[$key]['ID_CATEGORY']        = $category->ID;
                $result[$key]['CATEGORY_NAME']      = $category->TITLE;
                $result[$key]['CATEGORY_STATUS']    = $category->STATUS;
                $result[$key]['CONTENT']            = $val['CONTENT'];
                $result[$key]['IMAGE']['ORIGIN']    = $val['IMAGE_ORIGIN'];
                $result[$key]['IMAGE']['THUMB']     = $val['IMAGE_THUMB'];
                $result[$key]['CAPTION']            = $val['CAPTAON'];
                $result[$key]['HIT']                = $val['HIT'];
                $result[$key]['PUBLISH_TIMESTAMP']  = $val['PUBLISH_TIMESTAMP'];
                $result[$key]['STATUS']             = $val['STATUS'];
            }
        } else {
            $result = [];
        }
        
        // FILTER
        $final = collect($result)->where('CATEGORY_STATUS', 1);

        return $result;
    }

    public function all_data($type, $dir_origin, $dir_thumb, $limit, $page) {
        // Find Category Review Product
        $find_review_category = $this->find_category_review_product('review-product');
        $id_category = ($find_review_category) ? $find_review_category->ID : 0 ;

        if ($type == 'array') {
            // Data News Tips
            $data = NewsTips::where('STATUS', 1)
            ->where('CATEGORY', '<>', $id_category)
            ->select(
                '*',
                \DB::raw('CONCAT("'. url($dir_origin) .'", "/", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN'),    
                \DB::raw('CONCAT("'. url($dir_thumb) .'", "/", CATEGORY, "/", IMAGE) AS IMAGE_THUMB'),    
            )
            ->orderBy('PUBLISH_TIMESTAMP', 'desc')
            ->forPage($page, $limit)
            ->get();

            if (count($data) > 0) {
                foreach ($data as $key => $val) {
                    // Get Category By News Tips Data
                    $category = Category::where('ID', $val['CATEGORY'])->first();

                    $result[$key]['ID']                 = $val['ID'];
                    $result[$key]['TITLE']              = $val['TITLE'];
                    $result[$key]['SUMMARY']            = $val['SUMMARY'];
                    $result[$key]['ID_CATEGORY']        = $category->ID;
                    $result[$key]['CATEGORY_NAME']      = $category->TITLE;
                    $result[$key]['CATEGORY_STATUS']    = $category->STATUS;
                    $result[$key]['CONTENT']            = $val['CONTENT'];
                    $result[$key]['IMAGE']['ORIGIN']    = $val['IMAGE_ORIGIN'];
                    $result[$key]['IMAGE']['THUMB']     = $val['IMAGE_THUMB'];
                    $result[$key]['CAPTION']            = $val['CAPTAON'];
                    $result[$key]['HIT']                = $val['HIT'];
                    $result[$key]['PUBLISH_TIMESTAMP']  = $val['PUBLISH_TIMESTAMP'];
                    $result[$key]['STATUS']             = $val['STATUS'];
                }
            } else {
                $result = [];
            }
            
            // FILTER
            $final = collect($result)->where('CATEGORY_STATUS', 1);

            return $result;
        } elseif ($type == 'count') {
            $data = NewsTips::where('STATUS', 1)
            ->where('CATEGORY', '<>', $id_category)
            ->count();

            return $data;
        }
    }

    public function search_by_title($keyword) {
        $data = \DB::table('news_and_tips')->where('TITLE', $keyword)->first();

        return $data;
    }

    public function artikel_product($tag, $dir_origin_image, $dir_thumb_image, $limit) {
        $article = \DB::table('news_and_tips')->where('TAG', $tag)->where('STATUS', 1)->select(
            'SLUG',
            'TITLE',
            \DB::raw('CONCAT("'. url($dir_origin_image) .'", "/", CATEGORY, "/", IMAGE) AS IMAGE_ORIGIN'),
            \DB::raw('CONCAT("'. url($dir_thumb_image) .'", "/", CATEGORY, "/", IMAGE) AS IMAGE_THUMB'),
            'HIT',
            'PUBLISH_TIMESTAMP'
        )->orderBy('ID', 'desc')->limit($limit)->get();
        return $article;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use \Mailjet\Resources;

class MailJet extends Model
{
    public function send($body) {
        // Connect to MailJet
        $mj = new \Mailjet\Client(env('MAILJET_KEY'), env('MAILJET_SECRET'), true, ['version' => env('MAILJET_VERSION')]);
        $response = $mj->post(Resources::$Email, ['body' => $body]);

        return $response->success();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddCartProduct extends Model
{
    protected $table = 'add_cart_product';
    protected $primaryKey = 'ID';
    public $timestamps = false;


    public function result_by_customer($data) {
        ## Filter Request data 
        if (count($data) > 0) {
            ## Looping Data Request
            foreach ($data as $key => $value) {
                $results[$key] = \DB::table('add_cart_product')->where('CUSTOMER_ID', $value['CUSTOMER_ID'])
                ->where('UNIQ_CODE', $value['UNIQ_CODE'])
                ->where('STATUS', 0)
                ->first();
            }
            ## Filtering Data Jika ada yang null
            $result = array_filter($results);
        } else {
            $result = [];
        }

        return $result;
    }

    public function insert($data, $customer_id) {
        $store = \DB::table('add_cart_product')->insert($data);

        return $store;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use DB;

class Banner extends Model
{
    protected $table = 'banner';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    // Superadmin
    public function datatables($type) {
        $status = [
            " ",
            "checked='checked'"
        ];

        if ($type == "delete") {
            $get = Banner::where('STATUS', 99);
        } else {
            $get = Banner::where('STATUS', '<>', 99);
        }

        $get->select(
            'ID as id',
            'CATEGORY as category',
            \DB::raw('CONCAT("'.url().'/storage/images/banner/origin/'.'", IMAGE) AS image_origin'),
            \DB::raw('CONCAT("'.url().'/storage/images/banner/thumb/'.'", IMAGE) AS image_thumb'),
            'TITLE as title',
            'CAPTION as caption',
            'LINK as link',
            'POSITION as position',
            'PUBLISH_DATE as publish_date',
            'UPDATED_DATE as updated_date',
            \DB::raw('(CASE WHEN STATUS = 1 THEN "' . $status[1] . '" ELSE "'. $status[0] .'" END) AS checked') 
        );

        return $get->get();
        
    }

    public function find_data($dir_banner_origin, $dir_banner_thumb, $id) {
        $getData = DB::table('banner')
        ->select(
            'ID as id',
            'CATEGORY as category',
            'IMAGE as image',
            DB::raw('CONCAT("'.url().'/'.$dir_banner_origin.'", IMAGE) AS image_origin'),
            DB::raw('CONCAT("'.url().'/'.$dir_banner_thumb.'", IMAGE) AS image_thumb'),
            'TITLE AS title',
            'CAPTION AS caption',
            'LINK AS url',
            'POSITION as posisi',
            'STATUS AS status',
            'PUBLISH_DATE AS publish',
            'UPDATED_DATE AS update_date'
        )
        ->where('ID', $id)
        ->first();

        return $getData;
    }


    public function get_all($dir_banner_origin, $dir_banner_thumb, $publish=99, $type) {
        ## Definisi $type 
            //    - banner
            //    - event
            //    - highlighter
            
        $getData = DB::table('banner')->where('CATEGORY', $type)
        ->select(
            'ID as id',
            'CATEGORY as category',
            DB::raw('CONCAT("'.url().'/'.$dir_banner_origin.'", IMAGE) AS image_origin'),
            DB::raw('CONCAT("'.url().'/'.$dir_banner_thumb.'", IMAGE) AS image_thumb'),
            'TITLE AS title',
            'CAPTION AS caption',
            'LINK AS url',
            'POSITION as posisi',
            'STATUS AS status',
            'PUBLISH_DATE AS publish',
        )->orderBy('POSITION', 'asc');
        if ($publish == 1) {
            $result = $getData->where('STATUS', $publish)->get();
        } elseif ($publish == 99) {
            $result = $getData->where('STATUS', '<>', $publish)->get();
        }
        
        return $result;
    }
}

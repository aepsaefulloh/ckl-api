<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Admin;

class ActivityAdmin extends Model
{
    protected $table = 'activity_admin';
    protected $primaryKey = 'ID';
    protected $fillable = [
        'IP_ADDRESS',
        'ADMIN_ID',
        'LOGIN_DATE',
    ];
    public $timestamps = false;

    public function activity_me($id) {
        $akun_admin = Admin::where('ID', $id)->first();
        $admin = ActivityAdmin::where('ADMIN_ID', $id)
        ->select(
            'ID',
            'IP_ADDRESS',
            \DB::raw("CONCAT('".$akun_admin->FULLNAME."') AS ADMIN"),
            'LOGIN_DATE'
        )->orderBy('LOGIN_DATE', 'desc')
        ->get();

        return $admin;
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    protected $table = 'bank';
    protected $primaryKey = 'ID';
    public $timestamps = false;

    public function datatables() {
        $data = Bank::where('STATUS', '1')->get();

        return $data;
    }

    public function find_data($id) {
        $data = Bank::where('ID', $id)->first();
        
        return $data;
    }

    public function delete_data($id) {
        $data = Bank::where('ID', $id)->first();
        $data->STATUS = 99;
        $data->save();
        
        return $data;
    }
}

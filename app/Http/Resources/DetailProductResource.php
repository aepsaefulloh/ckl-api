<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\AdditionalImageModel;

class DetailProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $get_additional_image = AdditionalImageModel::where('UNIQ_CODE', $this->UNIQ_CODE)
        ->where('status', 1)
        ->select('IMAGE as _nama')
        ->get();

        return [
            '_nama'             => $this->NAME_PRODUCT,
            '_deskripsi'        => $this->DESCRIPTION_PRODUCT,
            '_kategori'         => $this->CATEGORY,
            '_foto_utama'       => $this->IMAGE,
            '_foto_lainnya'     => $get_additional_image,
            '_harga'            => $this->PRICE,
            '_warna_tersedia'   => $this->AVAILABLE_COLOR,
            '_view'             => $this->HIT,
            "_date"             => Date('Y-m-d', strtotime($this->CREATED_AT))
        ];
    }
}

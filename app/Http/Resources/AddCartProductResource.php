<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\ProductModel;

class AddCartProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $product = ProductModel::where('UNIQ_CODE', $this->UNIQ_CODE)->pluck('NAME_PRODUCT')->first();

        return [
            '_nama_produk'      => $product,
            '_total'            => $this->JUMLAH,
            '_tanggal'          => $this->CREATED_AT 
        ];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CategoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_key'          => $this->ID, 
            '_parent_key'   => $this->PARENT_ID,
            '_kategori'     => $this->TITLE,
            '_seo'          => $this->SEO
        ];
    }
}

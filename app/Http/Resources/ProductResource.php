<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\AdditionalImageModel;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        $this->dir_product_origin = "storage/images/product/origin/"; 
        $this->dir_product_thumb = "storage/images/product/thumb/";
        
        

        return [
            '_kode'             => $this->UNIQ_CODE,
            '_nama'             => $this->NAME_PRODUCT,
            '_deskripsi'        => $this->DESCRIPTION_PRODUCT,
            '_kategori'         => $this->CATEGORY,
            '_kategori_name'    => $this->TITLE,
            '_kategori_seo'    => $this->SEO,
            '_foto_origin'      => url()."/".$this->dir_product_origin.$this->CATEGORY."/".$this->IMAGE,
            '_foto_thumb'       => url()."/".$this->dir_product_thumb.$this->CATEGORY."/".$this->IMAGE,
            '_add_foto_origin'      => url()."/".$this->dir_product_origin.$this->CATEGORY."/".$this->ADD_IMG,
            '_add_foto_thumb'       => url()."/".$this->dir_product_thumb.$this->CATEGORY."/".$this->ADD_IMG,
            '_harga'            => $this->PRICE,
            '_warna_tersedia'   => $this->AVAILABLE_COLOR,
            '_view'             => $this->HIT,
            "_date"             => Date('Y-m-d', strtotime($this->CREATED_AT))
        ];
    }
}

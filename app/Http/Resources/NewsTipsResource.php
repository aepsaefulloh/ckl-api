<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Models\Category;

class NewsTipsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $getKategori = Category::find($this->CATEGORY);
        return [
            '_key'          => $this->ID,
            '_judul'        => $this->TITLE,
            '_ringkasan'    => $this->SUMMARY,
            '_kategori'     => $getKategori->TITLE,
            '_konten'       => $this->CONTENT,
            '_foto'         => [
                "_origin"    => url('/storage/images/news_tips/origin').'/'.$this->CATEGORY.'/'.$this->IMAGE,
                "_thumb"     => url('/storage/images/news_tips/thumb').'/'.$this->CATEGORY.'/'.$this->IMAGE,
            ],
            '_caption'      => $this->CAPTION,
            '_meta_desc'    => $this->META_DESC,
            '_hit'          => $this->HIT,
            '_waktu'        => Date('Y-m-d G:i:s', strtotime($this->PUBLISH_TIMESTAMP)) 
        ];
    }
}

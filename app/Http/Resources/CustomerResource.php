<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CustomerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            '_nama'             => $this->FULLNAME,
            '_nama_pengguna'    => $this->USER,
            '_email'            => $this->EMAIL,
            '_no_hp'            => $this->TELEPHONE,
            '_alamat'           => $this->ADDRESS
        ];
    }
}

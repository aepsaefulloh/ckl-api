<?php
namespace App\Http\Controllers\CMS\Voucher;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Voucher;

class VoucherController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->voucher = New Voucher;
    }

    public function all_data() {
        $data = $this->voucher->all_data();

        if (count($data) > 0) {
            return response()->json([
                "status"    => 200,
                "message"   => "OKE",
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "data"      => $data
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Notfound Data!",
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "data"      => []
            ], 200);
        }
        
    }
}
<?php
namespace App\Http\Controllers\CMS\Bank;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bank;

class BankController extends Controller
{
    public function __construct() {
        $this->middleware('auth');

        $this->bank = New Bank;
    }
    
    public function datatables(Request $request) {
        if ($request->params == 'delete' || $request->params == 'all') {
            $data = $this->bank->datatables();
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $data,
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            if ($request->mode == 'Tambah') {
                $data = New Bank;
            } elseif ($request->mode == 'Ubah') {
                $data = Bank::find($request->id);
            }
            
            $data->BANK_NAME    = $request->bank;
            $data->NO_REK       = $request->no_rek;
            $data->NAMA         = $request->nama;
            $data->save();

            return redirect()->json([
                "status"    => 200,
                "message"   => "Success"
            ], 200);
        } else {
            return redirect()->json([
                "status"    => 404,
                "message"   => "Ups Failed"
            ], 200);
        }
    }

    public function find_data(Request $request) {
        if ($request->_token != '') {
            $data = $this->bank->find_data($request->id);
            return response()->json([
                "status"    => 200,
                "message"   => "Oke",
                "data"      => $data
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "ups failed",
                "data"      => []
            ], 200);
        }
        
    }

    public function delete(Request $request) {
        if ($request->_token != '') {
            $data = $this->bank->delete_data($request->id);
            return response()->json([
                "status"    => 200,
                "message"   => "Success Delete"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "ups failed"
            ], 200);
        }
        
    }
}
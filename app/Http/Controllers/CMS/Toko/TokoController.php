<?php
namespace App\Http\Controllers\CMS\Toko;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toko;

class TokoController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->toko = New Toko;
        $this->dir_toko_origin = "storage/images/toko/origin/";
        $this->dir_toko_thumb = "storage/images/toko/thumb/";
    }

    public function datatables(Request $request) {
        if ($request->params == 'all' || $request->params == 'delete') {
            $data = $this->toko->datatables($this->dir_toko_origin, $this->dir_toko_thumb, $request->params);
            return response()->json([
                "status"    => 200,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Oke",
                "data"      => $data
            ]);   
        } else {
            return response()->json([
                "status"    => 404,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            $dirFile = $this->dir_toko_origin;
            $dirFile_thumb = $this->dir_toko_thumb;
            
            if(!is_dir($dirFile)){
                mkdir($dirFile,0755, true);
            }
            if(!is_dir($dirFile_thumb)){
                mkdir($dirFile_thumb,0755, true);
            }

            if ($request->mode == "Ubah") {
                    $post = Toko::where('ID', $request->id)->first();
            } else  {
                $post = New Toko;
            }

            if ($request->image != '') {
                $image = $request->image;
                $image = str_replace("[removed]", "", $image);
                $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
                $imageName = uniqid(). '-' . $request->nama_toko . '-' . \Carbon\Carbon::now()->format('Y-m-d') . '.' . $request->image_tipe;
                          
                $original = Image::make(base64_decode($img))->save($dirFile . $imageName);
                $resizeImage  = Image::make(base64_decode($img))->resize(200, 200, function($constraint) {$constraint->aspectRatio();});
                $thumb = Image::make($resizeImage)->save($dirFile_thumb . $imageName);
                $post->IMAGE        = $imageName;
            } 
            
            $post->NAME_TOKO    = $request->nama_toko;
            $post->BANK         = $request->bank;
            $post->NO_REK       = $request->no_rek;
            $post->TELEPHONE    = $request->telephone;
            $post->ADDRESS      = $request->address;
            $post->PROV_ID      = $request->provinsi;
            $post->KOTA_ID      = $request->kab;
            $post->STATUS       = $request->status;
            $post->CREATED_AT   = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            $post->save();

            return response()->json([
                "status"    => 200,
                "message"   => "Success"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed"
            ], 200);
        }
    }

    public function publish(Request $request) {
        if ($request->_token != '') {
            $status = [
                "true"  => 1,
                "false" => 0
            ];
    
            $message = [
                "true"=>"Toko Aktif!",
                "false"=>"Toko Nonaktif!"
            ];
    
            ## proses update status data menjadi 1(publish)
            $update_data           = Toko::where('ID', $request->id)->first();
            $update_data->STATUS   = $status[$request->status];
            $update_data->save();
    
            return response()->json([
                "status"    => 200,
                "message"   => $message[$request->status],
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Failed!",
            ], 200);
        }
    }

    public function search_toko(Request $request) {
        $data = $this->toko->searchToko($request->params);

        if (count($data) > 0) {
            return response()->json([
                "status"    => 200,
                "message"   => "OKE",
                "links"     => [
                    "self"  => url($request->fullURL())
                ],
                "data"      => $data
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "links"     => [
                    "self"  => url($request->fullURL())
                ],
                "data"      => []
            ]);
        }
        
        return $data;
    }

    public function find_data($id) {
        $data = $this->toko->get_by_id($id, $this->dir_toko_origin, $this->dir_toko_thumb);

        if ($data) {
            return response()->json([
                "status"    => 200,
                "message"   => "OKE",
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "data"      => $data
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "links"     => [
                    "self"  => url($request->fullURL())
                ],
                "data"      => []
            ]);
        }
        
        return $data;
    }

    public function delete(Request $request) {
        if ($request->_token != '') {
            $update_data           = Toko::where('ID', $request->id)->first();
            $update_data->STATUS   = 99;
            $update_data->save();

            return response()->json([
                "status"    => 200,
                "message"   => 'Delete Success!'
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed",
            ], 200);
        }
    }



    
    public function get_all_data() {
        if ($this->authentikasi() == 1) {
            ## get data Toko yang status nya bukan 99 (belum didelete)
            $toko_data = Toko::where('STATUS', '!=', 99)->get();

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $toko_data
            ]);
        } else {
            return view('CMS/Auth/login', ['message_login' => 'Login Terlebih Dahulu!']);
        }
    }

    public function get_find_data($id) {
        if ($this->authentikasi() == 1) {
            ## replace request id random key menjadi id asli yang seperti didatabase 
            $req_id = (int)preg_replace('/^[^-]*-/', '',$id);

            ## get data Toko berdasarka ID
            $toko_data = Toko::find($req_id);

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $toko_data
            ]);   
        } else {
            return view('CMS/Auth/login', ['message_login' => 'Login Terlebih Dahulu!']);
        }
    }

    public function post_data(Request $request) {
        
    }

    public function update_data($id, Request $request) {
        
    }

    public function delete_data($id, Request $request) {
        if ($this->authentikasi() == 1) {
            ## replace request id random key menjadi id asli yang seperti didatabase 
            $req_id = (int)preg_replace('/^[^-]*-/', '',$id);

            ## proses update status data menjadi 99(delete)
            $update_data           = Toko::find($req_id);
            $update_data->STATUS   = 99;
            $update_data->save();
            return redirect()->route('admin.view-Toko-all');
        } else {
            return view('CMS/Auth/login', ['message_login' => 'Login Terlebih Dahulu!']);
        }
    }

    public function publish_data($id) {
        
    }

    public function unpublish_data($id) {
        
   }

    public function get_delete_all_data() {
        
    }

    public function back_delete_data($id, Request $request) {
        
    }
}
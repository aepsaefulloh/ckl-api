<?php
namespace App\Http\Controllers\CMS\NewsTips;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NewsTips;
use Illuminate\Support\Facades\File;

class NewsTipsController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->news_tips = new NewsTips;
        $this->dir_news_tips_origin = "storage/images/news_tips/origin/";
        $this->dir_news_tips_thumb = "storage/images/news_tips/thumb/";
    }

    public function datatables(Request $request) {
        if ($request->params == 'all' || $request->params == 'delete') {
            $data = $this->news_tips->datatables($this->dir_news_tips_origin, $this->dir_news_tips_thumb, $request->params);
            return response()->json([
                "status"    => 200,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Oke",
                "data"      => $data
            ]);   
        } else {
            return response()->json([
                "status"    => 404,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            if ($request->mode == "Tambah") {
                $post = New NewsTips;
                $post->HIT = 0;
            } elseif ($request->mode == "Ubah") {
                $post = NewsTips::find($request->id);
            }

            $dirFile = $this->dir_news_tips_origin.$request->category.'/';
            $dirFile_thumb = $this->dir_news_tips_thumb.$request->category.'/';

            if(!is_dir($dirFile)){
                mkdir($dirFile, 0755, true);
            }
            if(!is_dir($dirFile_thumb)){
                mkdir($dirFile_thumb, 0755, true);
            }

            // Kondisi jika update category pada data news tips
            if ($request->mode == "Ubah" && $post->CATEGORY != $request->category) {
                if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') {
                    $file_origin = url($this->dir_news_tips_origin.$post->CATEGORY.'/'.$post->IMAGE);
                    $file_thumb = url($this->dir_news_tips_thumb.$post->CATEGORY.'/'.$post->IMAGE);
                } else {
                    $file_origin = url($this->dir_news_tips_origin.$post->CATEGORY.'/'.$post->IMAGE);
                    $file_thumb = url($this->dir_news_tips_thumb.$post->CATEGORY.'/'.$post->IMAGE);
                }

                // Extension IMAGE
                $ext_origin = pathinfo($file_origin, PATHINFO_EXTENSION);
                $ext_thumb = pathinfo($file_thumb, PATHINFO_EXTENSION);
                
                // Get Image
                $file_origin_get = file_get_contents($file_origin);
                $file_thumb_get = file_get_contents($file_thumb);
                
                // Convert Base64
                $base64_origin = 'data:image/' . $ext_origin . ';base64,' .base64_encode($file_origin_get);
                $base64_thumb = 'data:image/' . $ext_thumb . ';base64,' .base64_encode($file_thumb_get); 

                // Base64 Decode
                $image_origin = str_replace("[removed]", "", $base64_origin);
                $img_origin = preg_replace('/^data:image\/\w+;base64,/', '', $image_origin);

                $image_thumb = str_replace("[removed]", "", $base64_thumb);
                $img_thumb = preg_replace('/^data:image\/\w+;base64,/', '', $image_thumb);

                // Move file origin Image
                Image::make(base64_decode($img_origin))->save($this->dir_news_tips_origin.$request->category.'/' . $post->IMAGE);
                Image::make(base64_decode($img_thumb))->save($this->dir_news_tips_thumb.$request->category.'/'. $post->IMAGE);

                // Delete
                File::delete($file_origin);
                File::delete($file_thumb); 
            }
            
            if ($request->image != '') {
                $image = $request->image;
                $image = str_replace("[removed]", "", $image);
                $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
                $imageName = rand().'-'.$request->title. '-' . \Carbon\Carbon::now()->format('Y-m-d') . '.' . $request->image_type;
                          
                $original = Image::make(base64_decode($img))->save($dirFile . $imageName);
                $resizeImage  = Image::make(base64_decode($img))->resize(200, 200, function($constraint) {$constraint->aspectRatio();});
                $thumb = Image::make($resizeImage)->save($dirFile_thumb . $imageName);
                $post->IMAGE        = $imageName;
            } 

            $post->TITLE                = $request->title;
            $post->SLUG                 = \Illuminate\Support\Str::slug($request->title);
            $post->SUMMARY              = $request->summary;
            $post->CATEGORY             = $request->category;
            $post->CONTENT              = $request->content;
            $post->EMBLED               = $request->embled_yt;
            $post->CAPTION              = $request->caption;
            $post->TAG                  = strtolower($request->tag);
            $post->META_DESC            = $request->meta_desc;
            $post->PUBLISH_TIMESTAMP    = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            $post->STATUS               = $request->status;
            $post->save();

            return response()->json([
                "status"        => 200,
                "message"       => "created Success",
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ]);
        }
        
    }

    public function find_data($id) {
        $data = $this->news_tips->find_data($this->dir_news_tips_origin, $this->dir_news_tips_thumb, $id);

        if (isset($data)) {
            return response()->json([
                "status"    => 200,
                "message"   => "oke",
                "data"      => $data
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "data"      => []
            ], 200);
        }
        
    } 

    public function delete(Request $request) {
        if ($request->_token != '') {
            $post = $this->news_tips->delete_data($request->id);

            return response()->json([
                "status"    => 200,
                "message"   => "Data Success Delete"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ], 200);
        }
        
    }

    public function publish(Request $request) {
        if ($request->_token != '') {
            $status = [
                "true"=> 1,
                "false"=>0
            ];

            $message = [
                "true"=>"Data berhasil dipublish!",
                "false"=>"Data berhasil di unpublish"
            ];

            $update_data           = NewsTips::find($request->id);
            $update_data->STATUS   = $status[$request->status];
            $update_data->save();

            return response()->json([
                "status"    => 200,
                "message"   => $message[$request->status]
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ], 200);
        }
        
    }

    public function delete_image(Request $request) {
        if ($request->_token != '') {
            $update_data            = NewsTips::find($request->id);
            $update_data->IMAGE     = 'default.png';
            $update_data->save();

            return response()->json([
                "status"    => 200,
                "message"   => "Delete Image Success"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ], 200);
        }
        
    }
}
<?php
namespace App\Http\Controllers\CMS\Banner;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->dir_banner_origin = "storage/images/banner/origin/";
        $this->dir_banner_thumb = "storage/images/banner/thumb/";
        $this->banner = new Banner;
    }

    public function datatables(Request $request) {
        if ($request->params == 'delete' || $request->params == 'all') {
            $data = $this->banner->datatables($request->params);
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $data,
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }

    public function find($id) {
        $data = $this->banner->find_data($this->dir_banner_origin, $this->dir_banner_thumb, $id);
        
        if ($data) {
            return response()->json([
                "status"    => 200,
                "message"   => "oke",
                "data"      => $data
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Not Found",
                "data"      => []
            ], 200);
        }
        
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            $dirFile = $this->dir_banner_origin;
            $dirFile_thumb = $this->dir_banner_thumb;
            
            if(!is_dir($dirFile)){
                mkdir($dirFile,0755, true);
            }
            if(!is_dir($dirFile_thumb)){
                mkdir($dirFile_thumb,0755, true);
            }

            if ($request->mode == "Ubah") {
                    $post = Banner::where('ID', $request->id)->first();
                    $post->UPDATED_DATE = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            } else  {
                $post = New Banner;
            }

            if ($request->image != '') {
                $image = $request->image;
                $image = str_replace("[removed]", "", $image);
                $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
                $imageName = uniqid(). '-' . \Carbon\Carbon::now()->format('Y-m-d') . '.' . $request->image_tipe;
                          
                $original = Image::make(base64_decode($img))->save($dirFile . $imageName);
                $resizeImage  = Image::make(base64_decode($img))->resize(200, 200, function($constraint) {$constraint->aspectRatio();});
                $thumb = Image::make($resizeImage)->save($dirFile_thumb . $imageName);
                $post->IMAGE        = $imageName;
            } 
            
            $post->CATEGORY     = $request->category;
            $post->TITLE        = $request->title;
            $post->LINK         = $request->url;
            $post->CAPTION      = $request->caption;
            $post->POSITION     = $request->posisi;
            $post->STATUS       = $request->status;
            $post->PUBLISH_DATE = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            $post->save();

            return response()->json([
                "status"    => 200,
                "message"   => "Success"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed"
            ], 200);
        }
    }

    public function publish(Request $request) {
        if ($request->_token != '') {
            $status = [
                "true"=> 1,
                "false"=>0
            ];

            $message = [
                "true"=>"Data berhasil dipublish!",
                "false"=>"Data berhasil di unpublish"
            ];

            ## proses update status data menjadi 1(publish)
            $update_data           = Banner::where('ID', $request->banner)->first();
            $update_data->STATUS   = $status[$request->status];
            $update_data->save();

            return response()->json([
                "status"    => 200,
                "message"   => $message[$request->status],
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed",
            ], 200);
        }
    }

    public function delete_image(Request $request) {
        if ($request->_token != '') {
            $update_data           = Banner::where('ID', $request->id)->first();
            $update_data->IMAGE    = 'default.png';
            $update_data->save();

            return response()->json([
                "status"    => 200,
                "message"   => 'Delete Success!'
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed",
            ], 200);
        }
    }

    public function delete(Request $request) {
        if ($request->_token != '') {
            $update_data           = Banner::where('ID', $request->id)->first();
            $update_data->STATUS   = 99;
            $update_data->save();

            return response()->json([
                "status"    => 200,
                "message"   => 'Delete Success!'
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed",
            ], 200);
        }
    }

}
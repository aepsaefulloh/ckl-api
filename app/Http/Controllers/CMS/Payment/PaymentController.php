<?php
namespace App\Http\Controllers\CMS\Payment;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Transaction;

class PaymentController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->transaction = new Transaction;
    }

    public function transaction_datatables(Request $request) {
        if ($request->params == 'delete' || $request->params == 'all') {
            $data = $this->transaction->datatables($request->params);

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $data
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }

    public function transaction_detail($id) {
        $data = $this->transaction->result_by_id($id);
            
        if ($data) {
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $data
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }

    public function transaction_payment_update($type, Request $request) {
        if ($request->_token != '') {
            $update_transaction = $this->transaction->update_payment_status($type, $request->id);
            
            if ($update_transaction == true) {
                return response()->json([
                    "status"    => 200,
                    "message"  => "Data Has Updated!"
                ], 200);
            } else {
                return response()->json([
                    "status"    => 401,
                    "message"  => "Data Failed Update!"
                ], 200);
            }
            
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Access!"
            ], 200);
        }
        
    }

    public function update_status(Request $request) {
        if ($request->_token != '') {
            $data = ["status"    => $request->status];
            if (isset($request->no_resi) && $request->no_resi != '') {
                $data['no_resi'] = $request->no_resi;
            } 
            
            $update_transaction = $this->transaction->payment_status($request->id, $data);
            
            if ($update_transaction == true) {
                return response()->json([
                    "status"    => 200,
                    "message"  => "Data Has Updated!"
                ], 200);
            } else {
                return response()->json([
                    "status"    => 401,
                    "message"  => "Data Failed Update!"
                ], 200);
            }
            
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Access!"
            ], 200);
        }
    }
}
<?php
namespace App\Http\Controllers\CMS\Account;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\Admin;

class AdminController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->admin = New Admin;
    }
    
    public function datatables() {
        $data = $this->admin->datatables();
        
        return response()->json([
            "status"    => 200,
            "message"   => "OK",
            "data"      => $data,
        ], 200);
    }

    public function find_by_id($id) {
        $admin = $this->admin->find_by_id($id);

        if ($admin) {
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $admin,
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found Data!",
                "data"      => [],
            ]);
        }
        
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            if ($request->mode == 'Tambah') {
                $data = New Admin;
            } elseif ($request->mode == 'Ubah') {
                $data = Admin::find($request->id);
            }
            
            $data->FULLNAME     = $request->fullname;
            $data->USER         = $request->user;
            $data->EMAIL        = $request->email;
            $data->PASSWORD     = Hash::make($request->password);
            $data->ROLE         = $request->role;
            $data->STATUS       = $request->status;
            $data->save();

            return redirect()->json([
                "status"    => 200,
                "message"   => "Success"
            ], 200);
        } else {
            return redirect()->json([
                "status"    => 404,
                "message"   => "Ups Failed"
            ], 200);
        }
    }
}
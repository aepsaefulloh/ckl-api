<?php
namespace App\Http\Controllers\CMS\Account;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Customer;


class CustomerController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->customer = New Customer();
    }

    public function datatables() {
        $data = $this->customer->datatables();
        
        return response()->json([
            "status"    => 200,
            "message"   => "OK",
            "data"      => $data,
        ], 200);
    }

    public function find_data($id) {
        if ($this->authentikasi() == 1) {
            ## replace request id random key menjadi id asli yang seperti didatabase 
            $req_id = (int)preg_replace('/^[^-]*-/', '',$id);

            ## get data NewsTips berdasarka ID
            $customer = Customer::find($req_id);

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $customer
            ]);   
        } else {
            return view('CMS/Auth/login', ['message_login' => 'Login Terlebih Dahulu!']);
        }
    }
}
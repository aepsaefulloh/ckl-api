<?php
namespace App\Http\Controllers\CMS\Category;

use Intervention\Image\ImageManagerStatic as Image;
use App\Models\AdditionalCategoryBrandModel;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
        
        $this->CategoryModel = new Category;
        $this->add_category_brand = New AdditionalCategoryBrandModel;
        $this->dir_origin = 'storage/images/category/origin/';
        $this->dir_thumb = 'storage/images/category/thumb/';
    }
    
    public function get_all_data(Request $request) {
        if ($request->params == 'product' || $request->params == 'news_tips') {
            $category_data = $this->CategoryModel->all_data($request->params);
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $category_data,
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "data"      => [],
            ]);
        }
        
    }

    public function datatables(Request $request) {
        if ($request->params == 'delete' || $request->params == 'all') {
            $data = $this->CategoryModel->datatables($request->params);
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $data,
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }
   
    public function find_data($id) {
        $id = (int)preg_replace('/^[^-]*-/', '',$id);
        $category_data = $this->CategoryModel->get_find($id);

        return response()->json([
            "status"    => true,
            "message"   => "OK",
            "data"      => $category_data
        ]);
    }

    public function store(Request $request) {
        if (isset($request->_token) && $request->_token != '') {
            if ($request->mode == 'Tambah') {
                $post_category = new Category;
            } elseif ($request->mode == 'Ubah') {
                $post_category = Category::find($request->id);
            }
    
            // Deteksi folder origin & thumb
            if(!is_dir($this->dir_origin)){
                mkdir($this->dir_origin, 0755, true);
            }
            if(!is_dir($this->dir_thumb)){
                mkdir($this->dir_thumb, 0755, true);
            }

            // Deteksi Image
            if ($request->image != '') {
                $image = $request->image;
                $image = str_replace("[removed]", "", $image);
                $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
                $imageName = $request->image_name. '-' . \Carbon\Carbon::now()->format('Y-m-d') . '.' . $request->type_image;
                          
                $original = Image::make(base64_decode($img))->save($this->dir_origin . $imageName);
                $resizeImage  = Image::make(base64_decode($img))->resize(200, 200, function($constraint) {$constraint->aspectRatio();});
                $thumb = Image::make($resizeImage)->save($this->dir_thumb . $imageName);
                $post_category->IMAGE        = $imageName;
            } 
            
           $post_category->PARENT_ID    = $request->category;
           $post_category->TITLE        = $request->title;
           $post_category->SEO          = $request->seo;
           $post_category->META_DESC    = $request->meta_desc;
           $post_category->TIPE         = $request->tipe;
           $post_category->TAG          = $request->tag;
           $post_category->STATUS       = $request->status;
           $post_category->save(); 
    
           if ($request->mode == "Tambah") {
               // Insert Data Ke Tables Additional Category Brand
               foreach ($request->brand as $b) {
                    $this->add_category_brand->store(['CATEGORY' => $post_category->ID, 'BRAND' => $b]);
                }
           } elseif ($request->mode == "Ubah") {
                // Hapus Data Additional Category Brand
                $this->add_category_brand->remove($request->id);
                
                // Insert Data Ke Tables Additional Category Brand
                foreach ($request->brand as $b) {
                    $this->add_category_brand->store(['CATEGORY' => $request->id, 'BRAND' => $b]);
                }
           }
           
            return response()->json([
                "status"    => 201,
                "message"   => "Created Data Success"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Failed Data"
            ], 200);
        }
    }

    public function publish(Request $request) {
        ## replace request id random key menjadi id asli yang seperti didatabase 
        $req_id = (int)preg_replace('/^[^-]*-/', '',$request->category);
        $status = [
            "true"=> 1,
            "false"=>0
        ];

        $message = [
            "true"=>"Data berhasil dipublish!",
            "false"=>"Data berhasil di unpublish"
        ];

        ## proses update status data menjadi 1(publish)
        $update_data           = Category::find($req_id);
        $update_data->STATUS   = $status[$request->status];
        $update_data->save();

        return response()->json([
            "status"    => 200,
            "message"   => $message[$request->status],
        ], 200);
    }

    public function delete(Request $request) {
        // ## proses update status data menjadi 99(delete)
        $update_data           = Category::find((int)$request->id);
        $update_data->STATUS   = 99;
        $update_data->save();

        return response()->json([
            "status"    => 200,
            "message"   => $request->id
        ], 200);   
    }
}
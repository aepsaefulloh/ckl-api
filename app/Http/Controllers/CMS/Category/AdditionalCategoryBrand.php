<?php
namespace App\Http\Controllers\CMS\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AdditionalCategoryBrandModel;

class AdditionalCategoryBrand extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->add_category_brand = New AdditionalCategoryBrandModel;
    }

    public function all_data_by_id($id) {
        $data = $this->add_category_brand->all_data_by_id($id);

        return response()->json($data);
    }
}
<?php
namespace App\Http\Controllers\CMS\Setting;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;

class SettingController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->setting = New Setting;
        $this->dir_origin = 'storage/images/logo/origin/';
        $this->dir_thumb = 'storage/images/logo/thumb/';
    }

    public function find_data($id) {
        $data = $this->setting->find_data($id, $this->dir_origin, $this->dir_thumb);

        return response()->json([
            "status"    => 200,
            "message"   => "oke",
            "data"      => $data
        ], 200);
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            $dirFile = $this->dir_origin;
            $dirFile_thumb = $this->dir_thumb;
            
            if(!is_dir($dirFile)){
                mkdir($dirFile,0755, true);
            }
            if(!is_dir($dirFile_thumb)){
                mkdir($dirFile_thumb,0755, true);
            }

            if ($request->mode == "Ubah") {
                $post = Setting::where('ID', $request->id)->first();
                $post->UPDATED_AT   = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            } else  {
                $post = New setting;
            }

            // Logo
            if ($request->image != '') {
                $image = $request->image;
                $image = str_replace("[removed]", "", $image);
                $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
                $imageName = uniqid(). '-' . \Carbon\Carbon::now()->format('Y-m-d') . '.' . $request->image_tipe;
                          
                $original = Image::make(base64_decode($img))->save($dirFile . $imageName);
                $resizeImage  = Image::make(base64_decode($img))->resize(200, 200, function($constraint) {$constraint->aspectRatio();});
                $thumb = Image::make($resizeImage)->save($dirFile_thumb . $imageName);
                $post->LOGO        = $imageName;
            } 

            // Logo Footer
            if ($request->image_footer != '') {
                $image_footer = $request->image_footer;
                $image_footer = str_replace("[removed]", "", $image_footer);
                $img_footer = preg_replace('/^data:image\/\w+;base64,/', '', $image_footer);
                $imageNameFooter = uniqid(). '-' . \Carbon\Carbon::now()->format('Y-m-d') . '.' . $request->image_tipe_footer;
                          
                $original_footer = Image::make(base64_decode($img_footer))->save($dirFile . $imageNameFooter);
                $resizeImage_footer  = Image::make(base64_decode($img_footer))->resize(200, 200, function($constraint) {$constraint->aspectRatio();});
                $thumb = Image::make($resizeImage_footer)->save($dirFile_thumb . $imageNameFooter);
                $post->LOGO_FOOTER        = $imageNameFooter;
            } 

            $post->SITE_NAME    = $request->site_name;
            $post->KEYWORD      = $request->keyword;
            $post->DESCRIPTION  = $request->description;
            $post->LOCATION     = $request->lokasi;
            $post->PHONE        = $request->phone;
            $post->EMAIL        = $request->email;
            $post->IG           = $request->ig;
            $post->FB           = $request->fb;
            $post->TWITTER      = $request->twitter;  
            $post->EMBLED_MAPS  = $request->embled_maps;
            $post->CREATED_AT   = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            $post->save();

            return response()->json([
                "status"    => 200,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Created",
                $request->all()
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed"
            ], 200);
        }
        
    }
}
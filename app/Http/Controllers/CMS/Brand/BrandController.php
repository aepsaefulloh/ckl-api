<?php
namespace App\Http\Controllers\CMS\Brand;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Brand;

class BrandController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
        
        $this->brand = New Brand;
        $this->dir_origin = 'storage/images/brand/origin/';
        $this->dir_thumb = 'storage/images/brand/thumb/';
    }

    public function all_data() {
        $data = $this->brand->all_data();

        return response()->json([
            "status"    => 200,
            "message"   => "oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }

    public function datatables(Request $request) {
        if ($request->params == 'all' || $request->params == 'delete') {
            $data = $this->brand->datatables($request->params, $this->dir_origin, $this->dir_thumb);
            return response()->json([
                "status"    => 200,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Oke",
                "data"      => $data
            ]);   
        } else {
            return response()->json([
                "status"    => 404,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Ups Failed!",
                "data"      => []
            ]);
        }
    }

    public function find_data($id) {
        $data = $this->brand->find_data($id, $this->dir_origin, $this->dir_thumb);

        if (isset($data->ID)) {
            return response()->json([
                "status"    => 200,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "OKE",
                "data"      => $data
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Not Found Data",
            ], 200);
        }
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            $dirFile = $this->dir_origin;
            $dirFile_thumb = $this->dir_thumb;
            
            if(!is_dir($dirFile)){
                mkdir($dirFile,0755, true);
            }
            if(!is_dir($dirFile_thumb)){
                mkdir($dirFile_thumb,0755, true);
            }

            if ($request->mode == "Ubah") {
                $post = Brand::where('ID', $request->id)->first();
            } else  {
                $post = New Brand;
            }

            if ($request->image != '') {
                $image = $request->image;
                $image = str_replace("[removed]", "", $image);
                $img = preg_replace('/^data:image\/\w+;base64,/', '', $image);
                $imageName = uniqid(). '-' . \Carbon\Carbon::now()->format('Y-m-d') . '.' . $request->image_tipe;
                          
                $original = Image::make(base64_decode($img))->save($dirFile . $imageName);
                $resizeImage  = Image::make(base64_decode($img))->resize(200, 200, function($constraint) {$constraint->aspectRatio();});
                $thumb = Image::make($resizeImage)->save($dirFile_thumb . $imageName);
                $post->IMAGE        = $imageName;
            } 
            
            $post->NAME_BRAND   = $request->nama;
            $post->SEO          = $request->seo;
            $post->META_DESC    = $request->meta_desc;
            $post->STATUS       = $request->status;
            $post->save();

            return response()->json([
                "status"    => 200,
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "message"   => "Created",
                $request->all()
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed"
            ], 200);
        }
        
    }

    public function publish(Request $request) {
        ## replace request id random key menjadi id asli yang seperti didatabase 
        $status = [
            "true"=> 1,
            "false"=>0
        ];

        $message = [
            "true"=>"Data berhasil dipublish!",
            "false"=>"Data berhasil di unpublish"
        ];

        ## proses update status data menjadi 1(publish)
        $update_data           = Brand::find($request->brand);
        $update_data->STATUS   = $status[$request->status];
        $update_data->save();

        return response()->json([
            "status"    => 200,
            "message"   => $message[$request->status],
        ], 200);
    }

    public function delete(Request $request) {
        // ## proses update status data menjadi 99(delete)
        $update_data           = Brand::find((int)$request->id);
        $update_data->STATUS   = 99;
        $update_data->save();

        return response()->json([
            "status"    => 200,
            "message"   => $request->id
        ], 200);   
    }
}
<?php
namespace App\Http\Controllers\CMS\Product;

use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Models\AdditionalImageModel;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Request;
use App\Models\ProductModel;
use App\Models\ActivityStok;
use App\Models\ProductVideos;
use App\Models\Category;
use App\Models\Brand;
use App\Models\Toko;
use App\Models\Stok;
use Storage;

class ProductController extends Controller
{
    public function __construct(){
        $this->middleware('auth');

        $this->stok = new Stok;
        $this->toko = New Toko;
        $this->brand = New Brand;
        $this->category = new Category;
        $this->ProductModel = new ProductModel;
        $this->ProductVideos = New ProductVideos;
        $this->AdditionalImageModel = new AdditionalImageModel;
        $this->dir_product_origin = "storage/images/product/origin/"; 
        $this->dir_product_thumb = "storage/images/product/thumb/";
    }

    public function datatables(Request $request) {
        if ($request->params == 'all' || $request->params == 'delete') {
            $data = $this->ProductModel->datatables($this->dir_product_origin, $this->dir_product_thumb, $request->params);

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "data"      => $data
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Data Failed!",
                "links"     => [
                    "self"  => url($request->fullURL())
                ],
                "data"      => []
            ]);
        }
        
    }

    public function find($id) {
        $data = $this->ProductModel->find_data($this->dir_product_origin, $this->dir_product_thumb, $id);

        if (isset($data)) {
            return response()->json([
                "status"    => 200,
                "message"   => "oke",
                "data"      => $data
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Not Found!",
                "data"      => []
            ]);
        }
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            $dirFile = $this->dir_product_origin.$request->category;
            $dirFile_thumb = $this->dir_product_thumb.$request->category;
            
            // Kondisi Direktori Folder
            if(!is_dir($dirFile)){
                mkdir($dirFile,0755, true);
            }
            if(!is_dir($dirFile_thumb)){
                mkdir($dirFile_thumb,0755, true);
            }

            if ($request->mode == 'Tambah') {
                $post_product = new ProductModel;
                $post_product->UNIQ_CODE    = $request->uniq_code;
                $post_product->CREATED_AT   = \Carbon\Carbon::now()->format('Y-m-d G:i:s');

            } elseif ($request->mode == 'Ubah') {
                $post_product = ProductModel::find($request->id);
                $post_product->UPDATED_AT   = \Carbon\Carbon::now()->format('Y-m-d G:i:s');

                // Kondis dimana jika update category image lagsung di move ke folder category baru
                if ($post_product->CATEGORY != $request->category) {
                    // Moving Image File Default Image
                    if (!is_dir($this->dir_product_origin.$post_product->CATEGORY)) {
                        mkdir($this->dir_product_origin.$post_product->CATEGORY);
                    }

                    if (file_exists($this->dir_product_origin.$post_product->CATEGORY.'/'.$post_product->IMAGE)) {
                        File::move($this->dir_product_origin.$post_product->CATEGORY.'/'.$post_product->IMAGE, $dirFile.'/'.$post_product->IMAGE);
                    }

                    // Remove Image Old Default Image
                    if (file_exists($this->dir_product_origin.$post_product->CATEGORY.'/'.$post_product->IMAGE)) {
                        File::delete($this->dir_product_origin.$post_product->CATEGORY.'/'.$post_product->IMAGE);
                    }

                    // Moving Image File Default Image Thumb
                    if (!is_dir($this->dir_product_thumb.$post_product->CATEGORY)) {
                        mkdir($this->dir_product_thumb.$post_product->CATEGORY);
                    }

                    
                    if (file_exists($this->dir_product_thumb.$post_product->CATEGORY.'/'.$post_product->IMAGE)) {
                        File::move($this->dir_product_thumb.$post_product->CATEGORY.'/'.$post_product->IMAGE, $dirFile_thumb.'/'.$post_product->IMAGE);
                    }

                    // Remove Image Old Default Image
                    if (file_exists($this->dir_product_thumb.$post_product->CATEGORY.'/'.$post_product->IMAGE)) {
                        File::delete($this->dir_product_thumb.$post_product->CATEGORY.'/'.$post_product->IMAGE);
                    }

                    // Find Add Image
                    $search_add_image = AdditionalImageModel::where('UNIQ_CODE', $request->id)->where('STATUS', 1)->get();
                    

                    if (count($search_add_image) > 0) {
                        unset($search_add_image[0]);
                        foreach ($search_add_image as $key => $val) {
                            // Moving Image File Default Image  
                            if (!is_dir($this->dir_product_origin.$post_product->CATEGORY)) {
                                mkdir($this->dir_product_origin.$post_product->CATEGORY);
                            }

                            if (file_exists($this->dir_product_origin.$post_product->CATEGORY.'/'.$val->IMAGE)) {
                                File::move($this->dir_product_origin.$post_product->CATEGORY.'/'.$val->IMAGE, $dirFile.'/'.$val->IMAGE);
                            }
    
                            // Remove Image Old Default Image
                            if (file_exists($this->dir_product_origin.$post_product->CATEGORY.'/'.$val->IMAGE)) {
                                File::delete($this->dir_product_origin.$post_product->CATEGORY.'/'.$val->IMAGE);
                            }
    
                            // Moving Image File Default Image thumb  
                            if (!is_dir($this->dir_product_thumb.$post_product->CATEGORY.'/'.$val->IMAGE)) {
                                mkdir($this->dir_product_thumb.$post_product->CATEGORY.'/'.$val->IMAGE);
                            }

                            if (file_exists($this->dir_product_thumb.$post_product->CATEGORY.'/'.$val->IMAGE)) {
                                File::move($this->dir_product_thumb.$post_product->CATEGORY.'/'.$val->IMAGE, $dirFile_thumb.'/'.$val->IMAGE);
                            }
    
                            // Remove Image Old Default Image thumb
                            if (!file_exists($this->dir_product_thumb.$post_product->CATEGORY.'/'.$val->IMAGE)) {
                                File::delete($this->dir_product_thumb.$post_product->CATEGORY.'/'.$val->IMAGE);
                            }
                        }
                    } 

                } 
                
            } 
            
            $post_product->NAME_PRODUCT         = $request->name_product;
            $post_product->DESCRIPTION_PRODUCT  = $request->description_product;
            $post_product->CATEGORY             = $request->category;
            $post_product->BRAND                = $request->brand;
            $post_product->PRICE                = $request->price;
            $post_product->WEIGHT               = $request->weight;
            $post_product->ID_TOKO              = $request->id_toko;
            $post_product->PCS                  = $request->pcs;
            
            if ($request->mode == "Ubah") {
                $post_product->IMAGE            = $post_product->IMAGE;
            } elseif ($request->mode == "Tambah") {
                $post_product->IMAGE            = 'default.png';
            }
            
            $post_product->STATUS_BARANG        = $request->status_barang;
            $post_product->SALE                 = $request->sale;
            $post_product->DISCOUNT             = $request->sale_persen;
            $post_product->TAG                  = $request->tag;
            $post_product->META_DESC            = $request->meta_desc;
            $post_product->STATUS               = $request->status;
            $post_product->save(); 

            Log::info('test');

            $detect_uniq_code = ($request->mode == "Ubah") ? $request->id : $request->uniq_code;
            // Konsidi Jika ditemukan request an image
            if (count($request->image) > 0) {
                // Remove File old
                if (file_exists($dirFile.'/'.$post_product->IMAGE)) {
                    Storage::delete($dirFile.'/'.$post_product->IMAGE);
                }

                if (file_exists($dirFile_thumb.'/'.$post_product->IMAGE)) {
                    Storage::delete($dirFile_thumb.'/'.$post_product->IMAGE);
                }

                // Kondisi Insert Image & Convert Base64 to image
                foreach ($request->image as $key => $value) {
                    $img = preg_replace('/^data:image\/\w+;base64,/', '', str_replace("[removed]", "", $value['image']));
                    $imageName = $key.'-'.$detect_uniq_code. '-' . \Carbon\Carbon::now()->format('Y-m-d') . '-'. uniqid() . '.' . $value['tipe'];
                    $original = Image::make(base64_decode($img))->save($dirFile . '/' . $imageName);
                    $resizeImage = Image::make(base64_decode($img))->resize(200, 200, function($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumb = Image::make($resizeImage)->save($dirFile_thumb .'/'. $imageName);   

                    // Insert Addintional Image
                    $this->AdditionalImageModel->store($detect_uniq_code, $imageName, 1);
                }
                // Find Add_Image 
                $find_add_image = AdditionalImageModel::where('UNIQ_CODE', $detect_uniq_code)->where('STATUS', 1)->get();
                foreach ($find_add_image as $key => $val) {
                    $get_add_image[$key]['image_name'] = $val->IMAGE;
                }
                $post_products               = ProductModel::find($detect_uniq_code);
                $post_products->IMAGE        = $get_add_image[0]['image_name'];
                $post_products->save();
            } 

            // Insert Stok & Activity Stok
            $find_stok = Stok::where('UNIQ_CODE', $detect_uniq_code)->first();

            ## Insert Activity Stok
            \DB::table('activity_stok')->insert([
                "UNIQ_CODE"     => $detect_uniq_code,
                "STOCK"         => ($find_stok && $request->mode == 'Ubah') ? ((int) $request->stok - (int) $find_stok->STOCK_IN) : $request->stok,
                "STOCK_DATE"    => \Carbon\Carbon::now()->format('Y-m-d G:i:s')    
            ]);

            if ($find_stok) {
                ## Update Stok
                \DB::table('stok')->where('UNIQ_CODE', $detect_uniq_code)->update([
                    "STOCK_IN"      => $request->stok,
                    "STOCK_UPDATE"  => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);
            } else {
                ## Insert Stok
                \DB::table('stok')->insert([
                    "UNIQ_CODE"     => $detect_uniq_code,
                    "STOCK_IN"      => $request->stok,
                    "STOCK_DATE"    => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
                ]);
            }
            

            // if (count($request->stok) > 0 && $request->mode == "Tambah") {
            //     foreach ($request->stok as $key => $value) {
            //         // Insert Color Product
            //         $post_color             = New ProductColor;
            //         $post_color->TITLE      = $value['TITLE'];
            //         $post_color->COLOR      = $value['COLOR'];
            //         $post_color->UNIQ_CODE  = $detect_uniq_code;
            //         $post_color->save();

            //         // Insert Stock Product
            //         $post_stok                  = New Stok;
            //         $post_stok->UNIQ_CODE       = $detect_uniq_code;
            //         $post_stok->PRODUCT_COLOR   = $post_color->ID;
            //         $post_stok->STOCK_IN        = $value['STOK'];
            //         $post_stok->STOCK_DATE      = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            //         $post_stok->save();

            //         // Insert Activity Stok
            //         $post_activity_stok                 = New ActivityStok;
            //         $post_activity_stok->UNIQ_CODE      = $detect_uniq_code;
            //         $post_activity_stok->STOCK          = $value['STOK'];
            //         $post_activity_stok->PRODUCT_COLOR  = $post_color->ID;
            //         $post_activity_stok->STOCK_DATE     = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            //         $post_activity_stok->save();
            //     }
            // } elseif (count($request->stok) > 0 && $request->mode == "Ubah") {
            //     foreach ($request->stok as $key => $value) {
            //         if ($value['ID_STOK'] != '') {
            //             // Find Data Stok Product
            //             $search_stok = Stok::where('ID', $value['ID_STOK'])->first();

            //             if ($search_stok) {
            //                 // Insert Color Product
            //                 $post_color             = ProductColor::find($search_stok->PRODUCT_COLOR);
            //                 $post_color->TITLE      = $value['TITLE'];
            //                 $post_color->COLOR      = $value['COLOR'];
            //                 $post_color->UNIQ_CODE  = $detect_uniq_code;
            //                 $post_color->save();

            //                 // Insert Stock Product
            //                 $post_stok                  = Stok::find($search_stok->ID);
            //                 $post_stok->UNIQ_CODE       = $detect_uniq_code;
            //                 $post_stok->PRODUCT_COLOR   = $post_color->ID;
            //                 $post_stok->STOCK_IN        = $value['STOK'];
            //                 $post_stok->STOCK_UPDATE    = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            //                 $post_stok->save();
            //             } else {
            //                 // Insert Color Product
            //                 $post_color             = New ProductColor;
            //                 $post_color->TITLE      = $value['TITLE'];
            //                 $post_color->COLOR      = $value['COLOR'];
            //                 $post_color->UNIQ_CODE  = $detect_uniq_code;
            //                 $post_color->save();

            //                 // Insert Stock Product
            //                 $post_stok                  = New Stok;
            //                 $post_stok->UNIQ_CODE       = $detect_uniq_code;
            //                 $post_stok->PRODUCT_COLOR   = $post_color->ID;
            //                 $post_stok->STOCK_IN        = $value['STOK'];
            //                 $post_stok->STOCK_DATE      = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            //                 $post_stok->save();
            //             }
            //         } else {
            //             // Insert Color Product
            //             $post_color             = New ProductColor;
            //             $post_color->TITLE      = $value['TITLE'];
            //             $post_color->COLOR      = $value['COLOR'];
            //             $post_color->UNIQ_CODE  = $detect_uniq_code;
            //             $post_color->save();

            //             // Insert Stock Product
            //             $post_stok                  = New Stok;
            //             $post_stok->UNIQ_CODE       = $detect_uniq_code;
            //             $post_stok->PRODUCT_COLOR   = $post_color->ID;
            //             $post_stok->STOCK_IN        = $value['STOK'];
            //             $post_stok->STOCK_DATE      = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            //             $post_stok->save();
            //         }
            //         // Insert Activity Stok
            //         $post_activity_stok                 = New ActivityStok;
            //         $post_activity_stok->UNIQ_CODE      = $detect_uniq_code;
            //         $post_activity_stok->STOCK          = $value['STOK'];
            //         $post_activity_stok->PRODUCT_COLOR  = $post_color->ID;
            //         $post_activity_stok->STOCK_DATE     = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            //         $post_activity_stok->save();
            //     }
            // } 
    
            return response()->json([
                "status"    => 201,
                "message"   => "Created Data Success"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ]);
        }
        
    }

    public function publish(Request $request) {
        if ($request->_token != '') {
            $status = [
                "true"  => 1,
                "false" => 0
            ];
    
            $message = [
                "true"=>"Data berhasil dipublish!",
                "false"=>"Data berhasil di unpublish"
            ];
    
            ## proses update status data menjadi 1(publish)
            $update_data           = ProductModel::where('UNIQ_CODE', $request->id)->first();
            $update_data->STATUS   = $status[$request->status];
            $update_data->save();
    
            return response()->json([
                "status"    => 200,
                "message"   => $message[$request->status],
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Failed!",
            ], 200);
        }
    }

    public function delete(Request $request) {
        if ($request->_token != '') {
            // ## proses update status data menjadi 99(delete)
            $update_data           = ProductModel::where('UNIQ_CODE', $request->id)->first();
            $update_data->STATUS   = 99;
            $update_data->save();

            return response()->json([
                "status"    => 200,
                "message"   => "success delete data"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ], 200);
        }   
    }

    // Additional Image
    public function delete_additional_image(Request $request) {
        if ($request->_token != '') {
            // Update Status Image ke 99 (Hapus)
            $update = AdditionalImageModel::where('ID', $request->id)->first();
            $update->STATUS = 99;
            $update->save();

            // Find Additional Image
            $find_add_image = AdditionalImageModel::where('UNIQ_CODE', $update->UNIQ_CODE)->where('STATUS', 1)->get();

            if (count($find_add_image) > 0) {
                foreach ($find_add_image as $key => $val) {
                    $get_images[$key] = $val->IMAGE; 
                }
                $get_image = $get_images[0];
            } else {
                $get_image = 'default.png';
            }

            // Update Image Product
            $updateProduct = ProductModel::where('UNIQ_CODE', $update->UNIQ_CODE)->first();
            $updateProduct->IMAGE = $get_image;
            $updateProduct->save();
            
            return response()->json([
                "status"    => 200,
                "message"   => "Delete Success",     
                $request->all()           
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ]);
        }
        
    }

    public function remove_color($id) {
        // Find Stock Product
        $find_stock = Stok::where('ID', $id)->first();
        
        // Find Activity Stock Remove
        \DB::table('activity_stok')->where('PRODUCT_COLOR', $find_stock->PRODUCT_COLOR)->delete();

        // Find Stock Product Remove
        \DB::table('stok')->where('ID', $find_stock->ID)->delete();

        // Find Color Product
        \DB::table('product_color')->where('ID', $find_stock->PRODUCT_COLOR)->delete();

        return response()->json([
            "status"    => 200,
            "message"   => "Oke"
        ], 200);
    }

    // product 
    public function videos_datatables($id) {
        $data = $this->ProductVideos->datatables($id);

        return response()->json([
            "status"    => 200,
            "message"   => "OK",
            "data"      => $data
        ]);
    }

    public function videos_store(Request $request) {
        if ($request->_token != '') {
            $post_product_videos                = new ProductVideos;
            $post_product_videos->UNIQ_CODE     = $request->uniq_code;
            $post_product_videos->CREATED_DATE  = \Carbon\Carbon::now()->format('Y-m-d G:i:s');
            
            $post_product_videos->TITLE         = $request->judul;
            $post_product_videos->URL           = $request->embled_yt;
            $post_product_videos->STATUS        = $request->status;
            $post_product_videos->save(); 
    
            return response()->json([
                "status"    => 201,
                "message"   => "Created Data Success"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed!"
            ]);
        }   
    } 

    public function delete_videos(Request $request) {
        if ($request->_token != '') {
            $id = $request->id;

            $delete_data = $this->ProductVideos->remove($id);

            if ($delete_data == true) {
                return response()->json([
                    "status"    => 200,
                    "message"   => "Delete Success"
                ], 200);
            } else {
                return response()->json([
                    "status"    => 404,
                    "message"   => "Delete Failed!"
                ], 200);
            }
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Failed!",
            ], 200);
        }
    }

    public function publish_videos($id) {
        $publish_data = $this->ProductVideos->publish($id);

        if ($publish_data['status'] == true) {
            return response()->json([
                "status"    => 200,
                "message"   => $publish_data['message']
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Delete Failed!"
            ], 200);
        }
        
    }
}
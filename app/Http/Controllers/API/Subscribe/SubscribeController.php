<?php
namespace App\Http\Controllers\API\Subscribe;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Subscribe;

class SubscribeController extends Controller
{
    public function __construct() {
        $this->subscribe = New Subscribe;
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            $data = [
                "EMAIL"         => $request->email,
                "IP_ADDRESS"    => $request->ip_address,
                "CREATE_DATE"   => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
            ];

            $insert = $this->subscribe->insert($data);
            
            if ($insert == 1) {
                return response()->json([
                    "status"    => "200",
                    "message"   => "Created Oke!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]
                ], 200);
            } else {
                return response()->json([
                    "status"    => "404",
                    "message"   => "Ups Failed!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]
                ], 200);
            }
        } else {
            return response()->json([
                "status"    => "404",
                "message"   => "Ups Not Found!",
                "links"     => [
                    "self"  => url($request->fullURL())
                ]
            ], 200);
        }
    }
}
<?php
namespace App\Http\Controllers\API\Category;

use App\Http\Resources\CategoryResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{
    public function __construct() {
        $this->category = New Category;
    }

    public function all_parent($type) {
        if ($type == 'product' || $type == 'news_tips') {
            $category = $this->category->all_parent($type);

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $category
        ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found!",
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "data"      => []
            ], 200);
        }
    }

    public function all_sub($type) {
        if ($type == 'product' || $type == 'news_tips') {
            $category = $this->category->all_sub($type);

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $category
        ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found!",
                "links"     => [
                    "self"  => url(Request()->fullURL())
                ],
                "data"      => []
            ], 200);
        }
    }

    // public function get_all_data_news_tips() {
    //     $category = Category::where('TIPE', 'news_tips')->where('STATUS', 1)->get();

    //     return response()->json([
    //         "status"    => 200,
    //         "message"   => "OK",
    //         "data"      => CategoryResource::collection($category) 
    //     ], 200);
    // }
}
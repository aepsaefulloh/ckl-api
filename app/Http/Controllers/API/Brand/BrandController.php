<?php
namespace App\Http\Controllers\API\Brand;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Brand;

class BrandController extends Controller
{
    public function __construct() {
        $this->brand = New Brand;
        $this->dir_origin = 'storage/images/brand/origin/';
        $this->dir_thumb = 'storage/images/brand/thumb/';
    }
    
    public function all_data() {
        $brand = $this->brand->get_all($this->dir_origin, $this->dir_thumb);

        return response()->json([
            "status"    => 200,
            "message"   => "OK",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $brand
        ], 200);
    }
}
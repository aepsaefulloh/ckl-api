<?php
namespace App\Http\Controllers\API\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\AboutForm;

class AboutFormController extends Controller
{
    public function __construct() {
        $this->about_form = New AboutForm;
    }

    public function store(Request $request) {
        if ($request->_token != '') {
            $data = [
                "FULLNAME"      => ($request->last_name != '') ? $request->first_name.' '.$request->last_name : $request->first_name,
                "TELEPHONE"     => $request->telephone,
                "EMAIL"         => $request->email,
                "MESSAGE"       => $request->message,
                "IP_ADDRESS"    => $request->ip_address,
                "CREATE_DATE"   => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
            ];

            $insert = $this->about_form->insert($data);
            
            if ($insert == 1) {
                return response()->json([
                    "status"    => "200",
                    "message"   => "Created Oke!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]
                ], 200);
            } else {
                return response()->json([
                    "status"    => "404",
                    "message"   => "Ups Failed!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]
                ], 200);
            }
        } else {
            return response()->json([
                "status"    => "404",
                "message"   => "Not Access!",
                "links"     => [
                    "self"  => url($request->fullURL())
                ]
            ], 200);
        }
    }
}
<?php
namespace App\Http\Controllers\API\Bank;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bank;
use DB;

class BankController extends Controller
{
    public function get_all(Request $request) {
        ## Get Data Province
        $getBank = Bank::All();

        if ($getBank->count() > 0) {
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "data"  => $getBank
            ]);
        } else {
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "data"  => []
            ]);
        }
    }

}
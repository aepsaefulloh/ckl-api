<?php
namespace App\Http\Controllers\API\Wilayah;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Province;
use City;

class WilayahController extends Controller
{
    public function get_prov() {
        $data = Province::all();

        return response()->json([
            "status"    => 200,
            "message"   => "OKE",
            "links"     => [
                "self"      => url(Request()->fullURL()),
                "source"    => "RajaOngkir",
                "data"      => "Province"
            ],
            "data"      => $data
        ], 200);
    }

    public function get_city($id) {
        $data = City::where('province', $id);

        return response()->json([
            "status"    => 200,
            "message"   => "OKE",
            "links"     => [
                "self"      => url(Request()->fullURL()),
                "source"    => "RajaOngkir",
                "data"      => "City"
            ],
            "data"      => $data
        ], 200);
    }
}
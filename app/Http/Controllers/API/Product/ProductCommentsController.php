<?php
namespace App\Http\Controllers\API\Product;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductComments;

class ProductCommentsController extends Controller
{
    public function __construct(){
        $this->comments = New ProductComments;
    }

    public function result_by_product($id, Request $request) {
        $limit = ($request->limit != '') ? $request->limit : 10 ;
        $comment = $this->comments->result_by_product($id, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "oke!",
            "links"     => [
                "self"  => url($request->fullURL())
            ],
            "data"      => $comment
        ], 200);
    } 

    public function store(Request $request) {
        if ($request->_token != '') {
            $data = [
                "CUSTOMER_ID"   => $request->customer_id,
                "UNIQ_CODE"     => $request->product_id,
                "COMMENTS"      => $request->comments,
                "COMMENTS_DATE" => \Carbon\Carbon::now()->format('Y-m-d G:i:s')
            ];

            $post_comments = $this->comments->insert($data);
            
            if ($post_comments == 1) {
                return response()->json([
                    "status"    => 200,
                    "message"   => "Success Created!"
                ], 200);
            } else {
                return response()->json([
                    "status"    => 404,
                    "message"   => "Ups Failed Insert!"
                ], 200);
            }
            
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Access!"
            ], 200);
        }
        
    }
}
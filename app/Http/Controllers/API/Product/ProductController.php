<?php
namespace App\Http\Controllers\API\Product;

use App\Http\Resources\AddCartProductResource;
use App\Http\Resources\DetailProductResource;
use App\Http\Resources\ProductResource;
use App\Http\Controllers\Controller;
use App\Models\AddCartProduct;
use Illuminate\Http\Request;
use App\Models\ProductModel;
use App\Models\AdditionalImageModel;
use App\Models\Customer;
use App\Models\Category;
use Carbon\Carbon;
use DB;

class ProductController extends Controller
{
    ## Public API Client
    public function __construct(){
        $this->dir_product_origin = "storage/images/product/origin/"; 
        $this->dir_product_thumb = "storage/images/product/thumb/";
        $this->product = New ProductModel;
    }

    public function sale_product(Request $request) {
        $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 6 ;
        $sale = (isset($request->discount) && $request->discount != '') ? $request->discount : null;
        $data = $this->product->sale($this->dir_product_thumb, $this->dir_product_origin, $sale, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }

    public function popular_product(Request $request) {
        $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 6 ;
        $data = $this->product->popular($this->dir_product_thumb, $this->dir_product_origin, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }

    public function news_product(Request $request) {
        $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 6 ;
        $data = $this->product->news_data($this->dir_product_thumb, $this->dir_product_origin, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }

    public function all_product(Request $request) {
        $page = (isset($request->page) && $request->page != '') ? $request->page : 1 ;
        $per_page = (isset($request->per_page) && $request->per_page != '') ? $request->per_page : 20 ; 
        $brand = (isset($request->brand) && $request->brand != '') ? $request->brand : null ;
        $category = (isset($request->category) && $request->category != '') ? $request->category : null ;

        // Get All Product
        $product = $this->product->all_data(
           'array', 
           $this->dir_product_origin, 
           $this->dir_product_thumb, 
           $request->search,
           $brand,
           $category,
           $page,
           $per_page,
           $request->price_awal,
           $request->price_akhir
        );


        // Get Total Data
        $total_data = $this->product->all_data(
            'count',
            '',
            '',
            $request->search,
            $brand,
            $category,
            '',
            '',
            $request->price_awal,
            $request->price_akhir
        );

       return response()->json([
           "status"     => 200,
           "message"    => "Oke",
           "link"       => [
               "self"       => url($request->fullURL()),
               "search"     => $request->search,
               "brand"      => $request->brand,
               "category"   => $request->category,
               "page"       => $page
           ],
           "total_data" => $total_data, 
           "per_page"   => $per_page,
           "data"       => $product
        ], 200);
    }

    public function hit_top_product(Request $request) {
        $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 6 ;
        $data = $this->product->hit_top($this->dir_product_thumb, $this->dir_product_origin, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }

    public function related_product(Request $request, $category_id, $product_id) {
        $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 6 ;
        $data = $this->product->related_category_product($this->dir_product_thumb, $this->dir_product_origin, $category_id, $product_id, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }

    public function find_product(Request $request, $id) {
        if (!preg_match("/Googlebot|MJ12bot|yandexbot/i", $request->server('HTTP_USER_AGENT')) && $request->server('HTTP_USER_AGENT') != '') {
            ## Update Hit Product
            $updateHIT = DB::table('product')->where('UNIQ_CODE', $id)
            ->update([
                'HIT'   => DB::raw('HIT+1')
            ]);

            ## get data Product find ID
            $data = $this->product->find_data($this->dir_product_origin, $this->dir_product_thumb, $id);
            
            if ($data) {
                return response()->json([
                    "status"    => 200,
                    "message"   => "OK",
                    "links"     => [
                        "self"          => url($request->fullURL()),
                        "uniq_code"     => $id
                    ],
                    "data"      => $data
                ], 200);
            } else {
                return response()->json([
                    "status"    => 404,
                    "message"   => "Not Found Data!",
                    "links"     => [
                        "self"          => url($request->fullURL()),
                        "uniq_code"     => $id
                    ],
                    "data"      => []
                ], 200);
            }
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found Data!",
                "links"     => [
                    "self"  => url($request->fullURL()) 
                ],
                "data"      => []
            ], 404);
        }
    }



    ## Private API Client
    public function get_add_cart_product_customer_data(Request $request) {
         ## Get Data Customer 
         $customer = Customer::where('TOKEN', explode(' ', $request->header('Authorization'))[1])->pluck('ID')->first();

         if ($customer) {
             ## Data Add Cart Product
             $add_cart = AddCartProduct::where('CUSTOMER_ID', $customer)->get();

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "data"      => AddCartProductResource::collection($add_cart)
            ], 200);
         } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found Data!",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "data"      => []
            ], 404);
         }
         
    }

    public function add_cart_product_count(Request $request) {
        ## Get Data Customer 
        $customer = Customer::where('TOKEN', explode(' ', $request->header('Authorization'))[1])->pluck('ID')->first();

        if ($customer) {
            ## Total Add Cart
            $add_cart = AddCartProduct::where('CUSTOMER_ID', $customer)->sum('JUMLAH');

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "_jumlah"   => $add_cart
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found Data!",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "_jumlah"   => 0
            ], 404);
        }
    }

    public function stock_cart_product_customer(Request $request){
        $query = "SELECT
                    * 
                FROM
                    add_cart_product
                    LEFT JOIN (
                        SELECT
                            product.*,
                            toko.PROV_ID,
                            toko.KOTA_ID,
                            toko.NAMA as NAMA_TOKO,
                            toko.COD 
                        FROM
                            ( SELECT * FROM product WHERE STATUS = 1 ) AS product
                            INNER JOIN ( SELECT * FROM `toko` WHERE STATUS = 1 ) AS toko ON `product`.`ID_TOKO` = `toko`.`ID` 
                            ) AS product ON `add_cart_product`.`UNIQ_CODE` = `product`.`UNIQ_CODE` 
                WHERE
                    add_cart_product.CUSTOMER_ID = $request->customer_id 
                    AND add_cart_product.STATUS = 1";
                
                if(isset($request->chart_id)){
                    
                    $array_chart ="(";

                    foreach(json_decode($request->chart_id,true) as $k => $v){
                        $array_chart .= $v.',';
                    }

                    $array_chart = substr($array_chart, 0, -1);
                    $array_chart .= ")";
                    $query .= " AND add_cart_product.ID IN ".$array_chart;
                };
        $stock_cart = DB::select(DB::raw($query));
        $stock_cart = json_decode(json_encode($stock_cart), true);
        $final = [];
        $total_hrg_cart = 0;
        foreach($stock_cart as $k => $v){
            $v["DIR_IMAGE_ORIGIN"] = url()."/".$this->dir_product_origin.$v["CATEGORY"]."/".$v["IMAGE"];
            $v["DIR_IMAGE_THUMB"] = url()."/".$this->dir_product_thumb.$v["CATEGORY"]."/".$v["IMAGE"];
            $v["TOTAL_HRG"] = $v["PRICE"]*$v["JUMLAH"];
            $total_hrg_cart = $total_hrg_cart+($v["PRICE"]*$v["JUMLAH"]);
            array_push($final,$v);
        }

        if($stock_cart){
            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "_data"     => $final,
                "_jumlah"   => count($final),
                "_total_hrg_cart" => $total_hrg_cart
            ], 200);
        }else{
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found Data!",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
                "_data"     => 0,
                "_jumlah"   => 0
            ], 404);
        }
    }

    public function add_cart_product_customer(Request $request) {
        ## Cek ID Product 
        $product = ProductModel::where('UNIQ_CODE', $request->uniq_code)->where('STATUS',1)->first();

        ## Get Data Customer 
        // $customer = Customer::where('TOKEN', explode(' ', $request->header('Authorization'))[1])->pluck('ID')->first();

        if ($product) {
            
            $post               = new AddCartProduct;
            $post->UNIQ_CODE    = $request->uniq_code;
            $post->JUMLAH       = $request->qty;
            $post->COLOR        = $request->color;
            $post->CUSTOMER_ID  = $request->customer_id;
            $post->CREATED_AT   = Carbon::now()->format('Y-m-d G:i:s');
            $post->save();

            return response()->json([
                "status"    => 200,
                "message"   => "sukses add cart",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
            ], 200);   
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found!",
                "links"     => [
                    "self"  => url($request->path()) 
                ],
            ], 404);
        }
    }

    public function remove_cart_product_customer(Request $request) {
        ## Hapus data add cart 
        AddCartProduct::where('ID', $request->id)->where("CUSTOMER_ID",$request->customer_id)->update(['STATUS' => 0]);

        return response()->json([
            "status"    => 200,
            "message"   => "Sukses Delete",
            "links"     => [
                "self"  => url($request->path()) 
            ],
        ]);
    }
}
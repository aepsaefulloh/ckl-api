<?php
namespace App\Http\Controllers\API\NewsTips;

use App\Http\Resources\DetailNewsTipsResource;
use App\Http\Resources\NewsTipsResource;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\NewsTips;

class NewsTipsController extends Controller
{
    public function __construct() {
        $this->news_tips = New NewsTips;
        $this->dir_origin = 'storage/images/news_tips/origin/';
        $this->dir_thumb = 'storage/images/news_tips/thumb/';
    }
    ## Public Api Clients
    public function get_all_data(Request $request) {
        if (!preg_match("/Googlebot|MJ12bot|yandexbot/i", $request->server('HTTP_USER_AGENT')) && $request->server('HTTP_USER_AGENT') != '') {
            $page = (isset($request->page) && $request->page != '') ? $request->page : 1 ;
            $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 3 ;

            if ($request->type == "popular") {
                $news_tips = $this->news_tips->news_and_popular('HIT', $this->dir_origin, $this->dir_thumb, $limit);
                
                // Total Data
                $totalData = count($news_tips);
            } elseif ($request->type == 'news') {
                $news_tips = $this->news_tips->news_and_popular('PUBLISH_TIMESTAMP', $this->dir_origin, $this->dir_thumb, $limit);
                
                // Total Data
                $totalData = count($news_tips);
            } else {
                $news_tips = $this->news_tips->all_data('array', $this->dir_origin, $this->dir_thumb, $limit, $page);
                
                // Total Data News Tips
                $totalData = $this->news_tips->all_data('count', $this->dir_origin, $this->dir_thumb, '', '');

                // Total Page News Tips
                $totalPage = ceil($totalData/$limit);
            }

            return response()->json([
                "status"    => 200,
                "message"   => "OK",
                "links"     => [
                    "self"      => url($request->fullURL()),
                    "page"      => $page 
                ],
                "total_data"    => count($news_tips),
                "total_page"    => (isset($totalPage)) ? $totalPage : 0,
                "total_data_all"=> (isset($totalData)) ? $totalData : 0,
                "data"      => $news_tips
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Failed!",
                "links"     => [
                    "self"  => url($request->fullURL()) 
                ],
                "data"      => []
            ], 404);
        }
    }

    public function get_find_data(Request $request, $title) {
        if (!preg_match("/Googlebot|MJ12bot|yandexbot/i", $request->server('HTTP_USER_AGENT')) && $request->server('HTTP_USER_AGENT') != '') {
            $news_tips = NewsTips::where('SLUG', $title)
            ->where('status', 1)
            ->get();

            if (count($news_tips) > 0) {
                ## update HIT news Tips
                $update = NewsTips::find($news_tips[0]->ID);
                $update->HIT = $news_tips[0]->HIT+1;
                $update->save();

                ## get Data News tips updated
                $get = NewsTips::where('ID', $news_tips[0]->ID)->where('status', 1)->get();

                return response()->json([
                    "status"    => 200,
                    "message"   => "OK",
                    "links"     => [
                        "self"  => url($request->fullURL()) 
                    ],
                    "data"      => NewsTipsResource::collection($get)[0]
                ], 200);
            } else {
                return response()->json([
                    "status"    => 200,
                    "message"   => "Not Content Data!",
                    "links"     => [
                        "self"  => url($request->path()) 
                    ],
                    "data"      => ''
                ], 200);
            }
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Failed!",
                "links"     => [
                    "self"  => url($request->urlFull()) 
                ],
                "data"      => []
            ], 404);
        }
    }

    public function review_product(Request $request) {
        $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 6 ;
        $data = $this->news_tips->review_data($this->dir_thumb, $this->dir_origin, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }

    public function search_static_page(Request $request) {
        if ($request->_token != '') {
            $data = $this->news_tips->search_by_title($request->keyword);

            return response()->json([
                "status"    => 200,
                "message"   => "Oke!",
                "data"      => $data
            ], 200);
        } else {
            return response()->json([
                "status"    => 401,
                "message"   => "Not Access Private!"
            ], 200);
        }
        
    }

    public function artikel_product(Request $request) {
        $limit = (isset($request->limit) && $request->limit != '') ? $request->limit : 6 ;
        $data = $this->news_tips->artikel_product($request->tag, $this->dir_thumb, $this->dir_origin, $limit);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "links"     => [
                "self"  => url(Request()->fullURL())
            ],
            "data"      => $data
        ]);
    }
}
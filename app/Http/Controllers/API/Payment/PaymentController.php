<?php
namespace App\Http\Controllers\API\Payment;
use App\Http\Controllers\Controller;
use App\Models\AddCartProduct;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\MailJet;

class PaymentController extends Controller
{
    ## Private
    public function __construct(){
        $this->add_cart = New AddCartProduct;
        $this->transaction = new Transaction();
        $this->MailJet = New MailJet();
    }

    public function insert_transaksi(Request $request) {
        if ($request->_token != '') {
            $insert = $this->transaction->insert($request->transaksi, $request->product);
            $request_transaksi = $request->transaksi;
            $find_bank = \DB::table('bank')->where('ID', $request_transaksi['BANK_ID'])->first();

            $body = [
                'Messages' => [
                    [
                        'From' => [
                            'Email' => env('MAILJET_EMAIL'),
                            'Name' => env('MAILJET_NAME')
                        ],
                        'To' => [
                            [
                                'Email' => $request_transaksi['EMAIL'],
                                'Name' => $request_transaksi['FULLNAME']
                            ]
                        ],
                        'Subject' => "Thank you for shopping with Teman Musisi",
                        'TextPart' => "Orderan Nomer ".$insert['transaction_number'],
                        'HTMLPart' => "<p>Thank you for shopping with Teman Musisi</p>
                                        <p>Hallo, ".$request_transaksi['FULLNAME']."</p>
                                        <p>Thank you for your order! Your items will be delivered after the payment has been made. Please make your payment within 24 hours to avoid order cancellation and send the payment receipt to <a href='mailto:".env('MAILJET_EMAIL')."'>".env('MAILJET_EMAIL')."</a></p>
                                        <p>Order No : ".$insert['transaction_number']."</p>
                                        <p>SubTotal : Rp. ".number_format(collect($request->product)->sum('PRICE_TOTAL'), 0, ',', '.')."</p>
                                        <p>Shipping : Rp. ".number_format($request_transaksi['PRICE_ONGKIR'], 0, ',', '.')."</p>
                                        <p>Total : Rp. ".number_format((collect($request->product)->sum('PRICE_TOTAL')+$request_transaksi['PRICE_ONGKIR']), 0, ',', '.')."</p>
                                        <p>Please settle the payment by transferring it to the following bang account with your order number as the reference.</p>
                                        <p>".$find_bank->BANK_NAME." ".$find_bank->NO_REK." a/n ".$find_bank->NAMA."</p>
                                        <hr />
                                        <p align='center'>Teman Musisi Terms</p>
                                        <p align='center'>All sales are final. No refunds. No exchanges</p>
                                        <br/ >
                                        <p align='center'>Shipping</p>
                                        <p align='center'>Kindly make sure that the shipping address is correct and somebody is home to receive the package form 9 am to 6 pm on the estimated delivery date.</p>
                                        <br/> 
                                        <p align='center'>Please transfer to</p>
                                        <p align='center'>".$find_bank->BANK_NAME." ".$find_bank->NO_REK." a/n ".$find_bank->NAMA."</p>
                                        <br/>
                                        <p align='center'>Please complete your payment within 24 hours. The order will automatically be canceled if there's no payment within 24 hours.</p>",
                        'CustomID' => "Orderan Nomer ".$insert['transaction_number']
                    ]
                ]
            ];
            
            // Send Email To Customer 
            $sendMail = $this->MailJet->send($body);

            // Send Email Admin
            $find_admin = \DB::table('admin')->where('ROLE', 'superadmin')->where('STATUS', 1)->get();

            foreach ($find_admin as $admins) {
                $body_admin =  [
                    'Messages' => [
                        [
                            'From' => [
                                'Email' => env('MAILJET_EMAIL'),
                                'Name' => env('MAILJET_NAME')
                            ],
                            'To' => [
                                [
                                    'Email' => $admins->EMAIL,
                                    'Name' => $admins->FULLNAME
                                ]
                            ],
                            'Subject' => "Orderan Masuk di Teman Musisi",
                            'TextPart' => "Orderan Nomer ".$insert['transaction_number'],
                            'HTMLPart' => "<p>Hallo, ".$admins->FULLNAME."</p>
                                           <p>Ada Orderan Masuk dengan Nomer Order : ".$insert['transaction_number']." dengan rincian sebagai berikut : </p>
                                           <ul>
                                                <li>Nama Lengkap : ".$request_transaksi['FULLNAME']."</li>
                                                <li>Email : ".$request_transaksi['EMAIL']."</li>
                                                <li>No. Telepon : ".$request_transaksi['PHONE']."</li>
                                                <li>Waktu Order : ".$insert['transaction_date']."</li>
                                           </ul>
                                           <p>Mohon untuk di cek data rincian transaksi order pada CMS Admin dan mengecek transaksi pembayarannya juga.</p>
                                           <p>Terima kasih</p>",
                            'CustomID' => "Orderan Nomer ".$insert['transaction_number']
                        ]
                    ]
                ];
    
                $this->MailJet->send($body_admin);
            }
            
            if ($insert['status'] == 1) {
                return response()->json([
                    "status"    => 200,
                    "message"   => "Order Success!",
                    "data"      => [
                        "transaction_id"    => $insert['transaction_id'],
                        "transaction_number"=> $insert['transaction_number']
                    ]
                ], 200);
            } else {
                return response()->json([
                    "status"    => 401,
                    "message"   => "Ups Failed"
                ], 200);
            }
            
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Access!"
            ], 200);
        }
        
    }

    public function transaction_by_number($tr_number, Request $request) {
        if ($request->_token != '') {
            $result = $this->transaction->result_by_number($tr_number);

            return response()->json([
                "status"    => 200,
                "message"   => "Oke",
                "data"      => $result
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Access!"
            ], 200);
        }
    }
    
}
<?php
namespace App\Http\Controllers\API\Banner;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    public function __construct() {
        $this->banner = new Banner;
        $this->dir_banner_origin = "storage/images/banner/origin/";
        $this->dir_banner_thumb = "storage/images/banner/thumb/";
    }

    public function get_by_category(Request $request) {
        if (!preg_match("/Googlebot|MJ12bot|yandexbot/i", $request->server('HTTP_USER_AGENT')) && $request->server('HTTP_USER_AGENT') != '') {
            $getBanner = $this->banner->get_all($this->dir_banner_origin, $this->dir_banner_thumb, 1, $request->params);
            return response()->json([
                "status"    => 200,
                "message"   => "Oke",
                "links"     => [
                    "self"  => url($request->path())
                ],
                "data"      => $getBanner
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Not Found!",
                "links"     => [
                    "self"  => url($request->path())
                ],
                "data"      => []
            ]);
        }
    }
}
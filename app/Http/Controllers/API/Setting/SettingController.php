<?php
namespace App\Http\Controllers\API\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Setting;

class SettingController extends Controller
{
    public function __construct() {
        $this->setting = New Setting;
        $this->dir_origin = 'storage/images/logo/origin/';
        $this->dir_thumb = 'storage/images/logo/thumb/';
    }

    public function find_data($id) {
        $data = $this->setting->find_data($id, $this->dir_origin, $this->dir_thumb);

        return response()->json([
            "status"    => 200,
            "message"   => "oke",
            "data"      => $data
        ], 200);
    }
}
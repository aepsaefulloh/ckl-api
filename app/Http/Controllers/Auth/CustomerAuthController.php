<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\Customer;

class CustomerAuthController extends Controller
{
    public function __construct() {
        $this->customer = New Customer;
    }

    public function register(Request $request) {
        if ($request->_token != '') {
            $data = [
                "EMAIL"     => $request->email,
                "USER"      => $request->username,
                "PASSWORD"  => Hash::make($request->password),
                "REG_DATE"  => \Carbon\Carbon::now()->format('Y-m-d G:i:s'),
                "FULLNAME"  => $request->fullname,
                "ADDRESS"   => $request->address,
                "TELEPHONE" => $request->phone,
                "STATUS"    => 1
            ];

            $post_akun = $this->customer->register($data, $request->email);

            if ($post_akun == 1) {
                return response()->json([
                    "status"    => 200,
                    "message"   => "Register Account Success!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]
                ], 200);
            } elseif ($post_akun == 99) {
                return response()->json([
                    "status"    => 404,
                    "message"   => "email_already",
                    "links"     => [
                        "self"  => url($request->fullURL()),
                        "email" => $request->email
                    ]  
                ], 200);
            } else {
                return response()->json([
                    "status"    => 504,
                    "message"   => "Ups Not Valid!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]  
                ], 200);
            } 
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => 'Ups Failed!',
                "links"     => [
                    "self"  => url($request->fullULR())
                ]
            ], 200);
        }
    }

    public function login(Request $request)
    {
        if ($request->_token != '') {
            $search_akun = $this->customer->login($request->email, $request->password, $request->ip_address, $request->user_agent);

            if ($search_akun['CODE'] == 'valid') {
                return response()->json([
                    "status"    =>  200,
                    "message"   => "Login Success!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ],
                    "data"      => $search_akun  
                ], 200);
            } elseif ($search_akun['CODE'] == 'not_valid') {
                return response()->json([
                    "status"    => 504,
                    "message"   => "Ups Not Valid!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]  
                ], 200);
            } elseif ($search_akun['CODE'] == 'not_found') {
                return response()->json([
                    "status"    => 404,
                    "message"   => "Not Found Data!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]  
                ], 200);
            }
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => 'Ups Failed!',
                "links"     => [
                    "self"  => url($request->fullULR())
                ]
            ], 200);
        }
    }

    public function profile()
    {
        return response()->json([
            "_no"       => auth()->user()->ID,
            "_email"    => auth()->user()->EMAIL,
            "_username" => auth()->user()->USER,
            "_nama"     => auth()->user()->FULLNAME,
            "_alamat"   => auth()->user()->ADDRESS,
            "_no_hp"    => auth()->user()->TELEPHONE,
            "_status"   => auth()->user()->STATUS,
            "_tgl_reg"  => auth()->user()->REG_DATE,
            "_token"    => auth()->user()->TOKEN,
            ], 200);
    }

    public function logout(Request $request)
    {
        if ($request->_token != '') {
            $logout = $this->customer->logout($request->id);

            if ($logout == 1) {
                return response()->json([
                    "status"    => 200,
                    "message"   => "Logout Account Success!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]
                ], 200);
            } else {
                return response()->json([
                    "status"    => 404,
                    "message"   => "Ups Failed Logout!",
                    "links"     => [
                        "self"  => url($request->fullURL())
                    ]
                ], 200);
            }
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Ups Failed Not Access!",
                "links"     => [
                    "self"  => url($request->fullURL())
                ]
            ], 200);
        }    
    }
}
<?php
namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use App\Models\ActivityAdmin;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Admin;
use Carbon\Carbon;

class AdminAuthController extends Controller
{
    public function __construct() {
        $this->activity_admin = New ActivityAdmin;
    }

    public function login(Request $request)
    {
        ## get request login
        $email    = $request->email;
        $password = $request->password;

        ## Find Admin dengan email
        $admin = Admin::where('EMAIL', $email)
        ->where('ROLE', 'superadmin')
        ->where('STATUS', 1)
        ->first();

        ## Check Password dan data akun jika di temukan
        if ($admin && Hash::check($password, $admin->PASSWORD)) {
            if ($admin->TOKEN_KEY == '') {
                ## Generate Create Token di Admin
                $key_token = base64_encode(Str::random(10)).Crypt::encrypt('is_token_valid_sistem');

                ## Update Data Token dan Last Login
                $admin->update([
                    'TOKEN_KEY'     => $key_token,
                    'LAST_LOGIN'    => Carbon::now()->format('Y-m-d G:i:s')
                ]);
                $id = $admin->ID;
                $admin = Admin::where('ID', $id)->first();
            } 

            // Insert Data Ke table Aktifitas Admin
            $activity_admin                 = new ActivityAdmin;
            $activity_admin->IP_ADDRESS     = $request->ip_address;
            $activity_admin->ADMIN_ID       = $admin->ID;
            $activity_admin->LOGIN_DATE     = Carbon::now()->format('Y-m-d G:i:s');
            $activity_admin->save(); 

            return response()->json([
                "status"    => 200,
                "message"   => "Login Success",
                "links"     => [
                    "self"      => url($request->fullURL())
                ],
                "data"      => $admin
            ]);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Akun tidak terdaftar!",
                "links"     => [
                    "self"      => url($request->fullURL())
                ],
                "data"      => []
            ]);
        }
    }

    public function find_data($id) {
        $get = Admin::where('ID', $id)->first();

        return response()->json($get);
    }

    public function update_token(Request $request) {
        if ($request->_token != '') {
            $post = Admin::where('ID', $request->ID)->first();
            $post->TOKEN_KEY = $request->TOKEN_KEY;
            $post->LAST_LOGIN = $request->LAST_LOGIN;
            $post->save();

            return response()->json([
                "status"    => 200,
                "message"   => "success updated"
            ], 200);
        } else {
            return response()->json([
                "status"    => 404,
                "message"   => "Failed"
            ], 200);
        }
        
    }
    
    public function activity($id) {
        $data = $this->activity_admin->activity_me($id);

        return response()->json([
            "status"    => 200,
            "message"   => "Oke",
            "data"      => $data
        ], 200);
    }
}
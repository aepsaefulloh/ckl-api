<!--Font Popins--> 
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" />

<!--begin::Page Vendors Styles(used by this page)-->
<link href="{{ url('assets/CMS/plugins/custom/fullcalendar/fullcalendar.bundle.css') }}" rel="stylesheet" type="text/css" />
<!--end::Page Vendors Styles-->
<!--begin::Global Theme Styles(used by all pages)-->
<link href="{{ url('assets/CMS/plugins/global/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/CMS/plugins/custom/prismjs/prismjs.bundle.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/CMS/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
<!--end::Global Theme Styles-->
<!--begin::Layout Themes(used by all pages)-->
<link href="{{ url('assets/CMS/css/themes/layout/header/base/light.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/CMS/css/themes/layout/header/menu/light.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/CMS/css/themes/layout/brand/dark.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/CMS/css/themes/layout/aside/dark.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ url('assets/CMS/css/themes/layout/select2.css') }}" rel="stylesheet" type="text/css" />
<!--end::Layout Themes-->
<link rel="shortcut icon" href="{{ url('assets/CMS/media/logos/favicon.ico') }}" />
@extends('layouts.CMS_TOKO.app')

@section('title', 'Dashboard Admin')

@section('dashboard', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Informasi Data</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            <div class="card card-custom gutter-b">
                <div class="card-body">
                    <!--begin::Details-->
                    <div class="d-flex mb-9">
                        <!--begin: Pic-->
                        <div class="flex-shrink-0 mr-7 mt-lg-0 mt-3">
                            <div class="symbol symbol-50 symbol-lg-120" id="img_logo"></div>
                            <div class="symbol symbol-50 symbol-lg-120 symbol-primary d-none">
                                <span class="font-size-h3 symbol-label font-weight-boldest">JM</span>
                            </div>
                        </div>
                       
                        <div class="flex-grow-1">
                            <!--begin::Title-->
                            <div class="d-flex justify-content-between flex-wrap mt-1">
                                <div class="d-flex mr-3">
                                    <a href="#" class="text-dark-75 text-hover-primary font-size-h5 font-weight-bold mr-3">
                                        <span id="nama_toko"></span>
                                    </a>
                                    <a href="#" id="icon_status">
                                        
                                    </a>
                                </div>
                                <div class="my-lg-0 my-3">
                                    <a href="{{ url('admin-toko/detail', $id) }}" class="btn btn-sm btn-light-success font-weight-bolder text-uppercase mr-3">
                                        <i class="flaticon-edit"></i> Update
                                    </a>
                                </div>
                            </div>

                            <div class="d-flex flex-wrap justify-content-between mt-1">
                                <div class="d-flex flex-column flex-grow-1 pr-8">
                                    <div class="d-flex flex-wrap mb-4">
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                            <i class="flaticon2-new-email mr-2 font-size-lg"></i> <span id="email_akun"></span>
                                        </a>
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                            <i class="flaticon2-calendar-3 mr-2 font-size-lg"></i> <span id="fullname_akun"></span>
                                        </a>
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold mr-lg-8 mr-5 mb-lg-0 mb-2">
                                            <i class="flaticon2-shopping-cart mr-2 font-size-lg"></i> <span id="bank_akun"></span>
                                        </a>
                                        <a href="#" class="text-dark-50 text-hover-primary font-weight-bold" id="status_cod"></a>
                                    </div>
                                    <span class="font-weight-bold text-dark-50" id="alamat_toko"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Data Informasi --}}
            <div class="row">
                {{-- Data Count Product --}}
                <div class="col-6">
                    <div class="card card-custom bg-danger card-stretch gutter-b">
                        <div class="card-body">
                            <span class="svg-icon svg-icon-2x svg-icon-white">
                                <!--begin::Svg Icon | path:/metronic/theme/html/demo1/dist/assets/media/svg/icons/Media/Equalizer.svg-->
                                <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <rect x="0" y="0" width="24" height="24"></rect>
                                        <rect fill="#000000" opacity="0.3" x="13" y="4" width="3" height="16" rx="1.5"></rect>
                                        <rect fill="#000000" x="8" y="9" width="3" height="11" rx="1.5"></rect>
                                        <rect fill="#000000" x="18" y="11" width="3" height="9" rx="1.5"></rect>
                                        <rect fill="#000000" x="3" y="13" width="3" height="7" rx="1.5"></rect>
                                    </g>
                                </svg>
                                <!--end::Svg Icon-->
                            </span>
                            <span class="card-title font-weight-bolder text-white font-size-h2 mb-0 mt-6 d-block" id="total_product">Loading...</span>
                            <span class="font-weight-bold text-white font-size-sm">Total Produk</span>
                        </div>
                        <!--end::Body-->
                    </div>
                    <!--end::Stats Widget 31-->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <style>
        #card_login_activity {
            height: 600px;
            overflow-y: scroll;
        }
    </style>
@endsection

@push('scripts')
<script>
    $(document).ready(function(){     
        // Get Toko
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{ url('/api/admin-toko/getToko') }}",
            success: function(data) {
                if (data.status == 200) {
                    $('#nama_toko').html(data.data.NAMA);
                    $('#img_logo').html(`
                        <img src="`+data.data.IMAGE+`" alt="Gambar Toko">
                    `)
                    $('#fullname_akun').html(data.data.AkunTokoFullname)
                    $('#email_akun').html(data.data.AkunTokoEmail)
                    $('#bank_akun').html(data.data.AkunTokoBank)
                    if (data.data.STATUS == 1) {
                        $('#icon_status').html(`
                            <i class="flaticon2-correct text-success font-size-h5" title="Akun Sudah Aktif"></i>
                        `);
                    } else if (data.data.STATUS == 0) {
                        $('#icon_status').html(`
                            <i class="flaticon-circle text-danger font-size-h5" title="Akun Belum Aktif"></i>
                        `);
                    }

                    $('#alamat_toko').html(data.data.ADDRESS);

                    if (data.data.COD == 1) {
                        $('#status_cod').html(`
                            <i class="flaticon2-box text-success mr-2 font-size-lg"></i> COD
                        `)
                    } else {
                        $('#status_cod').html(`
                            <i class="flaticon2-cancel text-danger mr-2 font-size-lg"></i> COD
                        `)
                    }
                }
            }
        });

        // Get Total Product
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{ url('/api/admin-toko/product-total') }}",
            success: function(data) {
                if (data.status == 200) {
                    $('#total_product').html(data.data.total)
                } else {
                    $('#total_product').html(0)
                }
            }
        });
    });  
</script>   
@endpush

@extends('layouts.CMS.app')

@section('title', 'Data Category')

@section('category-all', 'menu-item-open menu-item-here')

@section('category-all-submenu', 'menu-item-active')

@section('css')
<style>
    .auto_scroll{
        display:block;
        /* border: 1px solid red; */
        /* padding:5px; */
        /* margin-top:5px; */
        width:100%;
        height:250px;
        overflow:auto;
    }
</style>
@endsection

@section('content')
{{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Category</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Form {{$mode}}</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
    <!-- container -->
        <div class="container">
            <div class="card card-custom gutter-b example example-compact">
                <form id="fm_category" method="post" enctype="multipart/form-data">
                    <div class="card-body row">
                        <div class="col-lg-12 col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1">TIPE 
                                <span class="text-danger">*</span></label>
                                <select class="form-control" name="tipe" id="tipe">
                                    <option value="product">Product</option>
                                    <option value="news_tips">News & Tips</option>
                                </select>
                                {{-- <input type="text" name="tipe" required="true" class="form-control" id="tipe" /> --}}
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-4">
                            <div class="form-group">
                                <label for="exampleSelect1">PARENT ID 
                                <span class="text-danger">*</span></label><br>
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" data-toggle="collapse" href="#collapseOne" style="background-color:rgba(0,0,0,.03); padding: 1rem 1rem;">
                                            <h6>Category</h6>
                                        </div>
                                        <div id="collapseOne" class="collapse p-0" data-parent="#accordion">
                                            <div class="card-body p-0 m-0">
                                                <div class="form-group row">
                                                    <div class="col-12 col-form-label">
                                                        <div id="cat_list" class="radio-list auto_scroll">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-4">
                            <div class="form-group">
                                <label for="exampleInputPassword1">TITLE 
                                <span class="text-danger">*</span></label>
                                <input type="text" name="title" required="true" class="form-control" id="title" />
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">SEO 
                                <span class="text-danger">*</span></label>
                                <input type="text" name="seo" required="true" class="form-control" id="seo" />
                            </div>
                            <div class="form-group">
                                <label for="exampleSelect1">STATUS 
                                <span class="text-danger">*</span></label>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">Publish</option>
                                    <option value="0">Unpublish</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        <button type="reset" onclick="cancel()" class="btn btn-secondary">Cancel</button>
                    </div>
                </form>
            </div>
        </div>

    </div>
@endsection

@push('scripts')
<script>
var checked = 0;
$(function(){

    // load_category();

    if("{{$mode}}"=="Ubah"){
        $.ajax({
            type:'GET',
            // dataType:"JSON",
            url:"{{ url('4dm1n-api/edit-category',$id) }}",
            success:function(r){
                load_category(r.data.TIPE);
                setTimeout(function(){ 
                    jQuery("input[value="+r.data.PARENT_ID+"]").attr('checked', true);
                    $("#title").val(r.data.TITLE);
                    $("#seo").val(r.data.SEO);
                    $("#tipe").val(r.data.TIPE);
                    $("#status").val(r.data.STATUS);
                    checked = r.data.PARENT_ID;    
                }, 500);
            }
        });
    }else{
        load_category();
    }

    $("#tipe").change(function(){
        tipe = $("#tipe").val();
        load_category(tipe);
    });

});


function load_category(tipe="product"){
    $.ajax({
        type:'GET',
        // dataType:"JSON",
        url:"{{ route('admin.api-category-data') }}"+"?param="+tipe,
        success:function(r){
            if(checked>0){
                setTimeout(function(){ 
                    jQuery("input[value="+checked+"]").attr('checked', true);
                }, 500);
            }
            document.getElementById("cat_list").innerHTML = r.data;
        }
    });
}

//ajax simpan data
if("{{$mode}}"=="Ubah"){

    $('#fm_category').on('submit',function(e) {
        e.preventDefault();

        $.ajax({
        url: "{{ url('4dm1n-api/update-category',$id) }}",
        data: new FormData(this),
        type: 'POST',
        processData: false,
        contentType: false,
        dataType : 'JSON',
        beforeSend: function() {

        },
        onSubmit:function(){

        },
        success: function(res) {
            if(res.status){
                alert(JSON.stringify(res));
                window.location = "{{ route('admin.view-category-all') }}";
            }
        },
        error: function(){
            alert("error");
        },

        });
    });

}else{
    $('#fm_category').on('submit',function(e) {
        e.preventDefault();

        $.ajax({
        url: "{{ url('4dm1n-api/save-category') }}",
        data: new FormData(this),
        type: 'POST',
        processData: false,
        contentType: false,
        dataType : 'JSON',
        beforeSend: function() {

        },
        onSubmit:function(){

        },
        success: function(res) {
            if(res.status){
                alert(JSON.stringify(res));
                window.location = "{{ route('admin.view-category-all') }}";
            }
        },
        error: function(){
            alert("error");
        },

        });
    });
}

function cancel(){
    window.location = "{{ route('admin.view-category-all') }}";
}

</script>
@endpush
@extends('layouts.CMS.app')

@section('title', 'Data Category')

@section('category-all', 'menu-item-open menu-item-here')

@section('category-all-submenu', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Category</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Data Tabel</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            {{-- Card Tabel Category --}}
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="flaticon2-supermarket text-primary"></i>
                        </span>
                        <h3 class="card-label">Tabel Data Category</h3>
                    </div>
                    <div class="card-toolbar">
                        <a href="{{route('admin.view-category-form-add')}}" class="btn btn-primary font-weight-bolder">
                        <span class="svg-icon svg-icon-md">
                            <!--begin::Svg Icon | path:assets/media/svg/icons/Design/Flatten.svg-->
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24" />
                                    <circle fill="#000000" cx="9" cy="15" r="6" />
                                    <path d="M8.8012943,7.00241953 C9.83837775,5.20768121 11.7781543,4 14,4 C17.3137085,4 20,6.6862915 20,10 C20,12.2218457 18.7923188,14.1616223 16.9975805,15.1987057 C16.9991904,15.1326658 17,15.0664274 17,15 C17,10.581722 13.418278,7 9,7 C8.93357256,7 8.86733422,7.00080962 8.8012943,7.00241953 Z" fill="#000000" opacity="0.3" />
                                </g>
                            </svg>
                            <!--end::Svg Icon-->
                        </span>Tambah</a>
                        <!--end::Button-->
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-checkable" id="tb_category" style="margin-top: 13px !important">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    {{-- <th>Parent ID</th>   --}}
                                    <th>Nama Category</th>
                                    <th>SEO</th>
                                    <th>Tipe</th>
                                    <th>Publish</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ url('assets/CMS/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@push('scripts')
<script src="{{ url('assets/CMS/plugins/custom/datatables/datatables.bundle.js') }}"></script>
{{-- JS custom --}}
<script>

$('#tb_category').DataTable( {
    "ajax": "{{ route('admin.api-category-all') }}",
    "processing": true,
    "rowReorder": {
        "selector": 'td:nth-child(2)'
    },
    "responsive": true,
    "columns": [
        { data: "ID" ,className: 'text-center',
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }    
        },
        // { data: "PARENT_ID",className: 'text-center' },
        { data: "TITLE" },
        { data: "SEO" },
        { data: "TIPE" },
        { data: "ID",
            render: function (data, type, JsonResultRow, meta) {
                let encrypt = "{{ bin2hex(random_bytes(10)) }}"+'-'+JsonResultRow.ID.toString()+"{{  substr(str_shuffle('AbCdEfG'), 0) }}";
                let row = meta.row + meta.settings._iDisplayStart + 1;

                if (JsonResultRow.STATUS == 0) {
                    $('#btn_unpublish').addClass('disabled').addClass('btn-outline-success');
                } else if (JsonResultRow.STATUS == 1) {
                    $('#btn_publish').addClass('disabled').addClass('btn-outline-success');
                }
                // return `<div class="text-center"><a class="btn btn-xs btn-sm btn-success" id="btn_publish">Publish</a>
                // <a class="btn btn-xs btn-sm btn-success" id="btn_unpublish">Unpublish</a></div>`;
                return `<div class="text-center"><span class="switch switch-outline switch-icon switch-warning d-inline-block"><label><input id="publish`+row+`" type="checkbox" `+JsonResultRow.CHECKED.toString()+` name="select" onclick="publish('`+row+`','`+encrypt+`')"/> <span></span> </label></span></div>`;
            }
        },
        { data: "ID",
            render: function (data, type, JsonResultRow, meta) {
                let encrypt = "{{ bin2hex(random_bytes(10)) }}"+'-'+JsonResultRow.ID.toString()+"{{  substr(str_shuffle('AbCdEfG'), 0) }}";
                return `<div class="text-center"><a class="badge badge-success" href="{{ url('4dm1n/category-form-edit/`+encrypt+`') }}">Edit</a>
                <a class="badge badge-danger" onclick="delete_category('`+encrypt+`')">Hapus</a></div>`;
            }
        }
    ]
});

function publish(row,publish){
    if ($("#publish"+row).is(":checked")){
        var status = true;
    }else{
        var status = false;
    }
    $.ajax({
        url: "{{ url('4dm1n-api/publish-category')}}",
        type: 'POST',
        dataType:"JSON",
        data:{ 
            category:publish,
            status:status
         },
        beforeSend: function() {

        },
        success: function(res) {
            if(res){
                alert(JSON.stringify(res));
                // window.location = "{{ route('admin.view-category-all') }}";
            }
        },
        error: function(){
            alert("error");
        },

    });
}

function delete_category(id=null){
    var r = confirm("Yakin ingin dihapus !");
    if (r == true) {
        $.ajax({
            url: "{{ url('4dm1n-api/delete-category')}}"+"/"+id,
            type: 'POST',
            dataType:"JSON",
            beforeSend: function() {

            },
            onSubmit:function(){

            },
            success: function(res) {
                if(res.status){
                    alert(JSON.stringify(res));
                    window.location = "{{ route('admin.view-category-all') }}";
                }
            },
            error: function(){
                alert("error");
            },

        });
    }

}

</script>
@endpush
@extends('layouts.CMS.app')

@section('title', 'Data Category')

@section('category-all', 'menu-item-open menu-item-here')

@section('category-all-submenu-history', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Category</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Data Tabel</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            {{-- Card Tabel Category --}}
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="flaticon2-supermarket text-primary"></i>
                        </span>
                        <h3 class="card-label">Tabel Data Category (History)</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-checkable" id="tb_category" style="margin-top: 13px !important">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    {{-- <th>Parent ID</th>   --}}
                                    <th>Judul</th>
                                    <th>SEO</th>
                                    <th>Tipe</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ url('assets/CMS/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@push('scripts')
<script src="{{ url('assets/CMS/plugins/custom/datatables/datatables.bundle.js') }}"></script>
{{-- JS custom --}}
<script>

$('#tb_category').DataTable( {
    "ajax": "{{ route('admin.api-category-history') }}",
    "processing": true,
    "rowReorder": {
        "selector": 'td:nth-child(2)'
    },
    "responsive": true,
    "columns": [
        { "data": "ID",className: 'text-center',
            render: function (data, type, row, meta) {
                return meta.row + meta.settings._iDisplayStart + 1;
            }    
        },
        // { "data": "PARENT_ID" },
        { "data": "TITLE" },
        { "data": "SEO" },
        { "data": "TIPE" },
        { "data": "STATUS",className: 'text-center',
            render: function (data, type, row, meta) {
                return "Delete";
            }  
        },
    ]
});

function delete_category(id=null){
    $.ajax({
        url: "{{ url('4dm1n-api/delete-category')}}"+"/"+id,
        type: 'POST',
        dataType:"JSON",
        beforeSend: function() {

        },
        onSubmit:function(){

        },
        success: function(res) {
            if(res.status){
                alert(JSON.stringify(res));
                window.location = "{{ route('admin.view-category-all') }}";
            }
        },
        error: function(){
            alert("error");
        },

    });
}

</script>
@endpush
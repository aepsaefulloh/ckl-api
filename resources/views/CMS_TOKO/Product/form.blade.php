@extends('layouts.CMS_TOKO.app')

@if ($mode == "Ubah")
    @section('title', 'Update Product')
@else 
    @section('title', 'Tambah Product')
@endif

@section('product-all', 'menu-item-open menu-item-here')

@section('product-all-submenu', 'menu-item-active')

@section('css')
<style>
    .auto_scroll{
        display:block;
        width:100%;
        height:250px;
        overflow:auto;
    }

    .image-preview {
        height: 300px;
        position: relative;
        overflow: hidden;
        background-color:#e9eadf;
        color: #ecf0f1;
    }
    .image-upload {
        line-height: 200px;
        font-size: 200px;
        position: absolute;
        opacity: 0;
        z-index: 10;
    }
    .image-label {
        position: absolute;
        z-index: 5;
        opacity: 0.8;
        cursor: pointer;
        background-color: #F2F5F8;
        width: 150px;
        height: 40px;
        padding: 5px;
        font-size: 17px;
        text-transform: uppercase;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        text-align: center;
    }
</style>
@endsection

@section('content')
{{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Product</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Form {{$mode}}</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
    <!-- container -->
        <div class="container">
            <div class="card card-custom gutter-b example example-compact">
                <form id="fm_product" method="post" enctype="multipart/form-data">
                    <div class="card-body row">
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="exampleSelect1">PARENT ID 
                                <span class="text-danger">*</span></label><br>
                                <div id="accordion">
                                    <div class="card">
                                        <div class="card-header" data-toggle="collapse" href="#collapseOne" style="background-color:rgba(0,0,0,.03); padding: 1rem 1rem;">
                                            <span>Category</span>
                                        </div>
                                        <div id="collapseOne" class="collapse p-0" data-parent="#accordion">
                                            <div class="card-body p-0 m-0">
                                                <div class="form-group row">
                                                    <div class="col-12 col-form-label">
                                                        <div id="cat_list" class="radio-list auto_scroll">
                                                        
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12">
                            <div class="form-group">
                                <label for="uniq_code">CODE 
                                <span class="text-danger">*</span></label>
                                <div id="code">
                                </div>
                                <input type="text" name="uniq_code" required="true" class="form-control" readonly value="TM{{ mt_rand(100000, 999999) }}" id="uniq_code" />
                            </div>
                            <div class="form-group">
                                <label for="name_product">NAME PRODUCT 
                                <span class="text-danger">*</span></label>
                                <input type="text" name="name_product" required="true" class="form-control" id="name_product" />
                            </div>
                            <div class="form-group">
                                <label for="description_product">DESCRIPTION 
                                <span class="text-danger">*</span></label>
                                <textarea name="description_product" required="true" class="form-control" id="description_product" rows="15"></textarea>
                            </div>
                            <div class="row">
                                <div class="form-group col-4">
                                    <label>Foto <span class="text-danger">*</span></label>
                                    
                                    <div class="image_loading">
                                        <div class="text-center">
                                            <div class="spinner-border" role="status">
                                              <span class="sr-only">Loading...</span>
                                            </div>
                                          </div>
                                    </div>
                                    <div id="box-image_1" class="row"></div>

                                    <div id="image-preview_1" hidden=true class="image-preview">
                                        <label for="image-upload" id="image-label_1" class="image-label">Choose File</label>
                                        <input type="file" name="image_name[]" id="image-upload_1" class="image-upload" accept=".png, .jpg, .jpeg" />
                                    </div>
                                </div>
                                <div class="form-group col-4">
                                    <label>Foto </label>

                                    <div class="image_loading">
                                        <div class="text-center">
                                            <div class="spinner-border" role="status">
                                              <span class="sr-only">Loading...</span>
                                            </div>
                                          </div>
                                    </div>
                                    <div id="box-image_2" class="row"></div>
                                    <div id="image-preview_2" hidden=true class="image-preview">
                                        <label for="image-upload" id="image-label_2" class="image-label">Choose File</label>
                                        <input type="file" name="image_name[]" id="image-upload_2" class="image-upload" accept=".png, .jpg, .jpeg" />
                                    </div>
                                </div><div class="form-group col-4">
                                    <label>Foto</label>

                                    <div class="image_loading">
                                        <div class="text-center">
                                            <div class="spinner-border" role="status">
                                              <span class="sr-only">Loading...</span>
                                            </div>
                                          </div>
                                    </div>
                                    <div id="box-image_3" class="row"></div>
                                    <div id="image-preview_3" hidden=true class="image-preview">
                                        <label for="image-upload" id="image-label_2" class="image-label">Choose File</label>
                                        <input type="file" name="image_name[]" id="image-upload_3" class="image-upload" accept=".png, .jpg, .jpeg" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price">PRICE 
                                <span class="text-danger">*</span></label>
                                <input type="text" name="price" required="true" class="form-control" id="price" />
                            </div>
                            <div class="form-group">
                                <label for="available_color">AVAILABLE_COLOR 
                                <span class="text-danger">*</span></label>
                                <input type="color" name="available_color" required="true" class="form-control" id="available_color" />
                            </div>
                            <div class="form-group">
                                <label for="exampleSelect1">STATUS 
                                <span class="text-danger">*</span></label>
                                <select class="form-control" name="status" id="status">
                                    <option value="1">Aktif</option>
                                    <option value="0">Tidak Aktif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                        <button type="reset" onclick="cancel()" class="btn btn-secondary">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="https://booking.phm-hotels.com/public/libs/jquery-upload-preview/assets/js/jquery.uploadPreview.min.js"></script>

<script>
    // Image Preview
    $.uploadPreview({
        input_field: "#image-upload_1",   // Default: .image-upload
        preview_box: "#image-preview_1",  // Default: .image-preview
        label_field: "#image-label_1",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false                 // Default: false
    });

    $.uploadPreview({
        input_field: "#image-upload_2",   // Default: .image-upload
        preview_box: "#image-preview_2",  // Default: .image-preview
        label_field: "#image-label_2",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false                 // Default: false
    });

    $.uploadPreview({
        input_field: "#image-upload_3",   // Default: .image-upload
        preview_box: "#image-preview_3",  // Default: .image-preview
        label_field: "#image-label_3",    // Default: .image-label
        label_default: "Choose File",   // Default: Choose File
        label_selected: "Change File",  // Default: Change File
        no_label: false                 // Default: false
    });

    // Format rupiah in form
    var rupiah = document.getElementById('price');
    rupiah.addEventListener('keyup', function(e){
        rupiah.value = formatRupiah(this.value, 'Rp. ');
    });

    function formatRupiah(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);

        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    
    $(function(){
        // Get Data Product Category
        $.ajax({
            type:'GET',
            url:"{{ route('admin.api-category-data') }}"+"?param=product",
            success:function(r){
                document.getElementById("cat_list").innerHTML = r.data;
            }
        });

        // Kondisi Jika Form Untuk Update Data
        if("{{ $mode }}" == "Ubah"){
            $("#image-upload_1").attr("required",false);

            // Get Data Product 
            $.ajax({
                type:'GET',
                url:"{{ url('4dm1n-api/edit-product',$uniq_code) }}",
                success:function(r){
                    jQuery("input[value="+r.data.CATEGORY+"]").attr('checked', true);
                    $("#uniq_code").attr("hidden",true);
                    $("#code").html(`<h4>`+r.data.UNIQ_CODE+` </h4>`);
                    $("#uniq_code").val(r.data.UNIQ_CODE);
                    $("#name_product").val(r.data.NAME_PRODUCT);
                    $("#description_product").val(r.data.DESCRIPTION_PRODUCT);
                    $("#price").val('Rp. '+r.data.PRICE.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1."));
                    $("#available_color").val(r.data.AVAILABLE_COLOR);
                    $("#status").val(r.data.STATUS);

                    $('.image_loading').hide();
                    if(r.data.IMAGE!=""){
                        $('#box-image_1').show().append(`
                            <div class="col-lg-12 mb-1">
                                <img class="img-fluid" width=100% src="{{url('`+r.thumb+r.data.CATEGORY+`/`+r.data.IMAGE+`')}}">
                            </div>
                            <div class="col-lg-6 pr-1">
                                <button type="button" class="btn btn-sm btn-block btn-outline-primary" id="edit_image_1">Perbaharui</button>
                            </div>
                            <div class="col-lg-6 pl-1">
                                <button type="button" class="btn btn-sm btn-block btn-outline-danger" id="delete_image_1">Hapus</button>
                            </div>`
                        );

                        $('#edit_image_1').click(function() {
                            $('#box-image_1').hide();
                            $('#image-preview_1').attr("hidden",false);
                        });
                    } else {
                        $('#box-image_1').hide();
                        $('#image-preview_1').attr("hidden",false);
                    }

                    if (typeof r.data.IMAGE_ADDITIONAL_0 !== 'undefined') {
                        $('#box-image_2').show().append(`
                            <div class="col-lg-12 mb-1">
                                <img class="img-fluid" width=100% src="{{url('`+r.thumb+r.data.CATEGORY+`/`+r.data.IMAGE_ADDITIONAL_0+`')}}">
                            </div>
                            <div class="col-lg-6 pr-1">
                                <button type="button" class="btn btn-sm btn-block btn-outline-primary" id="edit_image_2">Perbaharui</button>
                            </div>
                            <div class="col-lg-6 pl-1">
                                <a href="javascript:void(0)" class="btn btn-sm btn-block btn-outline-danger" id="delete_image_2" onclick="delete_image(2,`+r.data.ID_IMAGE_ADDITIONAL_0+`)">Hapus</a>
                            </div>`
                        );                    
                    
                        $('#edit_image_2').click(function() {
                            $('#box-image_2').hide();
                            $('#image-preview_2').attr("hidden",false);
                        });
                    } else {
                        $('#box-image_2').hide();
                        $('#image-preview_2').attr("hidden",false);
                    }

                    if (typeof r.data.IMAGE_ADDITIONAL_1 !== 'undefined') {
                        $('#box-image_3').show().append(`
                            <div class="col-lg-12 mb-1">
                                <img class="img-fluid" width=100% src="{{url('`+r.thumb+r.data.CATEGORY+`/`+r.data.IMAGE_ADDITIONAL_1+`')}}">
                            </div>
                            <div class="col-lg-6 pr-1">
                                <button type="button" class="btn btn-sm btn-block btn-outline-primary" id="edit_image_3">Perbaharui</button>
                            </div>
                            <div class="col-lg-6 pl-1">
                                <a href="javascript:void(0)" class="btn btn-sm btn-block btn-outline-danger" id="delete_image_3" onclick="delete_image(3,`+r.data.ID_IMAGE_ADDITIONAL_1+`)">Hapus</a>
                            </div>`
                        );

                        $('#edit_image_3').click(function() {
                            $('#box-image_3').hide();
                            $('#image-preview_3').attr("hidden",false);
                        })          
                    } else {
                        $('#box-image_3').hide();
                        $('#image-preview_3').attr("hidden",false);
                    }
                }
            });
        } else {
            $('#box-image_1').hide();
            $('#image-preview_1').attr("hidden",false);
            $('#box-image_2').hide();
            $('#image-preview_2').attr("hidden",false);
            $('#box-image_3').hide();
            $('#image-preview_3').attr("hidden",false);

            $('.image_loading').hide();
            $("#image-upload_1").attr("required",true);
        }
    });


    // Kondisi Untuk Form Submit Update Data
    if("{{ $mode }}" == "Ubah"){
        $('#fm_product').on('submit',function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{ url('api/admin-toko/update-product',$uniq_code) }}",
                data: new FormData(this),
                type: 'POST',
                processData: false,
                contentType: false,
                dataType : 'JSON',
                beforeSend: function() {

                },
                onSubmit:function(){
                },
                success: function(res) {
                    if(res){
                        alert(JSON.stringify(res.message));
                        window.location = "{{ route('admin_toko.view-product-all') }}";
                    }
                },
                error: function(res){
                    alert("error");
                },
            });
            
        });

    } else {
        $('#fm_product').on('submit',function(e) {
            e.preventDefault();
            $.ajax({
                url: "{{ url('api/admin-toko/save-product') }}",
                data: new FormData(this),
                type: 'POST',
                processData: false,
                contentType: false,
                dataType : 'JSON',
                beforeSend: function() {

                },
                onSubmit:function(){

                },
                success: function(res) {
                    if(res){
                        alert(JSON.stringify(res.message));
                        window.location = "{{ route('admin_toko.view-product-all') }}";
                    }
                },
                error: function(){
                    alert("error");
                },
            });
        });
    }

    // Delete Image
    function delete_image(image=null,id=null){
        $('#box-image_'+image).hide();
        $('#image-preview_'+image).attr("hidden",false);

        $.ajax({
            url: "{{ url('api/admin-toko/delete-product_image')}}"+"/"+id,
            type: 'POST',
            dataType:"JSON",
            beforeSend: function() {

            },
            onSubmit:function(){

            },
            success: function(res) {
                if(res){
                    alert("Gambar berhasil dihapus!");
                }
            },
            error: function(){
                alert("error");
            },
        });
    }

    function cancel(){
        window.location = "{{ route('admin_toko.view-product-all') }}";
    }
</script>
@endpush
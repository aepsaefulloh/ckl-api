@extends('layouts.CMS_TOKO.app')

@section('title', 'Data Product ( History )')

@section('product-all', 'menu-item-open menu-item-here')

@section('product-all-submenu-history', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Product</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Data Tabel</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            {{-- Card Tabel Category --}}
            <div class="card card-custom">
                <div class="card-header">
                    <div class="card-title">
                        <span class="card-icon">
                            <i class="flaticon2-box-1 text-primary"></i>
                        </span>
                        <h3 class="card-label">Tabel Data Product (History)</h3>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-checkable" id="tb_product" style="margin-top: 13px !important">
                            <thead>
                                <tr class="text-center">
                                    <th>No</th>
                                    <th>Code</th>  
                                    <th>Nama Product</th>
                                    <th>Description</th>
                                    <th>Category</th>
                                    <th>Image</th>
                                    <th>Price</th>
                                    <th>Color</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('css')
<link href="{{ url('assets/CMS/plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css" />
@endsection

@push('scripts')
<script src="{{ url('assets/CMS/plugins/custom/datatables/datatables.bundle.js') }}"></script>
{{-- JS custom --}}
<script>

    $('#tb_product').DataTable( {
        "ajax": "{{ route('admin_toko.api-product-history') }}",
        "processing": true,
        "rowReorder": {
            "selector": 'td:nth-child(2)'
        },
        "responsive": true,
        "columns": [
            { "data": null ,
                render: function (data, type, row, meta) {
                    return meta.row + meta.settings._iDisplayStart + 1;
                }    
            },
            { "data": "UNIQ_CODE" },
            { "data": "NAME_PRODUCT" },
            { "data": "DESCRIPTION_PRODUCT",
               render: function(data) {
                    return data.substring(0, 100);
               }
            },
            { "data": "CATEGORY_NAME" },
            { "data": null, className: 'text-center',
                render: function (data, type, JsonResultRow, meta) {
                    return `<a href="`+data.IMAGE_ORIGIN+`" target="_blank">
                    <img width="60" src="`+data.IMAGE_THUMB+`"/>
                    </a>`;
                }
            },
            { "data": "PRICE",className: 'text-right',},
            { "data": "AVAILABLE_COLOR",
                render: function (data, type, row, meta) {
                    return `<div class="text-center" style="border: 1px solid black;background-color:`+data+`;color:white">`+data+`</div>`;
                }  
            },
            { "data": "UNIQ_CODE",className: 'text-center',
                render: function (data, type, row, meta) {
                    return `delete`;
                }  
            }
        ]
    });

    function delete_category(id=null){
        $.ajax({
            url: "{{ url('4dm1n-api/delete-category')}}"+"/"+id,
            type: 'POST',
            dataType:"JSON",
            beforeSend: function() {

            },
            onSubmit:function(){

            },
            success: function(res) {
                if(res.status){
                    alert(JSON.stringify(res));
                    window.location = "{{ route('admin.view-category-all') }}";
                }
            },
            error: function(){
                alert("error");
            },

        });
    }
</script>
@endpush
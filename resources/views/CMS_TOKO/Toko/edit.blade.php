@extends('layouts.CMS_TOKO.app')

@section('title', 'Edit Toko')

@section('dashboard', 'menu-item-active')

@section('content')
    {{-- Sub Header Content --}}
    <div class="subheader py-2 py-lg-4 subheader-solid" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-2">
                <h5 class="text-dark font-weight-bold mt-2 mb-2 mr-5">Dashboard</h5>
                <div class="subheader-separator subheader-separator-ver mt-2 mb-2 mr-4 bg-gray-200"></div>
                <span class="text-muted font-weight-bold mr-4">Update Toko</span>
            </div>
            
            <div class="d-flex align-items-center">
                <a href="#" class="btn btn-clean btn-sm font-weight-bold font-size-base mr-1">
                    {{ \Carbon\Carbon::now()->format('d m Y') }} <span id="jam"></span> : <span id="menit"></span> : <span id="detik"></span>
                </a>
            </div>
        </div>
    </div>

    <div class="d-flex flex-column-fluid">
        {{-- Content --}}
        <div class="container">
            <div id="status_loading">
                <div class="d-flex justify-content-center">
                    <div class="spinner-border" role="status">
                      <span class="sr-only">Loading...</span>
                    </div>
                </div>
            </div>
            <form class="form" action="{{ url('api/admin-toko/toko/store', $id) }}" method="POST" enctype="multipart/form-data" id="form" style="display: none;">
                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Data Akun</h3>
                    </div>
                    <!--begin::Form-->
                    <div class="card-body">
                        <div class="form-group">
                            <label>Nama Lengkap*</label>
                            <input type="text" name="akun_fullname" class="form-control form-control-solid" id="akun_fullname">
                        </div>
                        <div class="form-group">
                            <label>Email*</label>
                            <input type="email" name="akun_email" class="form-control form-control-solid" id="akun_email">
                        </div>
                        <div class="form-group">
                            <label>NIK*</label>
                            <input type="text" name="akun_nik" class="form-control form-control-solid" id="akun_nik">
                        </div>
                        <div class="form-group">
                            <label>Bank*</label>
                            <select name="akun_bank" class="form-control form-control-solid" id="akun_bank"></select>
                        </div>
                        <div class="form-group">
                            <label>No. Rekening*</label>
                            <input type="text" name="akun_no_rek" class="form-control form-control-solid" id="akun_no_rek">
                        </div>
                        <div class="form-group">
                            <label>Username*</label>
                            <input type="text" name="akun_user" class="form-control form-control-solid" id="akun_user">
                        </div>
                        <div class="form-group">
                            <label>Password <small class="font-italic">(Isi Jika Anda Ingin Update Password)</small></label>
                            <input type="password" name="password" placeholder="Masukan Password Baru" class="form-control form-control-solid">
                        </div>
                    </div>
                </div>

                <div class="card card-custom gutter-b example example-compact">
                    <div class="card-header">
                        <h3 class="card-title">Data Toko</h3>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <label>Foto</label>
                            <div id="image_loading">
                                <div class="text-center">
                                    <div class="spinner-border" role="status">
                                      <span class="sr-only">Loading...</span>
                                    </div>
                                  </div>
                            </div>
                            <div id="box-image"></div>
                            <div id="image-preview">
                                <label for="image-upload" id="image-label">Choose File</label>
                                <input type="file" name="image" id="image-upload" accept=".png, .jpg, .jpeg" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Nama Toko*</label>
                            <input type="text" name="toko_nama" class="form-control form-control-solid" id="nama_toko">
                        </div>
                        <div class="form-group">
                            <label>No. Telepon*</label>
                            <input type="text" name="toko_phone" class="form-control form-control-solid" id="phone_toko">
                        </div>
                        <div class="form-group">
                            <label>COD*</label>
                            <select name="toko_cod" class="form-control form-control-solid" id="cod_toko"></select>
                        </div>
                        <div class="form-group">
                            <label>Provinsi*</label>
                            <select name="prov_toko" class="form-control form-control-solid" id="prov_toko"></select>
                        </div>

                        <div class="form-group">
                            <label>Kabupaten*</label>
                            <select name="kab_toko" class="form-control form-control-solid" id="kab_toko">
                                <option value="">-- Loading --</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Alamat*</label>
                            <textarea name="toko_alamat" class="form-control form-control-solid" id="alamat_toko"></textarea>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <div class="text-right">
                        <a class="btn btn-secondary mr-2" href="{{ url('admin-toko/home') }}">Kembali</a>
                        <button type="submit" class="btn btn-primary" onclick="alert('Yakin Data ingin diupdate?')">Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('css')
<style>
    #image-preview {
        width: 100%;
        height: 500px;
        position: relative;
        overflow: hidden;
        background-color: #ffffff;
        color: #ecf0f1;
        display: none;
    }
    #image-upload {
        line-height: 200px;
        font-size: 200px;
        position: absolute;
        opacity: 0;
        z-index: 10;
    }
    #image-label {
        position: absolute;
        z-index: 5;
        opacity: 0.8;
        cursor: pointer;
        background-color: #F2F5F8;
        width: 150px;
        height: 40px;
        padding: 5px;
        font-size: 17px;
        text-transform: uppercase;
        top: 0;
        left: 0;
        right: 0;
        bottom: 0;
        margin: auto;
        text-align: center;
    }
</style>
@endsection

@push('scripts')
{{-- Jquery Upload Preview --}}
<script src="https://booking.phm-hotels.com/public/libs/jquery-upload-preview/assets/js/jquery.uploadPreview.min.js"></script>

<script>
    $(document).ready(function(){
        $.ajax({
            type: "GET",
            dataType: "json",
            url: "{{ url('api/admin-toko/getToko', $id) }}",
            success: function(data) {
                let data_bank = ["BCA", "BNI", "BRI", "Mandiri", "BJB"];

                $('#status_loading').hide();
                $('#form').show();

                $('#akun_fullname').val(data.data.akun.FULLNAME);
                $('#akun_email').val(data.data.akun.EMAIL);
                $('#akun_nik').val(data.data.akun.NIK);
                
                data_bank.forEach(x => {
                    if (x == data.data.akun.BANK) {
                        $('#akun_bank').append(`
                            <option value="`+data.data.akun.BANK+`" selected>`+data.data.akun.BANK+`</option>
                        `)    
                    } else {
                        $('#akun_bank').append(`
                        <option value="`+x+`">`+x+`</option>
                        `)
                    }
                });

                $('#akun_no_rek').val(data.data.akun.NO_REK);
                $('#akun_user').val(data.data.akun.USERNAME);


                $('#nama_toko').val(data.data.toko.NAMA);
                $('#alamat_toko').val(data.data.toko.ADDRESS);
                $('#phone_toko').val(data.data.toko.TELEPHONE);

                if (data.data.toko.COD == 1) {
                    $('#cod_toko').html(`
                        <option value="1" selected>COD</option>
                        <option value="0">Tidak</option>
                    `)
                } else if (data.data.toko.COD == 0) {
                    $('#cod_toko').html(`
                        <option value="1">COD</option>
                        <option value="0" selected>Tidak</option>
                    `)
                }

                $('#box-image').show().append(`
                    <img class="img-fluid w-100" src="`+data.data.toko.IMAGE+`">
                    <button type="button" class="btn btn-sm btn-block btn-primary" id="edit_image">Perbaharui Foto</button>    
                `)   
                $('#image-preview').hide();
                $('#image_loading').hide();

                $('#edit_image').click(function() {
                    $('#box-image').hide();
                    $('#image-preview').show();
                })


                // Looping Provinsi
                data.data.province.forEach(x => {
                    if (x.province_id == data.data.toko.PROV_ID) {
                        $('#prov_toko').append(`
                            <option value="`+x.province_id+`" selected>`+x.province+`</option>
                        `);
                    } else {    
                        $('#prov_toko').append(`
                            <option value="`+x.province_id+`">`+x.province+`</option>
                        `);   
                    }
                });

                var provID = data.data.toko.PROV_ID;
                var kotaID = data.data.toko.KOTA_ID;
                $('#prov_toko').change(function() {
                    provID = this.value;
                    $('#kab_toko').html(`
                        <option>-- Loading --</option>
                    `)

                    // Get City by Province
                    $.ajax({
                        type: "GET",
                        dataType: "json",
                        url: "{{ url('api/admin-toko/getToko/get-city') }}"+'/'+provID,
                        success: function(data) {
                            $('#kab_toko').empty();
                            data.data.forEach(x => {
                                $('#kab_toko').append(`
                                    <option value="`+x.city_id+`">`+x.type+` `+x.city_name+`</option>  
                                `);
                            });
                        }
                    })
                });   

                // Get City by Province (default)
                $.ajax({
                    type: "GET",
                    dataType: "json",
                    url: "{{ url('api/admin-toko/getToko/get-city') }}"+'/'+provID,
                    success: function(data) {
                        $('#kab_toko').empty();
                        data.data.forEach(x => {
                            if (x.city_id == kotaID) {
                                $('#kab_toko').append(`
                                    <option value="`+x.city_id+`" selected>`+x.type+` `+x.city_name+`</option>  
                                `);   
                            } else {
                                $('#kab_toko').append(`
                                    <option value="`+x.city_id+`">`+x.type+` `+x.city_name+`</option>  
                                `);
                            }
                        });
                    }
                })  

            }
        });
    });    
    $.uploadPreview({
        input_field: "#image-upload",   
        preview_box: "#image-preview",  
        label_field: "#image-label",    
        label_default: "Choose File",   
        label_selected: "Change File",  
        no_label: false                 
    });        
</script>
@endpush
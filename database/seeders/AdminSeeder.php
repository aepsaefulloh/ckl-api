<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use DB;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin')->delete();
        DB::table('admin')->insert([
            'EMAIL'     => 'dev@superadmin.com',
            'USER'      => 'Super Admin',
            'PASSWORD'  => Hash::make('admin@tm'),
            'ROLE'      => 'superadmin',
            'FULLNAME'  => 'Super Admin',
            'LAST_LOGIN'=> null,
            'TOKEN_KEY' => null,
            'STATUS'    => 1    
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'ID'         => 1,
                'PARENT_ID'  => 1,
                'TITLE'      => 'Studio & Recording',
                'SEO'        => 'studio-recording',
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'         => 2,
                'PARENT_ID'  => 2,
                'TITLE'      => 'Live Sound & Lighting',
                'SEO'        => 'live-sound-lighting',
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'         => 3,
                'PARENT_ID'  => 3,
                'TITLE'      => 'Software & Plugins',
                'SEO'        => 'software-plugins',
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'         => 4,
                'PARENT_ID'  => 4,
                'TITLE'      => 'Gitar',
                'SEO'        => 'gitar',
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 5,
                'PARENT_ID' => 5,
                'TITLE'     => "Bass",
                'SEO'       => "bass",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 6,
                'PARENT_ID' => 6,
                'TITLE'     => "Keyboard & Synthesizers",
                'SEO'       => "keyboard-synthesizers",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 7,
                'PARENT_ID' => 7,
                'TITLE'     => "Digital Piano",
                'SEO'       => "digital-piano",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 8,
                'PARENT_ID' => 8,
                'TITLE'     => "Drum & Percussion",
                'SEO'       => "drum-percussion",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 9,
                'PARENT_ID' => 9,
                'TITLE'     => "Microphones",
                'SEO'       => "microphones",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 10,
                'PARENT_ID' => 10,
                'TITLE'     => "DJ Equipment",
                'SEO'       => "dj-equipment",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 11,
                'PARENT_ID' => 11,
                'TITLE'     => "Accessories",
                'SEO'       => "accessories",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 12,
                'PARENT_ID' => 12,
                'TITLE'     => "Video Equipment",
                'SEO'       => "video-equipment",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 13,
                'PARENT_ID' => 1,
                'TITLE'     => "Audio Interfaces",
                'SEO'       => "audio-interfaces",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 14,
                'PARENT_ID' => 1,
                'TITLE'     => "Microphones",
                'SEO'       => "microphones",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 15,
                'PARENT_ID' => 1,
                'TITLE'     => "Studio Monitors",
                'SEO'       => "studio-monitors",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 16,
                'PARENT_ID' => 1,
                'TITLE'     => "Studio Mixers & Control Surfaces",
                'SEO'       => "studio-mixers-control-surfaces",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 17,
                'PARENT_ID' => 1,
                'TITLE'     => "Preamps & Channel Strips",
                'SEO'       => "preamps-channel-strips",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 18,
                'PARENT_ID' => 1,
                'TITLE'     => "Headphones",
                'SEO'       => "headphones",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 19,
                'PARENT_ID' => 1,
                'TITLE'     => "Patchbays",
                'SEO'       => "patchbays",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 20,
                'PARENT_ID' => 1,
                'TITLE'     => "Studio Accessories",
                'SEO'       => "studio-accessories",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 21,
                'PARENT_ID' => 1,
                'TITLE'     => "Acoustic Treatment",
                'SEO'       => "acoustic-treatment",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 22,
                'PARENT_ID' => 1,
                'TITLE'     => "Software",
                'SEO'       => "software",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 23,
                'PARENT_ID' => 1,
                'TITLE'     => "Audio Recorders",
                'SEO'       => "audio-recorders",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 24,
                'PARENT_ID' => 1,
                'TITLE'     => "Audio Players",
                'SEO'       => "audio-players",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 25,
                'PARENT_ID' => 4,
                'TITLE'     => "Elektrik",
                'SEO'       => "elektrik",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 26,
                'PARENT_ID' => 4,
                'TITLE'     => "Akustik",
                'SEO'       => "akustik",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 27,
                'PARENT_ID' => 5,
                'TITLE'     => "Elektrik",
                'SEO'       => "elektrik",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 28,
                'PARENT_ID' => 5,
                'TITLE'     => "Akustik",
                'SEO'       => "akustik",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 29,
                'PARENT_ID' => 8,
                'TITLE'     => "Elektrik",
                'SEO'       => "elektrik",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 30,
                'PARENT_ID' => 8,
                'TITLE'     => "Akustik",
                'SEO'       => "akustik",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 31,
                'PARENT_ID' => 8,
                'TITLE'     => "Perkusi",
                'SEO'       => "perkusi",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 32,
                'PARENT_ID' => 9,
                'TITLE'     => "Recording",
                'SEO'       => "recording",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 33,
                'PARENT_ID' => 9,
                'TITLE'     => "Live",
                'SEO'       => "live",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 34,
                'PARENT_ID' => 10,
                'TITLE'     => "Sample Pad",
                'SEO'       => "sample-pad",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 35,
                'PARENT_ID' => 10,
                'TITLE'     => "CDJ",
                'SEO'       => "cdj",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 36,
                'PARENT_ID' => 10,
                'TITLE'     => "Turntable",
                'SEO'       => "turntable",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 37,
                'PARENT_ID' => 10,
                'TITLE'     => "Mixer DJ",
                'SEO'       => "mixer-dj",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 38,
                'PARENT_ID' => 11,
                'TITLE'     => "Kabel",
                'SEO'       => "kabel",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 39,
                'PARENT_ID' => 12,
                'TITLE'     => "Video Editing Software",
                'SEO'       => "video-editing-software",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 40,
                'PARENT_ID' => 12,
                'TITLE'     => "Video Cameras",
                'SEO'       => "video-cameras",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 41,
                'PARENT_ID' => 12,
                'TITLE'     => "Camera Microphones",
                'SEO'       => "camera-microphones",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 42,
                'PARENT_ID' => 12,
                'TITLE'     => "Video Projectors & Screens",
                'SEO'       => "video-projectors-screens",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 43,
                'PARENT_ID' => 12,
                'TITLE'     => "Video Mixers",
                'SEO'       => "video-mixers",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 44,
                'PARENT_ID' => 12,
                'TITLE'     => "Video Camera Accessories",
                'SEO'       => "video-camera-accessories",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 45,
                'PARENT_ID' => 13,
                'TITLE'     => "Thunderbolt Audio Interfaces",
                'SEO'       => "thunderbolt-audio-interfaces",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 46,
                'PARENT_ID' => 13,
                'TITLE'     => "Ethernet Audio Interfaces",
                'SEO'       => "ethernet-audio-interfaces",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 47,
                'PARENT_ID' => 13,
                'TITLE'     => "FIreWire Audio Interfaces",
                'SEO'       => "firewire-audio-interfaces",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 48,
                'PARENT_ID' => 13,
                'TITLE'     => "USB Audio Interfaces",
                'SEO'       => "usb-audio-interfaces",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ], [
                'ID'        => 49,
                'PARENT_ID' => 13,
                'TITLE'     => "PCI Audio Interfaces",
                'SEO'       => "pci-audio-interfaces",
                'TIPE'       => 'product',
                'STATUS'     => 1
            ]
        ];
        
        DB::table('category')->delete();
        
        foreach ($data as $key) {
            DB::table('category')->insert([
                'ID'        => $key['ID'],
                'PARENT_ID' => $key['PARENT_ID'],
                'TITLE'     => $key['TITLE'],
                'SEO'       => $key['SEO'],
                'TIPE'      => $key['TIPE'],
                'STATUS'    => $key['STATUS']
            ]);   
        } 
    }
}


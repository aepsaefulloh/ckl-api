<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_admin', function (Blueprint $table) {
            $table->increments('ID');
            $table->ipAddress('IP_ADDRESS');
            $table->unsignedInteger('ADMIN_ID');
            $table->foreign('ADMIN_ID')->references('ID')->on('admin');
            $table->dateTime('LOGIN_DATE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_admin');
    }
}

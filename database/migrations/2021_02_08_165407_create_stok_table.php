<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stok', function (Blueprint $table) {
            $table->increments('ID');
            $table->String('UNIQ_CODE');
            $table->foreign('UNIQ_CODE')->references('UNIQ_CODE')->on('product');
            $table->Integer('STOCK_IN')->default(0);
            $table->DateTime('STOCK_DATE');
            $table->DateTime('STOCK_UPDATE')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stok');
    }
}

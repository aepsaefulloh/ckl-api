<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNewsAndTipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_and_tips', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('TITLE', 100);
            $table->text('SUMMARY');
            $table->string('CATEGORY', 100);
            $table->text('CONTENT')->nullable();
            $table->string('IMAGE', 100)->default('default.png');
            $table->string('CAPTION', 100)->nullable();
            $table->integer('HIT')->default(0);
            $table->dateTime('PUBLISH_TIMESTAMP')->nullable();
            $table->smallInteger('STATUS')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_and_tips');
    }
}

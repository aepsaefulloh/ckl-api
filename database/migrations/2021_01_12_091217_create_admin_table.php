<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('EMAIL', 100)->unique();
            $table->string('USER', 100);
            $table->string('PASSWORD');
            $table->string('ROLE', 100);
            $table->string('FULLNAME', 100);
            $table->DateTime('LAST_LOGIN')->nullable();
            $table->string('TOKEN_KEY')->nullable();
            $table->smallInteger('STATUS')->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVoucherTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('voucher', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('CATEGORY')->nullable();
            $table->string('TITLE')->nullable();
            $table->integer('PRICE')->nullable();
            $table->integer('PERCENT')->nullable();
            $table->integer('TOTAL')->nullable();
            $table->integer('SISA')->nullable();
            $table->DateTime('CREATED_DATE');
            $table->smallInteger('STATUS')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('voucher');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGearBaruTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gear_baru', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('TIPE');
            $table->string('UNIQ_CODE');
            $table->foreign('UNIQ_CODE')->references('UNIQ_CODE')->on('product');
            $table->DateTime('CREATED_AT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gear_baru');
    }
}

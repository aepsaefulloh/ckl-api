<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_customer', function (Blueprint $table) {
            $table->increments('ID');
            $table->ipAddress('IP_ADDRESS');
            $table->unsignedInteger('CUSTOMER_ID');
            $table->foreign('CUSTOMER_ID')->references('ID')->on('customer');
            $table->dateTime('LOGIN_DATE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity_customer');
    }
}

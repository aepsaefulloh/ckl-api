<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction', function (Blueprint $table) {
            $table->increments('ID');
            $table->DateTime('TRDATE');
            $table->string('FULLNAME');
            $table->string('PHONE');
            $table->string('EMAIL');
            $table->BigInteger('PROV_ID');
            $table->BigInteger('KAB_ID');
            $table->text('ADDRESS');
            $table->BigInteger('KODEPOS');
            $table->string('SHIPMENT')->nullable();
            $table->string('SHIPPING')->nullable();
            $table->string('PAYMENT');
            $table->Integer('TOTAL');
            $table->unsignedInteger('CUSTOMER_ID');
            $table->foreign('CUSTOMER_ID')->references('ID')->on('customer');
            $table->smallInteger('STATUS')->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBannerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banner', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('CATEGORY')->nullable();
            $table->string('IMAGE')->default('default.png');
            $table->string('TITLE')->nullable();
            $table->text('CAPTION')->nullable();
            $table->string('LINK')->nullable();
            $table->bigInteger('POSITION')->nullable();
            $table->smallInteger('STATUS')->default(0);
            $table->DateTime('PUBLISH_DATE');
            $table->DateTime('UPDATED_DATE')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('banner');
    }
}

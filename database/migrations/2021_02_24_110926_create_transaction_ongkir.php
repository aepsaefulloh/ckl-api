<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionOngkir extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_ongkir', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedInteger('TRANSACTION_ID');
            $table->foreign('TRANSACTION_ID')->references('ID')->on('transaction');
            $table->unsignedInteger('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID')->on('toko');
            $table->string('CODE_COURIER');
            $table->string('COURIER');
            $table->bigInteger('ONGKIR');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_ongkir');
    }
}

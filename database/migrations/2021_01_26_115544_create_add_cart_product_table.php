<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddCartProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('add_cart_product', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('UNIQ_CODE');
            $table->foreign('UNIQ_CODE')->references('UNIQ_CODE')->on('product');
            $table->Integer('JUMLAH')->default(1);
            $table->string('COLOR')->default(1);
            $table->unsignedInteger('CUSTOMER_ID');
            $table->foreign('CUSTOMER_ID')->references('ID')->on('customer');
            $table->DateTime('CREATED_AT')->nullable();
            $table->DateTime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('add_cart_product');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAddtionalImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('additional_image', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('UNIQ_CODE');
            $table->foreign('UNIQ_CODE')->references('UNIQ_CODE')->on('product');
            $table->string('IMAGE', 100);
            $table->smallInteger('STATUS')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addtional_image');
    }
}

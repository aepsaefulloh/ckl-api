<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTokoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toko', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('NAME_TOKO')->nullable();
            $table->string('IMAGE')->default('default.png');
            $table->string('BANK')->nullable();
            $table->string('NO_REK')->nullable();
            $table->string('TELEPHONE')->default(0);
            $table->text('ADDRESS')->nullable();
            $table->Integer('PROV_ID')->nullable();
            $table->Integer('KOTA_ID')->nullable();
            $table->SmallInteger('STATUS')->default(1);
            $table->DateTime('CREATED_AT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toko');
    }
}

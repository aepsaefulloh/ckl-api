<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('IMAGE')->default('default.png');
            $table->integer('PARENT_ID')->default(0);
            $table->unsignedInteger('BRAND_ID');
            $table->foreign('BRAND_ID')->references('ID')->on('BRAND');
            $table->string('TITLE', 100)->nullable();
            $table->string('SEO', 100)->nullable();
            $table->string('TIPE', 100)->nullable();
            $table->smallInteger('STATUS')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}

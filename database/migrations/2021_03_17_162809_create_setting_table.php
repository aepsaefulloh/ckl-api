<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('LOGO')->nullable();
            $table->string('SITE_NAME');
            $table->text('LOCATION');
            $table->string('PHONE');
            $table->string('EMAIL');
            $table->text('EMBLED_MAPS');
            $table->DateTime('CREATED_AT');
            $table->DateTime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting');
    }
}

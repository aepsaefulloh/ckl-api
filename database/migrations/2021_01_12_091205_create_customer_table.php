<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCustomerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customer', function (Blueprint $table) {
            $table->increments('ID');
            $table->string('EMAIL', 100)->unique();
            $table->string('USER', 100);
            $table->string('PASSWORD');
            $table->DateTime('REG_DATE');
            $table->string('FULLNAME', 100);
            $table->text('ADDRESS')->nullable();
            $table->string('TELEPHONE', 13);
            $table->text('TOKEN')->nullable();
            $table->smallInteger('STATUS')->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customer');
    }
}

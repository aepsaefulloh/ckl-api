<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_detail', function (Blueprint $table) {
            $table->increments('ID');
            $table->unsignedInteger('TRANSACTION_ID');
            $table->foreign('TRANSACTION_ID')->references('ID')->on('transaction');
            $table->unsignedInteger('ID_TOKO');
            $table->foreign('ID_TOKO')->references('ID')->on('toko');
            $table->string('UNIQ_CODE');
            $table->foreign('UNIQ_CODE')->references('UNIQ_CODE')->on('product');
            $table->string('VARIAN_COLOR', 100)->nullable();
            $table->Integer('QUANTITY');
            $table->bigInteger('PRICE');
            $table->bigInteger('TOTAL')->default(0);
            $table->text('NOTE')->nullable();
            $table->smallInteger('STATUS')->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_detail');
    }
}

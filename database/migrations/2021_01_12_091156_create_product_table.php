<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->string('UNIQ_CODE', 100)->unique();
            $table->string('NAME_PRODUCT', 100);
            $table->text('DESCRIPTION_PRODUCT')->nullable();
            $table->unsignedInteger('CATEGORY');
            $table->foreign('CATEGORY')->references('ID')->on('category');
            $table->unsignedInteger('BRAND');
            $table->foreign('BRAND')->references('ID')->on('brand');
            $table->string('IMAGE', 100);
            $table->double('PRICE');
            $table->integer('HIT')->default(0);
            $table->unsignedInteger('ID_TOKO');
            $table->string('PCS');
            $table->string('STATUS_BARANG');
            $table->foreign('ID_TOKO')->references('ID')->on('toko');
            $table->smallInteger('STATUS')->nullable()->default('0');
            $table->DateTime('CREATED_AT');
            $table->DateTime('UPDATED_AT')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product');
    }
}
